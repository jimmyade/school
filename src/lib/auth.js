const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const UserModel = require('../models/usersMosel');
const Sequelize = require('sequelize');
const {Users} = require('../models/usersMosel');
var bcrypt =  require('bcryptjs');
const Op = Sequelize.Op;


async function FindUserExist(credential){
    if(!credential) throw new Error('Invalid argument: user_id');
   
        
    const user = await UserModel.Users.findOne({
        where : { [Op.or] : [
                {username: credential},
                {id: credential}
            ]}
    });

    if(user) return user;

    return null;
}

passport.use( new LocalStrategy(function(username, password, done){
    UserModel.Users.findOne({where:{username:username}}).then(function(user){
        if(!user){
            return done(null, false, {message: "Unknown User!"});
        } 
        bcrypt.compare(password, user.password, function(err, isMatch){
            if (err) console.log(err);

            if(isMatch) {
                return done(null, user);
            } else{
                return done(null, false, {message: "Password Incorrect "});
            }
        });
        
    });
}))



// eslint-disable-next-line no-underscore-dangle

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    FindUserExist(id)
        .then(user=>done(null, user));
});
// passport.deserializeUser(async (id, done) => {
//   try {
//     const user = await UserModel.findById(id).exec();
//     return done(null, user);
//   } catch (err) {
//     return done(err);
//   }
// });

module.exports = {
  initialize: passport.initialize(),
  session: passport.session(),
  setUser: (req, res, next) => {
    res.locals.user = req.user;
    next();
  },
  
};

