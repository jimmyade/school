const { sign, verify } = require("jsonwebtoken");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { Users } = require("../models/usersMosel");
require('dotenv').config()






const createTokens = (user) => {
    const accessToken = sign(
        {
            username: user.username,
            id: user.id,
            group_id: user.group_id,
            name: user.name,
            user_phone: user.user_phone,
            service_id: user.service_id,
            service_code: user.service_code,
            organization_id: user.organization_id,
            service_logo: user.service_logo
        },
        process.env.JSON_SECRECT);
    return accessToken;
};


const validateToken = async (req, res, next) => {
    const accessToken = req.cookies["access-token"];

    if (!accessToken)
        // return res.send('User not Authenticated!');
        return next()

    try {
        const validToken = verify(accessToken, process.env.JSON_SECRECT);
        if (validToken) {
            req.user = validToken;
            return next()
        }
    } catch (err) {
        req.flash('info', err)
        res.redirect('/login')
    }
}


// const validateUser = async (req, res, next) => {
//     const accessToken = req.cookies["access-token"];

//     if (!accessToken) {
//         req.flash('info ', 'User not Authenticated!');
//         res.redirect('/login')
//     } 
// }

const logUserOut = async (req, res) => {
    res.clearCookie("access-token")
    res.redirect('/')
}

module.exports = { createTokens, validateToken,  logUserOut }