
const UserModel = require('../models/usersMosel');
const Sequelize = require('sequelize');
const {Users} = require('../models/usersMosel');
var bcrypt =  require('bcryptjs');
const Op = Sequelize.Op;


async function FindUserExist(credential){
    if(!credential) throw new Error('Invalid argument: user_id');
   
        
    const user = await UserModel.Users.findOne({
        where : { [Op.or] : [
                {username: credential},
                {id: credential}
            ]}
    });

    if(user) return user;

    return null;
}

