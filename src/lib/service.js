const { client_services } = require("../models/client_service_and_bufferModel");

exports.isAdmin = function (req, res, next) {
    if (req.isAuthenticated() && res.locals.user.group_id == 111111) {
        next();
      } else {
        req.flash('danger', 'Please Login as admin! ');
        res.redirect('/login')
      }
}

exports.isSuperAdmin = function (req, res, next) {
  if (req.isAuthenticated() && res.locals.user.group_id == 111) {
      next();
    } else {
      req.flash('danger', 'Please Login as General Admin!');
      res.redirect('/login')
    }
}

exports.details = async function(req, res, next) {
  let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
  res.locals.clientName =  c.client;
  res.locals.clientCode = c.service_code;
  next();
}


// const { paystack_credentials } = require("../models/basicTablesModel");

// // const requests = require('requests');
// let livePayKey = '';
// let testPayKay = ''
// exportS.paystackKeys = (req, res) => {

//     let payKey = await paystack_credentials.findOne({ where: { schoolId: req.user.organization_id } });
//     livePayKey = payKey.live_key;
//     testPayKay = payKey.test_key   

// }
// console.log(livePayKey)

// async function getPaystaskKey(paymentKey) {
//     let payKey = await paystack_credentials.findOne({ where: { schoolId: req.user.organization_id } });
//     livePayKey = payKey.live_key;
//     testPayKay = payKey.test_key
//     return testPayKay
// }
