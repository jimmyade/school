const  { verify } = require ("jsonwebtoken");

module.exports.deserializeLoginUser = function (req, res, next) {
    const { accessToken } = req.cookies["access-token"];
    if (!accessToken) {
        return next()
    }

    const { payload } = verify(accessToken)
    if (payload) {
        req.user = payload;
        return next()
    }
    return next()
}

// =deserializeLoginUser;