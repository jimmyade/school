const db = require('../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { user_groups, schoolcategory } = require("../models/basicTablesModel");
const { Api_Payments, payment_settings, items, payment_mandate } = require("../models/payment/paymentModel");
const { subjecttable, shlsession, termsetup } = require('../models/basicTablesModel');
const { classtable } = require("../models/classModel/classModel");


module.exports.addItems = async function (req, res) {
    res.render('./admin/payment/settings/additem')
}
module.exports.postItems = async function (req, res) {
    var pitems = [req.body];
    var acdsession = req.body.ses;
    let uploads = [];
    for (var n of pitems) {
        var m = n.item;
        for (var i = 0; i < m.length; i++) {
            var mm = m[i];
            let upload = {
                items: mm,
                schoolid: req.user.organization_id,
                acdsession: acdsession
            };
            uploads.push(upload);
        }
    }
    console.log(uploads)

    items.bulkCreate(uploads).then(function (result) {
        res.redirect('/admin/item/add_new/item')
    })
}


module.exports.feeItems = async function (req, res) {
    try{
        var item = req.body.item;
        var itemlevel = req.body.itemlevel;
        var sesion = req.body.acdsession;
        var itm = {
            items: item,
            acdsession: sesion,
            options: itemlevel,
            schoolid: req.user.organization_id
        }
        console.log(itm)
        let newItem = new items(itm);
        newItem.save().then(() => {
            res.json({msg: "successful"})
        })
    } catch(err) {
        res.json({msg: err.message})
    }
    
}
module.exports.academicitems = async function (req, res) {
    var ses = req.query.acdsession;
    let paymentitems = await items.findAll({ where: { schoolid: req.user.organization_id, acdsession: ses } });
    res.render('./admin/payment/settings/item', {
        paymentitems
    })
}

// module.exports.getsetupItem = async function (req, res) {
//     var itemid = req.query.item_id
//     let paymentitems = await items.findAll({ where: { schoolid: req.user.organization_id } });
//     res.render('./admin/payment/settings/item', {
//         paymentitems
//     })
// }

module.exports.searchWithSession = async (req, res) => {
    var schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/payment/settings/search_session_item', {
        schsession
    })
}

module.exports.searchAccsession = async (req, res) => {
    var schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/payment/settings/search_session_mandate', {
        schsession
    })
}

module.exports.paymentSetup1 = async (req, res) => {
    var schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });  
    let classt = await db.query(`SELECT * from classtable INNER JOIN schoolcategory on classtable.classCategory = schoolcategory.schoolcategoryid where classtable.schoolId = ${req.user.organization_id} `, { type: QueryTypes.SELECT });
    let categories = await schoolcategory.findAll();  
    let paymentitems = await items.findAll({ where: { schoolid: req.user.organization_id} });
    var settings = await db.query(`SELECT * from payment_settings INNER JOIN items on payment_settings.item = items.items LEFT JOIN termsetup ON payment_settings.termid = termsetup.setup_id WHERE payment_settings.schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    res.render('./admin/payment/settings/setup', {
        schsession, terms, classt, categories,
        paymentitems, settings
    })
}
module.exports.addSession = async function (req, res) {
    try {
        var dd = {
            acdsession: req.body.acdsession,
            schoolId: req.user.organization_id
        }
        let addsession = new shlsession(dd);
        console.log(addsession)
        addsession.save().then(() => {
            res.json({msg: "successful"})
        })
    } catch (err) {
        res.json({msg: err.message})
    }
}
module.exports.getUpdateSesPage = async(req, res) => {
    let ses = await  shlsession.findOne({where:{id:req.query.applicantno}});
    res.render('./admin/payment/settings/updatesession', {
        ses
    })
}
module.exports.UpdteSession = async(req, res) => {
    let ses = await  shlsession.findOne({where:{id:req.query.applicantno}});
    ses.update({acdsession: req.body.acdsession}, {new:true}).then(() => {
        req.flash('info', 'Updated')
        res.redirect('back')
    })
}

module.exports.deleteSession = async (req, res) => {
    let ses = await shlsession.findOne({where:{id:req.query.applicantno}});
    ses.destroy().then(() => {
        req.flash('info', 'Deleted!!!')
        res.redirect('back')
    })
}
module.exports.paymentsetup = async (req, res) => {
    try{
        console.log(req.body)
    } catch(err) {
        res.json({msg: err.message})
    }
    
}
module.exports.paymentSettings = async function (req, res) {
    var settings = await db.query(`SELECT * from payment_settings INNER JOIN shlsessions on payment_settings.sesion_id = shlsessions.id INNER JOIN classtable ON payment_settings.class_id = classtable.Id 
    LEFT JOIN termsetup ON payment_settings.termid = termsetup.setup_id WHERE payment_settings.schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    let term_session = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll();
    res.render('./admin/payment/settings/payment_setup', {
        settings, term_session, classt, terms
    })
}
module.exports.paymentSetup = async function (req, res) {
    var itemid = req.query.item_id
    let paymentitems = await items.findOne({ where: { itemsID: itemid } });
    var settings = await db.query(`SELECT * from payment_settings INNER JOIN shlsessions on payment_settings.sesion_id = shlsessions.id INNER JOIN classtable ON payment_settings.class_id = classtable.Id 
    LEFT JOIN termsetup ON payment_settings.termid = termsetup.setup_id WHERE payment_settings.schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    let term_session = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll();
    res.render('./admin/payment/settings/paymentItmeSetup', {
        settings, term_session, classt, terms, paymentitems
    })
}

module.exports.addPaymentSetup = async function (req, res) {
    var itemID = req.query.item_id
    var Iitems = [req.body];
    var Citems = [req.body];
    var Oitems = [req.body];
    var Sitems = [req.body];
    var Titems = [req.body];
    var Aitems = [req.body];

    let uploads = [];
    for (var a of Iitems) {
        for (var b of Citems) {
            for (var c of Oitems) {
                for (var d of Sitems) {
                    for (var e of Titems) {
                        for (var f of Aitems) {

                            var I = a.item;
                            var C = b.class_id;
                            var O = c.options;
                            var S = d.sesion;
                            var T = e.setup_id;
                            var A = f.amount;
                            for (var i = 0; i < I.length; i++) {

                                var II = I[i];
                                var CC = C[i];
                                var OO = O[i];
                                var SS = S[i];
                                var TT = T[i];
                                var AA = A[i];
                                var result = await classtable.findOne({ where: { Id: CC } })
                                let upload = {
                                    item: II,
                                    class_id: CC,
                                    options: OO,
                                    sesion_id: SS,
                                    schoolid: req.user.organization_id,
                                    termid: TT,
                                    amount: AA,
                                    classname: result.className
                                };
                                uploads.push(upload);



                            }
                        }
                    }
                }
            }
        }
    }

    console.log(uploads)
    payment_settings.bulkCreate(uploads).then(result => {
        req.flash('primary', "Items successfully setup")
        res.redirect('back')
    })
}
module.exports.addpaysettings = async function (req, res) {
    try {
        let setup = {
            item: req.body.item,
            amount: req.body.amount,
            sesion_id: req.body.sesion,
            options: req.body.options,
            schoolid: req.user.organization_id,
            termid: req.body.setup_id,
            class_id: req.body.class_id
        };
        let paymentSetup = new payment_settings(setup);
        paymentSetup.save().then(function (result) {
            req.flash('info', 'Added');
            res.redirect('/admin/payment/settings')
        })
    } catch (e) {
        console.log(e)
    }
}

module.exports.deleteSetup = async function (req, res) {
    var settings = await payment_settings.findOne({ where: { setup_id: req.query.payment_setup } });
    settings.destroy().then(function (result) {
        req.flash('danger', 'Setup successfully deleted');
        res.redirect('/admin/payment/settings')
    })
}

module.exports.getUpdateP = async function (req, res) {
    var settings = await db.query(`SELECT * from payment_settings INNER JOIN shlsessions ON payment_settings.sesion_id = shlsessions.id INNER JOIN classtable ON payment_settings.class_id = classtable.Id 
    LEFT JOIN termsetup ON payment_settings.termid = termsetup.setup_id WHERE payment_settings.paymentsetup_id = ${req.query.payment_setup}`, { type: QueryTypes.SELECT });
    let term_session = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll();
    console.log(settings)
    console.log(req.query.payment_setup)
    res.render('./admin/payment/settings/update_payment_setup', {
        settings: settings[0], term_session, classt, terms, idsetup: req.query.payment_setup
    })
}
module.exports.updatePaysettings = async function (req, res) {
    try {

        let setup = {
            item: req.body.item,
            amount: req.body.amount,
            sesion_id: req.body.sesion,
            options: req.body.options,
            schoolid: req.user.organization_id,
            termid: req.body.setup_id,
            class_id: req.body.class_id
        };
        let payup = await payment_settings.findOne({ where: { paymentsetup_id: req.query.payment_setup } });
        payup.update(setup, { new: true });
        req.flash('info', "Updated!")
        res.redirect(`back`)
    } catch (e) {
        console.log(e)
    }
}

module.exports.viewPayments = async function (req, res) {
    var payments = await db.query(`select * from api_payments INNER JOIN studentregistration on api_payments.CustReference = studentregistration.studentId WHERE api_payments.service_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // Api_Payments.findAll({where:{service_id:req.user.organization_id}});
    //console.log(payments)
    res.render('./admin/payment/view_payments', {
        payments
    })

}

module.exports.individualTransaction = async function (req, res) {
    var payments = await db.query(`select * from api_payments INNER JOIN studentregistration on api_payments.CustReference = studentregistration.studentId WHERE api_payments.ReceiptNo = ${req.query.transaction_recieptNo}`, { type: QueryTypes.SELECT });
    res.render('', {
        payments
    })
}
module.exports.getPayment = async function (req, res) {
    var payments = await Api_Payments.findOne({ where: { CustReference: req.query.studentid } });
    res.render("", {
        payments
    })
}
// view genereted mandate 
module.exports.generatedMandate = async (req, res) => {
    var ses = req.query.acdsession;
    let Mandates = await db.query(`SELECT * FROM payment_mandate INNER JOIN classtable ON payment_mandate.class_id = classtable.Id WHERE payment_mandate.sessions='${ses}' AND payment_mandate.schoolid = '${req.user.organization_id}'`, { type: QueryTypes.SELECT });
    // let Mandates = await payment_mandate.findAll({ where: { schoolid: req.user.organization_id } });
    res.render('./admin/payment/view_mandate', {
        Mandates: Mandates
    })
    // res.send(Mandates)
}
// and payment_settings.sessions = ${req.body.session}
module.exports.getMandatePage = async function (req, res) {
    res.render('')
}

module.exports.generateStudentFeesMandate = async function (req, res) {
    var mandate = await db.query(`INSERT IGNORE INTO payment_mandate (class_id, class_name, item, amount, sessions, term, entered_by, schoolid) 
    SELECT class_id, className, item, amount, acdsession, term, 'jude',  1609676920910  from payment_settings INNER JOIN shlsessions on payment_settings.sesion_id = shlsessions.id INNER JOIN classtable ON payment_settings.class_id = classtable.Id 
        LEFT JOIN termsetup ON payment_settings.termid = termsetup.setup_id; `);
    if (mandate == true) {
        req.flash('primary', 'Mandate Successfully Generated');
        res.redirect('back');
    }
}

module.exports.approveMandate = async function (req, res) {
    try {
        let mandateid = req.query.id;
        let authorizeMandate = {
            authorized: 1,
            authorized_by: req.user.username
        };
        let payment = await payment_mandate.findOne({ where: { mandate_invoice_id: mandateid } });
        payment.update(authorizeMandate, { new: true }).then(result => {
            req.flash('info', "Mandate Approved!");
            res.redirect('/admin/view/generated/mandate');
        }).catch(err => {
            req.flash('info', err.message);
            res.redirect('/admin/view/generated/mandate');
        })
    } catch (e) {
        req.flash('info', e.message);
        res.redirect('/admin/view/generated/mandate');
    }
}

module.exports.deleteMandate = async function (req, res) {
    let mandateid = req.query.id;
    let payment = await payment_mandate.findOne({ where: { mandate_invoice_id: mandateid } });
    payment.destroy().then(result => {
        req.flash('info', "Mandate Deleted!");
        res.redirect('/admin/view/generated/mandate');
    }).catch(err => {
        req.flash('info', err.message);
        res.redirect('/admin/view/generated/mandate');
    })
}


module.exports.approvePayment = async (req, res) => {

}
