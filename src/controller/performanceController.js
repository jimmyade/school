const db = require('../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const { classtable } = require("../models/classModel/classModel");
const { punctuality, attentionskills, workhabit, pychomotorskills } = require("../models/performance/performanceModel");
const { shlsession, termsetup } = require('../models/basicTablesModel');

module.exports.getPunctuality = async function (req, res) {
    var classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });

    res.render('./admin/performance/performance', {
        classt: classt, schsession, terms
    })
}

module.exports.getClassPunctuality = async function (req, res) {
    var classid = req.query.class_id;
    var acdses = req.query.acdsession;
    var term = req.query.term;

    classtable.findOne({ where: { id: classid, schoolId: req.user.organization_id } }).then(function (lassresult) {
        // punctuality.findAll({ where: { className: lassresult.className, schoolId:req.user.organization_id } }).then(function (result) {
            db.query(`select * from punctuality INNER JOIN studentregistration on punctuality.student_reg_Id = studentregistration.student_reg_Id 
            AND studentregistration.className = punctuality.className WHERE punctuality.className ='${lassresult.className}' AND punctuality.sesion ='${acdses}' AND punctuality.term LIKE '${term}' AND punctuality.schoolId =${req.user.organization_id}` , { type: QueryTypes.SELECT })
            .then (result =>{
                res.send(result);
            })
            
        // })
    })
}

module.exports.uploadPunctuality = async (req, res) => {
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach(async(rows) => {
            let upload = {
                className: req.body.schoolclass_id,
                term: req.body.term_id,
                schoolcategory_id: req.body.schoolcategory_id,
                schoolId: req.user.organization_id,
                sesion: req.body.schsession,
                registeredBy: req.user.username,
                student_reg_Id: rows[1],
                className: rows[2],
                totals:  rows[3],
            };
            // console.log(upload)
            uploads.push(upload);
        });
        console.log(uploads)
        punctuality.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
                req.flash('info', 'Uploaded')
                res.redirect('back')
            })
            
        }).catch(err => {
            req.flash('danger', err.message)
            res.redirect('back')
        });

    })

}


// attention controller
module.exports.attentionPage = async function (req, res) {
    var classt = await classtable.findAll({where:{schoolId:req.user.organization_id}});
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/performance/attentionskils', {
        classt: classt, schsession, terms
    })
}

module.exports.getAttention = async function (req, res) {
    var classid = req.query.class_id;
    var acdses = req.query.acdsession;
    var term = req.query.term;

    classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
        db.query(`SELECT * from attentionskills INNER JOIN studentregistration on attentionskills.student_reg_Id = studentregistration.student_reg_Id  WHERE attentionskills.className = '${lassresult.className}'and attentionskills.term LIKE '%${term}%' and attentionskills.sesion = '${acdses}' and attentionskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
            res.send(result)
        })
    })
}

module.exports.uploadAttention = async (req, res) => {
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach(async(rows) => {
            let upload = {
                className: req.body.schoolclass_id,
                term: req.body.term_id,
                schoolcategory_id: req.body.schoolcategory_id,
                sesion: req.body.schsession,
                registeredBy: req.user.username,
                schoolId: req.user.organization_id,
                student_reg_Id: rows[1],
                concentration_for_3_minutes:  rows[3],
                concentration_for_5_minutes:  rows[4],
                concentration_for_10_minutes:  rows[5],
                respond_to_correction:  rows[6],
                easily_distracted:  rows[7],
                overcome_difficulties:  rows[8],
                enjoy_listening:  rows[9],
                willing_to_paticipate:  rows[10]
            };
            // console.log(upload)
            uploads.push(upload);
        });
        console.log(uploads)
        attentionskills.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
                req.flash('info', 'Uploaded')
                res.redirect('back')
            })
            
        }).catch(err => {
            req.flash('danger', err.message)
            res.redirect('back')
        });

    })

}

// workhabit controller
module.exports.workhabitPage = async function (req, res) {
    var classt = await classtable.findAll({where:{schoolId:req.user.organization_id}});
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/performance/workhabit', {
        classt: classt, schsession, terms
    })
}

module.exports.getWorkhabit = async function (req, res) {
    var classid = req.query.class_id;
    var acdses = req.query.acdsession;
    var term = req.query.term;

    classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
        db.query(`select * from workhabit INNER JOIN studentregistration on workhabit.student_reg_Id = studentregistration.student_reg_Id 
        AND studentregistration.className = workhabit.className  WHERE workhabit.className = '${lassresult.className}' and workhabit.sesion = '${acdses}' and workhabit.term LIKE '${term}' and workhabit.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
            res.send(result)
        })
    })
}

module.exports.uploadWorkhabit = async (req, res) => {
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach(async(rows) => {
            let upload = {
                className: req.body.schoolclass_id,
                term: req.body.term_id,
                schoolcategory_id: req.body.schoolcategory_id,
                schoolId: req.user.organization_id,
                sesion: req.body.schsession,
                registeredBy: req.user.username,
                student_reg_Id: rows[1],
                follows_instructions:  rows[3],
                works_independently:  rows[4],
                does_not_disturb:  rows[5],
                care_for_materials:  rows[6],
                completes_task:  rows[7],
            };
            // console.log(upload)
            uploads.push(upload);
        });
        console.log(uploads)
        workhabit.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
                req.flash('info', 'Uploaded')
                res.redirect('back')
            })
            
        }).catch(err => {
            req.flash('danger', err.message)
            res.redirect('back')
        });

    })

}

// workhabit controller
module.exports.pychomotorskillsPage = async function (req, res) {
    var classt = await classtable.findAll({where:{schoolId:req.user.organization_id}});
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/performance/pychomotorskills', {
        classt: classt, schsession, terms
    })
}

module.exports.getPychomotorskills = async function (req, res) {
    var classid = req.query.class_id;
    var acdses = req.query.acdsession;
    var term = req.query.term;
    classtable.findOne({ where: { className: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
        db.query(`select * from pychomotorskills INNER JOIN studentregistration on pychomotorskills.student_reg_Id = studentregistration.student_reg_Id 
        AND studentregistration.className = pychomotorskills.className  WHERE pychomotorskills.className = '${lassresult.className}' and pychomotorskills.sesion = '${acdses}' and pychomotorskills.term LIKE '${term}' and pychomotorskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
            res.send(result)
        })
    })
}

module.exports.uploadPychomotorskills = async (req, res) => {
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach(async(rows) => {
            let upload = {
                    className: req.body.schoolclass_id,
                    term: req.body.term_id,
                    schoolcategory_id: req.body.schoolcategory_id,
                    sesion: req.body.schsession,
                    registeredBy: req.user.username,
                    schoolId:  req.user.organization_id,
                    student_reg_Id: rows[1],
                    drawing_and_painting: rows[3],
                    games:  rows[4],
                    flexibility:  rows[5],
                    verbal_fluency:  rows[6],
                    overall_progress:  rows[7]
            };
            // console.log(upload)
            uploads.push(upload);
        });
        console.log(uploads)
        pychomotorskills.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
                req.flash('info', 'Uploaded')
                res.redirect('back')
            })
            
        }).catch(err => {
            req.flash('danger', err.message)
            res.redirect('back')
        });

    })

}