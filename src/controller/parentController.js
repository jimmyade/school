const db = require('../../src/datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { studentregistration } = require("../models/studentModel/studentModel");
const { payment_settings } = require('../models/payment/paymentModel');
const { shlsession, termsetup } = require('../models/basicTablesModel');
const { school_classes, classtable } = require('../models/classModel/classModel');
const { punctuality, attentionskills, pychomotorskills, workhabit } = require('../models/performance/performanceModel');


module.exports.parentDashboard = async function (req, res) {

    console.log(req.user.username)
    let kids = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    let totalfess = await db.query(`select sum( payment_settings.amount ) as amount, payment_settings.paymentsetup_id as item_id 
    from studentregistration left JOIN payment_settings on studentregistration.className = payment_settings.classname 
    where studentregistration.guardian_email = '${req.user.username}';`, { type: QueryTypes.SELECT })
    res.render('./parent/dashboard', {
        kids, totalfess: totalfess[0]
    })
}

module.exports.wards = async (req, res) => {
    let students = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    res.render('./parent/view_lads.ejs', {
        students
    })
}

module.exports.feesBreakDown = async (req, res) => {
    let fees = await db.query(`select  studentregistration.student_reg_Id as id, studentregistration.firstname as firstname, studentregistration.surname as surname, studentregistration.middlename as middlename, studentregistration.className as classname, studentregistration.guardian_email as email, payment_settings.item as item, payment_settings.options as paymentoption, payment_settings.amount as amount, payment_settings.paymentsetup_id as item_id from studentregistration left JOIN payment_settings on studentregistration.className = payment_settings.classname 
    where studentregistration.guardian_email = '${req.user.username}';`, { type: QueryTypes.SELECT });
    res.render('./parent/fees', {
        fees
    })
}

module.exports.individualFee = async function (req, res) {
    var id = req.query.student_id;

    let fees = await db.query(`select studentregistration.student_reg_Id as id, studentregistration.firstname as firstname, studentregistration.surname as surname, studentregistration.middlename as middlename, studentregistration.className as classname, studentregistration.guardian_email as email, payment_settings.item as item, payment_settings.options as option, payment_settings.amount as amount, payment_settings.paymentsetup_id as item_id
    from studentregistration left JOIN payment_settings on studentregistration.className = payment_settings.classname where studentregistration.schoolId = ${req.user.organization_id} AND studentregistration.student_reg_Id = ${id}`, { type: QueryTypes.SELECT });
    res.render('./parent/fees', {
        fees, id
    })
}

module.exports.individualPayReview = async (req, res) => {
    console.log(req.query.student_id);
    console.log(req.query.itemid)
}

module.exports.feeChecPoint = async function (req, res) {
    var id = req.query.student_id;
    var item_id = req.query.payment_id;
    let pay = await db.query(`select studentregistration.student_reg_Id as id, studentregistration.firstname as firstname, studentregistration.surname as surname, studentregistration.middlename as middlename, studentregistration.className as classname, studentregistration.guardian_email as email, payment_settings.item as item, payment_settings.options as option, payment_settings.amount as amount, payment_settings.paymentsetup_id as item_id from studentregistration left JOIN payment_settings on studentregistration.className = payment_settings.classname where studentregistration.schoolId = ${req.user.organization_id} AND studentregistration.student_reg_Id = ${id} AND payment_settings.paymentsetup_id = ${item_id}`, { type: QueryTypes.SELECT });
    res.render('./parent/check_point', {
        pay
    })
}


// class related activities 
module.exports.getPunctuality = async function (req, res) {
    // var classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    res.render('./parent/performance/performance', {
        lads, schsession
    })
}

module.exports.getClassPunctuality = async function (req, res) {
    var student_id = req.query.lads_name;
    var schsess = req.query.schoolsession;
    db.query(`select * from punctuality INNER JOIN studentregistration on punctuality.student_reg_Id = studentregistration.student_reg_Id 
            AND studentregistration.className = punctuality.className WHERE punctuality.student_reg_Id ='${student_id}' and sesion ='${schsess}' AND punctuality.schoolId =${req.user.organization_id}`, { type: QueryTypes.SELECT })
        .then(result => {
            res.send(result);
        })

}


// attention controller
module.exports.attentionPage = async function (req, res) {
    // var classt = await classtable.findAll({where:{schoolId:req.user.organization_id}});
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    res.render('./parent/performance/attentionskils', {
        lads: lads, schsession
    })
}

module.exports.getAttention = async function (req, res) {
    var student_id = req.query.lads_name;
    var schsess = req.query.schoolsession;
    // classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
    db.query(`SELECT * from attentionskills INNER JOIN studentregistration on attentionskills.student_reg_Id = studentregistration.student_reg_Id  WHERE attentionskills.student_reg_Id ='${student_id}' and sesion ='${schsess}' and attentionskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
        res.send(result)
    })
    // })
}

// workhabit controller
module.exports.workhabitPage = async function (req, res) {
    // var classt = await classtable.findAll({where:{schoolId:req.user.organization_id}});
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    res.render('./parent/performance/workhabit', {
        lads: lads, schsession
    })
}

module.exports.getWorkhabit = async function (req, res) {
    var student_id = req.query.lads_name;
    var schsess = req.query.schoolsession;
    // classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
    db.query(`select * from workhabit INNER JOIN studentregistration on workhabit.student_reg_Id = studentregistration.student_reg_Id 
        AND studentregistration.className = workhabit.className  WHERE workhabit.student_reg_Id = '${student_id}' and sesion ='${schsess}' and workhabit.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
        res.send(result)
    })
    // })
}

// 
// workhabit controller
module.exports.pychomotorskillsPage = async function (req, res) {
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    res.render('./parent/performance/pychomotorskills', {
        lads: lads, schsession
    })
}

module.exports.getPychomotorskills = async function (req, res) {
    var student_id = req.query.lads_name;
    var schsess = req.query.schoolsession;
    // classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } }).then(function (lassresult) {
    db.query(`select * from pychomotorskills INNER JOIN studentregistration on pychomotorskills.student_reg_Id = studentregistration.student_reg_Id 
        AND studentregistration.className = pychomotorskills.className  WHERE pychomotorskills.student_reg_Id ='${student_id}' and sesion ='${schsess}' and pychomotorskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(function (result) {
        res.send(result)
    })
    // })
}


module.exports.getWardsResultDetails = async (req, res) => {
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    let classt = await school_classes.findAll({ where: { school_id: req.user.organization_id } });
    res.render('./parent/result/get_result', {
        lads, schsession, terms, classt, error:''
    })
}


module.exports.wardsResultDetails = async (req, res) => {
    try {
        var detail = req.body;
        console.log(detail)
        var studentid = detail.lads_name;
        var termid = detail.term_id;
        var clssid = detail.cls_id;
        var sess = detail.schsession;
        var error = []
        let terms = await termsetup.findOne({ where: { setup_id: termid } });
        // var studentp = req.query.position;
        // var clssid = req.query.classid;
        let student = await studentregistration.findOne({ where: { studentId: studentid } })
        let result1 = await db.query("SELECT `subjectName`,  subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title FROM `subjecttable` INNER JOIN `assessment` ON `subjecttable`.`Id` = `assessment`.`subject_id` WHERE  assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id ='" + termid + "' AND assessment.session_id = '" + sess + "' GROUP BY subjectName ORDER BY subject_id ASC;", { type: QueryTypes.SELECT })

        let calresults = await db.query("SELECT student_id, term_id, subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title  From assessment where assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id='" + termid + "' AND assessment.session_id='" + sess + "' ORDER BY subject_score DESC", { type: QueryTypes.SELECT })
        let classN = await classtable.findOne({ where: { Id: clssid } })
        var termdetail = await termsetup.findOne({ where: { term: terms.term, schoolId: req.user.organization_id, session: sess } })
        let puntual = await punctuality.findOne({ where: { student_reg_Id: student.student_reg_Id, term: terms.term, sesion: sess } });
        let attention = await attentionskills.findOne({ where: { student_reg_Id: student.student_reg_Id, term: terms.term, sesion: sess } });
        let pycho = await pychomotorskills.findOne({ where: { student_reg_Id: student.student_reg_Id, term: terms.term, sesion: sess } });
        let workh = await workhabit.findOne({ where: { student_reg_Id: student.student_reg_Id, term: terms.term, sesion: sess } });
        if (calresults == null || calresults == "" ) {
            error.push({ msg: `${studentid} result not ready` })
        }
        
        if (puntual == null) {
            error.push({ msg: 'Puntuality skills records not found' })
        }
        if (attention == null) {
            error.push({ msg: 'Attention skills records not found' })
        }
        if (pycho == null) {
            error.push({ msg: 'Pychomotor skills records not found' })
        }
        if (workh == null) {
            error.push({ msg: 'Work habit skills records not found' })
        }
        console.log(error.length)
        if (error.length > 0) {
            let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
            let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
            let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
            let classt = await school_classes.findAll({ where: { school_id: req.user.organization_id } });
            res.render('./parent/result/get_result', {
                lads, schsession, terms, classt, error
            })
        } else {
            res.render('./reports/exam_report/result_sheets/result_sheet1', {
                studentid, term: terms.term, session: sess, student, result1, calresults,
                puntual, attention, pycho, workh, termdetail, classN
            })
        }
        
    } catch (err) {
        req.flash('info', 'Still Processing Result');
        res.redirect('back')
    }


}



// payment_settings