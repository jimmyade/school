const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const request = require('request');
const excel = require('exceljs');
const student = require('../models/studentModel/studentModel');

const mkdirp  = require('mkdirp')

module.exports. assessmentFormat = async function (req, res) {
    let classss = req.query.class_id 

    var fileName = `${classss} assessment format.xlsx`; 
    student.studentregistration.findAll({ where: { className: classss } }).then(objs =>{
    // .then((objs) => {
      let Assessments = [];
  
      objs.forEach((obj) => {
  
        Assessments.push({
          studentId: obj.studentId,
          Ass1: "",
          Ass2: "",
          Ass3: "",
          Ass4: "",
          Ass5: "",
          Ass6: ""
        });
      })
  
    //   console.log(Assessments)
      let workbook = new excel.Workbook();
      let worksheet = workbook.addWorksheet("aseessment");
  
      worksheet.columns = [
        { header: "Student ID", key: "studentId", width: 25 },
        { header: "Assessment 1", key: "Ass1", width: 25 },
        { header: "Assessment 2", key: "Ass2", width: 25 },
        { header: "Assessment 3", key: "Ass3", width: 25 },
        { header: "Assessment 4", key: "Ass4", width: 25 },
        { header: "Assessment 5", key: "Ass5", width: 25 },
        { header: "Assessment 6", key: "Ass6", width: 25 },
        
      ];
  
      // Add Array Rows
      worksheet.addRows(Assessments);
      //console.log(worksheet)
  
      res.setHeader(
        "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition", "attachment; filename=" + fileName
      );
   
    workbook.xlsx.write(res).then(function () {
        console.log('Okay')
      });
    });
  };
  

  // function to download Punctuality excel format
  module.exports.punctualityFormat = async function (req, res) {
    let clssName = req.query.class_id  
    var documentName = `${clssName} punctuality format.xlsx`; 
    student.studentregistration.findAll({ where: { className: clssName } }).then(objs =>{
    // .then((objs) => {
      let puntuality = [];
      objs.forEach((obj) => {
        puntuality.push({
          studentId: obj.studentId,
          RegNo: obj.student_reg_Id,
          className: obj.className,
          totals: ""
        });
      })
      // console.log(puntuality)
      let workbook = new excel.Workbook();
      let worksheet = workbook.addWorksheet("puntuality");
  
      worksheet.columns = [
        { header: "Student ID", key: "studentId", width: 25 },
        { header: "Reg No", key: "RegNo", width: 10 },
        { header: "Class Name ", key: "className", width: 25 },
        { header: "Total", key: "totals", width: 25 }, 
      ];
      // Add Array Rows
      worksheet.addRows(puntuality);
  
      res.setHeader(
        "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition", "attachment; filename=" + documentName
      );
  
      workbook.xlsx.write(res).then(function () {
        console.log('Okay')
      });
    });
  };


   // function to download workhabit excel format
   module.exports.workhabit = async function (req, res) {
    let clssName = req.query.class_id  
    var documentName = `${clssName} workhabit format.xlsx`; 
    student.studentregistration.findAll({ where: { className: clssName } }).then(objs =>{
    // .then((objs) => {
      let puntuality = [];
      objs.forEach((obj) => {
        puntuality.push({
          studentId: obj.studentId,
          RegNo: obj.student_reg_Id,
          className: obj.className,
          follows_instructions: "",
          works_independently: "",
          does_not_disturb: "",
          care_for_materials: "",
          completes_task: ""
        });
      })
      // console.log(puntuality)
      let workbook = new excel.Workbook();
      let worksheet = workbook.addWorksheet("puntuality");
  
      worksheet.columns = [
        { header: "Student ID", key: "studentId", width: 25 },
        { header: "Reg No", key: "RegNo", width: 10 },
        { header: "Class Name ", key: "className", width: 25 },
        { header: "Follows Instruction", key: "follows_instructions", width: 25 }, 
        { header: "Works Independently", key: "works_independently", width: 25 }, 
        { header: "Does Not Disturb", key: "does_not_disturb", width: 25 }, 
        { header: "Care For Materials", key: "care_for_materials", width: 25 }, 
        { header: "Completes Task", key: "totals", completes_task: 25 }, 
      ];
      // Add Array Rows
      worksheet.addRows(puntuality);
      res.setHeader(
        "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition", "attachment; filename=" + documentName
      );
  
      workbook.xlsx.write(res).then(function () {
        console.log('Okay')
      });
    });
  };


    // function to download pychomotor excel format
    module.exports.pychomotor = async function (req, res) {
      let clssName = req.query.class_id  
      var documentName = `${clssName} pychomotor format.xlsx`; 
      student.studentregistration.findAll({ where: { className: clssName } }).then(objs =>{
      // .then((objs) => {
        let puntuality = [];
        objs.forEach((obj) => {
          puntuality.push({
            studentId: obj.studentId,
            RegNo: obj.student_reg_Id,
            className: obj.className,
            drawing_and_painting: "",
            games: "",
            flexibility: "",
            verbal_fluency: "",
            overall_progress: ""
          });
        })
        // console.log(puntuality)
        let workbook = new excel.Workbook();
        let worksheet = workbook.addWorksheet("puntuality");
    
        worksheet.columns = [
          { header: "Student ID", key: "studentId", width: 25 },
          { header: "Reg No", key: "RegNo", width: 10 },
          { header: "Class Name ", key: "className", width: 25 },
          { header: "Drawing And Painting", key: "follows_instructions", width: 25 }, 
          { header: "Games", key: "works_independently", width: 25 }, 
          { header: "Flexibility", key: "does_not_disturb", width: 25 }, 
          { header: "Verbal Fluency", key: "care_for_materials", width: 25 }, 
          { header: "Overall Progress", key: "totals", completes_task: 25 }, 
        ];
        // Add Array Rows
        worksheet.addRows(puntuality);
        res.setHeader(
          "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
          "Content-Disposition", "attachment; filename=" + documentName
        );
    
        workbook.xlsx.write(res).then(function () {
          console.log('Okay')
        });
      });
    };

    
    // function to download pychomotor excel format
    module.exports.attendance = async function (req, res) {
      let clssName = req.query.class_id  
      var documentName = `${clssName} attendance format.xlsx`; 
      student.studentregistration.findAll({ where: { className: clssName } }).then(objs =>{
      // .then((objs) => {
        let puntuality = [];
        objs.forEach((obj) => {
          puntuality.push({
            studentId: obj.studentId,
            RegNo: obj.student_reg_Id,
            className: obj.className,
            concentration_for_3_minutes: "",
            concentration_for_5_minutes: "",
            concentration_for_10_minutes: "",
            respond_to_correction: "",
            easily_distracted: "",
            overcome_difficulties: "",
            enjoy_listening: "",
            willing_to_paticipate: "",
          });
        })
        // console.log(puntuality)
        let workbook = new excel.Workbook();
        let worksheet = workbook.addWorksheet("puntuality");
    
        worksheet.columns = [
          { header: "Student ID", key: "studentId", width: 25 },
          { header: "Reg No", key: "RegNo", width: 10 },
          { header: "Class Name ", key: "className", width: 25 },
          { header: "Concentration For 3 Minutes", key: "follows_instructions", width: 25 }, 
          { header: "Concentration For 5 Minutes", key: "works_independently", width: 25 }, 
          { header: "Concentration For 10 Minutes", key: "does_not_disturb", width: 25 }, 
          { header: "Respond To Correction", key: "care_for_materials", width: 25 }, 
          { header: "Easily Distracted", key: "totals", completes_task: 25 },
          { header: "Overcome Difficulties", key: "totals", completes_task: 25 },
          { header: "Enjoy Listening", key: "totals", completes_task: 25 },
          { header: "Willing To Paticipate", key: "totals", completes_task: 25 }, 
        ];
        // Add Array Rows
        worksheet.addRows(puntuality);
        res.setHeader(
          "Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
          "Content-Disposition", "attachment; filename=" + documentName
        );
    
        workbook.xlsx.write(res).then(function () {
          console.log('Okay')
        });
      });
    };
  