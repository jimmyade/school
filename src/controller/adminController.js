const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db')

const { parent } = require("../models/parentModel/parentModel");
const { staffregistration } = require("../models/staffModel/staffmodel");
const { studentregistration } = require("../models/studentModel/studentModel");

module.exports.adminDashboard = async function(re, res){
    res.render('./admin/dashboard')
}
module.exports.adminPanel = async function(req, res){
    var students = await studentregistration.count({ where: { schoolId:req.user.organization_id} });
    var staffs = await staffregistration.count({ where: { schoolId:req.user.organization_id} });
    var p = await parent.count({where: { schoolId:req.user.organization_id}})
    let notices = await db.query(`SELECT * from noticeboard INNER JOIN user_groups on noticeboard.viewby = user_groups.group_id where noticeboard.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    var abs = await db.query(`SELECT COUNT(attendance.attendanceID) as count, attendance.classid as clsname  FROM attendance 
    INNER JOIN classtable on attendance.classid = classtable.className 
    where attendance.attendance = "0" AND week(attendance.registered_date) = week(now()) GROUP BY attendance.classid` , { type: QueryTypes.SELECT })
    res.render('./admin/adminPanel', {
        students, staffs, p, abs, notices
    })
}
module.exports.reportDash = async function(re, res){
    res.render('./admin/dashboard')
}
module.exports.inventoryDash = async function(re, res){
    res.render('./admin/dashboard')
}

module.exports.paymentDash = async function(re, res){
    res.render('./admin/payment/payment_panel')
}