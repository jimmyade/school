const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const fs = require('fs');
const multer = require('multer');

const readXlsxFile = require('read-excel-file/node');
const { studentregistration } = require('../../src/models/studentModel/studentModel')
const { titletable, termsetup, subjecttable, sessionsetting, shlsessions } = require('../../src/models/basicTablesModel');
const { Users } = require('../../src/models/usersMosel');
const { classtable } = require('../models/classModel/classModel');
const { parent } = require('../models/parentModel/parentModel');
const { payment_mandate } = require('../models/payment/paymentModel');
const joiValidation = require('../validation/joiValidation')


module.exports.studentRegistration = async function (req, res) {
    try {
        var titles = await titletable.findAll();
        var classname = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
        res.render('./admin/admission/student_Registration', {
            titles, classname
        });
    } catch (e) {
        console.log(e)
    }

}

function genID(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
        n = Math.floor(Math.random() * 7);
        s += nuc[n];
        i++;
    }
    return s;
}

module.exports.getAllRegisterdSt = async function (req, res) {
    var students = await studentregistration.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/admission/view_details', {
        students
    })
}

module.exports.createStudent = async  (req, res) => {
    student(req, res)
}

// var staff_id = staffID(7);
async function student(req, res) {
    try {
        // let stdInfo = joiValidation.newStudent.validateAsync(req.body)
        let stdInfo = await joiValidation.newStudentInfo.validateAsync(req.body)
        // res.send(stdInfo)
        var admittedStudent = {
            studentId: stdInfo.studentId,
            title: stdInfo.title,
            schoolId: req.user.organization_id,
            registered_by: req.user.name,
            surname: stdInfo.surname,
            firstname: stdInfo.firstname,
            middlename: stdInfo.middlename,
            gender:stdInfo.gender,
            dateofbirth: stdInfo.dateofbirth,
            className: stdInfo.className,
            classCategory: req.body.classCategory,
            guardian_name: stdInfo.guardian_name,
            guardian_address: stdInfo.guardian_address,
            phone: stdInfo.phone,
            yearOfEntry: stdInfo.yearOfEntry,
            previous_class:stdInfo.previous_class,
            previous_school: stdInfo.previous_school,
            yearOfGraduation: stdInfo.yearOfGraduation,
            guardian_email: stdInfo.guardian_email,
            photo : req.file.filename,
        };

        let newStudent = new studentregistration(admittedStudent);
        // console.log(newStudent)
        newStudent.save().then(function (result) {
            var createStudentLogin = {
                group_id: 555555,
                firstname: stdInfo.firstname,
                surname: stdInfo.surname,
                middlename: stdInfo.middlename,
                name: `${stdInfo.surname} ${stdInfo.surname} ${stdInfo.middlename}`,
                email: req.body.email,
                username: stdInfo.studentId,
                service_code: req.user.service_code,
                service_id: req.user.serviceid,
                user_phone: stdInfo.phone,
                organization_id: req.user.organization_id,
                password: "1234567",
                agency_id: result.student_reg_id
            }
            let newStudentLogin = new Users(createStudentLogin);
            newStudentLogin.save().then(() => {
               req.flash('info', "Student Successfully Registered");
               res.redirect('back')
            });
        })
    } catch (err) {
        req.flash('warning', err.message);
        res.redirect('back')
    }

}

module.exports.getStudentInfo = async function (req, res) {
    let studentID = req.query.admission;
    let info = await studentregistration.findOne({ where: { student_reg_id: studentID } });
    let schclass = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/admission/view_individual_info', {
        info, schclass
    });
}

// module.exports.getStudentUpdatePage = async function (req, res) {
//     let studentID = req.query.admissionid;
//     let info = await studentregistration.findOne({ where: { student_reg_id: studentID } });
//     res.render('', {
//         info
//     });
// }

module.exports.deleteStudentInfo = async function (req, res) {
    let studentID = req.query.admission;
    let info = await studentregistration.findOne({ where: { student_reg_id: studentID } });
    info.destroy().then(function (result) {
        req.flash('info', "succesfully deleted!");
        res.redirect('/admin/view_/registered_students')
    })
}
module.exports.getSearch = async function (req, res) {
    let classes = await classtable.findAll();
    res.render('./admin/admission/search_student', {
        students: "",
        classes
    })
}
module.exports.search = async function (req, res) {
    try {
        res.locals.clientName = " c.client";
        res.locals.clientCode = "c.service_code";
        res.locals.link = req.originalUrl;
        let students = await studentregistration.findAll({
            where: {
                [Op.or]: [
                    {
                        firstname: { [Op.like]: '%' + req.body.firstname + '%' },
                        gender: { [Op.like]: '%' + req.body.gender + '%' },
                        className: { [Op.like]: '%' + req.body.className + '%' },
                        classCategory: { [Op.like]: '%' + req.body.classCategory + '%' },
                        guardian_name: { [Op.like]: '%' + req.body.guardian_name + '%' },
                        // yearOfGraduation:{[Op.like]:'%' + req.body.yearOfGraduation + '%'},
                    }
                ]
            }
        });
        let classes = await classtable.findAll();
        res.render('./admin/admission/search_student', {
            students, classes
        })

    } catch (e) {

    }
}

module.exports.get_upload_page = async function (req, res) {
    var classname = await classtable.findAll({ where: { schoolId: req.user.organization_id } });

    res.render('./admin/admission/upload_students', {
        classname
    })
}
module.exports.upload_students = async function (req, res) {
    try {
        readXlsxFile('uploads/' + req.file.filename).then((rows) => {
            // `rows` is an array of rows
            // each row being an array of cells.   
            // console.log(rows);
            // Remove Header ROW
            rows.shift();
            let uploads = [];
            let studentlog = [];
            rows.forEach((row) => {
                let upload = {
                    serviceid: genID(12),
                    schoolId: req.user.organization_id,
                    registered_by: req.user.username,
                    className: req.body.className,
                    classCategory: req.body.classlevel,
                    previous_class: req.body.previous_class,
                    activity: 'active',
                    studentId: row[1],
                    surname: row[2],
                    firstname: row[3],
                    middlename: row[4],
                    dateofbirth: row[5],
                    gender: row[6],
                    guardian_name: row[7],
                    guardian_address: row[8],
                    phone: row[9],
                    yearOfEntry: row[11],
                    previous_school: row[10],
                    yearOfGraduation: row[12],
                    guardian_email: row[13]
                };
                let student = {
                    group_id: 555555,
                    surname: row[2],
                    firstname: row[3],
                    middlename: row[4],
                    name: `${row[2]} ${row[3]} ${row[4]}`,
                    email: `${row[3]}@${req.user.service_code}.com`,
                    username: `${row[1]}`,
                    user_phone: row[9],
                    service_code: req.user.service_code,
                    service_id: req.user.serviceid,
                    // user_phone: req.body.personal_phone_no,
                    organization_id: req.user.organization_id,
                    password: "1234567",
                    // agency_id: result.student_reg_id
                }
                uploads.push(upload);
                studentlog.push(student);
            });
            console.log(uploads)
            studentregistration.bulkCreate(uploads).then(result => {
                Users.bulkCreate(studentlog);
                var file = req.file.filename;
                var deleteFile = `./uploads/${file}`;
                fs.unlink(deleteFile, function (err) {
                    if (err) {
                        console.log(err.message)
                    }
                })
                req.flash('success', `Uploaded`);
                res.redirect('back')
            }).catch(err => {
                req.flash('success', `Filed ${err}`);
                res.redirect('back')
            });

        })
    } catch (err) {
        console.log(err)
    }

}

module.exports.parentLogin = async function (req, res) {
    var titles = await titletable.findAll();
    var parents = await parent.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/parents/create_parents_account', {
        titles, parents
    })
}

module.exports.createParentLogin = async function (req, res) {
    try {
        var addparent = {
            title: req.body.title,
            username: req.body.email,
            firstname: req.body.firstname,
            middlename: req.body.middlename,
            surname: req.body.surname,
            email: req.body.email,
            user_phone: req.body.user_phone,
            group_id: '444444',
            service_id: req.user.service_id,
            service_code: req.user.service_code,
            organization_id: req.user.organization_id
        }

        var newParentLogin = new Users(addparent);
        newParentLogin.save().then(function (result) {
            var parentt = {
                title: req.body.title,
                firstname: req.body.firstname,
                middlename: req.body.middlename,
                surname: req.body.surname,
                email: req.body.email,
                phone: req.body.user_phone,
                dob: req.body.dob,
                address: req.body.home_address,
                login_id: result.id,
                schoolId: req.user.organization_id,
                photo: req.file.filename,
            }
            var parents = new parent(parentt)
            parents.save();
            req.flash('info', "Account created Succesfully");
            res.redirect('back')
        }).catch(err => {
            // req.flash('danger', err.message);
            // res.redirect('back')
            console.log(err)
        })

    } catch (e) {
        // req.flash('danger', e.message);
        // res.redirect('back')
        console.log(e)
    }
}


module.exports.updateStudentPersonalInfo = async (req, res) => {

    try {
        let stdUpdetail = await joiValidation.updStudtInfo.validateAsync(req.body);
        let stdinfo = {
            surname: stdUpdetail.surname,
            firstname: stdUpdetail.firstname,
            middlename: stdUpdetail.middlename,
            gender: stdUpdetail.gender,
            dateofbirth: req.body.dateofbirth,
            guardian_name: stdUpdetail.guardian_name,
            guardian_address: stdUpdetail.guardian_address,
            phone: stdUpdetail.phone,
            guardian_email: stdUpdetail.guardian_email
        }
        let studentD = await studentregistration.findOne({ where: { student_reg_Id: stdUpdetail.id } });
        console.log(stdUpdetail)
        studentD.update(stdinfo, { new: true }).then(function (result) {
            res.json({ msg: 'success' });
        })
    } catch (err) {
        // req.flash('warning', err.message)
        res.json({ msg: err.message });
        // console.log(err.message)
    }

}

module.exports.updateAcademicInfo = async (req, res) => {
    try {
        let stdUpdetail = await joiValidation.updStudentAcademicInfo.validateAsync(req.body);
        let studentD = await studentregistration.findOne({ where: { student_reg_Id: stdUpdetail.id } });
        studentD.update(stdUpdetail, { new: true }).then(function (result) {
            res.json({ msg: 'success' });
        })
    } catch (err) {
        res.json({ msg: err.message });
    }
}

module.exports.uoloadStudentPassport = async (req, res) => {
    try {
        // console.log(req.query.studentid)

        let studentD = await studentregistration.findOne({ where: { student_reg_Id: req.query.studentid } });
        studentD.update({ photo: req.file.filename }, { new: true }).then(function (result) {
            req.flash('info', "Successfully Uploaded");
            res.redirect('back')
        })
    } catch (err) {
        req.flash('warning', err.message);
        res.redirect('back')
        // res.json({ msg: err.message });
    }
}