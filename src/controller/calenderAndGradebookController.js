const db = require('../../src/datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
// const { decodeBase64 } = require("bcryptjs");
const { calender_type, calender_type_periods, calender_year_period_setting, calender_year_setings, gradebook_columns, gradebook_template, gradebook_setting } = require("../models/calender/calenderModel");
const { schoolcategory, subjecttable, termsetup, shlsession } = require('../models/basicTablesModel');
const { classtable } = require('../models/classModel/classModel');
const { assessment } = require('../models/continuesassess/continuesassessmentModel');
const { studentRegistration } = require('./studentRegController');
const { studentregistration } = require('../models/studentModel/studentModel');

module.exports.calenderType = async function (req, res) {
    let calenderT = await calender_type.findAll({ where: { schoolid: req.user.organization_id } });
    res.render('./admin/calender/calender_type', {
        calenderT
    })
}

module.exports.addCanderType = async function (req, res) {
    try {
        let canlender = {
            CalenderTypeName: req.body.CalenderTypeName,
            duration: req.body.duration,
            schoolid: req.user.organization_id
        };
        let newCalenderType = new calender_type(canlender);
        newCalenderType.save();
        req.flash('info', 'Successful');
        res.redirect('/admin/settings/calender/calender_type')
    }
    catch (e) {
        req.flash('info', e.message);
        res.redirect('/admin/settings/calender/calender_type')
    }
}
module.exports.getUpdateCalenderType = async (req, res) => {
    let calender_id = req.query.calender_id;
    let calenderT = await calender_type.findOne({ where: { id: calender_id } });
    res.render('./admin/calender/calender_type_update', {
        calenderT, calender_id
    })
}
module.exports.UpdateCalenderType = async (req, res) => {
    let canlender = {
        CalenderTypeName: req.body.CalenderTypeName,
        duration: req.body.duration,
    };
    let calender_id = req.query.calender_id;
    let calenderT = await calender_type.findOne({ where: { id: calender_id } });
    calenderT.update(canlender, { new: true }).then(function (result) {
        req.flash('info', "Updated")
        res.redirect("back")
    }).catch(e => {
        req.flash('danger', e.message)
        res.redirect("back")
    })
    // res.render('./admin/calender/calender_type_update',)
}

module.exports.deleteCalenderType = async (req, res) => {
    let calender_id = req.query.calender_id;
    let calenderT = await calender_type.findOne({ where: { id: calender_id } });
    calenderT.destroy().then(function (result) {
        req.flash('danger', "deleted");
        res.redirect('back');
    })
}

module.exports.calenderTypePeriod = async function (req, res) {
    // let calendertypep = await calender_type_periods.findAll();
    console.log(req.user.service_id)
    let calenderT = await calender_type.findAll({ where: { schoolid: req.user.organization_id } });
    db.query(`select * from calender_type_periods INNER JOIN calender_type ON calender_type_periods.calendertype_id = calender_type.id where calender_type_periods.school_id = ${req.user.organization_id} `, { type: QueryTypes.SELECT })
        .then(function (calendertypep) {
            res.render('./admin/calender/calender_type_periods', {
                calendertypep, calenderT
            })
        })

}

module.exports.addCalenderTypePeriod = async function (req, res) {
    try {
        let calenderPeriod = {
            period_name: req.body.period_name,
            calendertype_id: req.body.calendertype_id,
            school_id: req.user.organization_id
        }
        let newCalenderPeriod = new calender_type_periods(calenderPeriod);
        newCalenderPeriod.save();
        req.flash('info', 'Success')
        res.redirect('/admin/settings/calender/calender_type_periods');
    } catch (e) {
        req.flash('danger', e.message);
        res.redirect('/admin/settings/calender/calender_type_periods');

    }
}

module.exports.getUpdateCalenderTypePeriod = async (req, res) => {
    let calenderperiodid = req.query.calenderperiodid;
    let calenderTP = await calender_type_periods.findOne({ where: { id_term: calenderperiodid } });
    let calenderT = await calender_type.findAll({ where: { schoolid: req.user.organization_id } });
    res.render("./admin/calender/calender_type_periods_upd", {
        calenderTP,
        calenderperiodid, calenderT
    })
}

module.exports.updateCalenderTypePeriod = async (req, res) => {
    try {
        let calenderPeriod = {
            period_name: req.body.period_name,
            calendertype_id: req.body.calendertype_id,
            school_id: req.user.organization_id
        };
        let calenderperiodid = req.query.calenderperiodid;
        let calenderTP = await calender_type_periods.findOne({ where: { id_term: calenderperiodid } });
        calenderTP.update(calenderPeriod, { new: true }).then(function (result) {
            req.flash('info', 'Successfully Updated');
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message);
        res.redirect('back')
    }
}

module.exports.deleteCalenderTypePeriod = async (req, res) => {
    let calenderperiodid = req.query.calenderperiodid;
    let calenderTP = await calender_type_periods.findOne({ where: { id_term: calenderperiodid } });
    calenderTP.destroy().then(function(result){
        req.flash('danger', 'Deleted!!');
        res.redirect('back')
    })
}


module.exports.calenderYearPeriodSettings = async function (req, res) {
    let calendertypep = await calender_type_periods.findAll();
    let calenderT = await calender_type.findAll({ where: { schoolid: req.user.organization_id } });
    let schoolCategories = await schoolcategory.findAll();
}
module.exports.addcalenderYearPeriodSettings = async function (req, res) {

}

module.exports.calender_year_settings = async function (req, res) {
    let canlenderyearsetup = await calender_year_setings.findAll({ where: { school_id: req.user.organization_id } });
    res.render('./admin/calender/calender_year_settings', {
        canlenderyearsetup
    })
}

module.exports.addcalender_year_settings = async function (req, res) {
    try {
        let yearsettings = {
            calender_year_name: req.body.calender_year_name,
            start_date: req.body.start_date,
            end_date: req.body.end_date,
            school_id: req.user.organization_id
        }
        let calenderYearettings = new calender_year_setings(yearsettings);
        calenderYearettings.save()
        req.flash('info', 'Success')
        res.redirect('/admin/settings/calender/calender_year_settings')
    } catch (e) {
        req.flash('danger', e.message)
        res.redirect('/admin/settings/calender/calender_year_settings')
        // console.log(e)
    }
}

// calender_year_settings  UPDATE
module.exports.getUpdateCalender_year_settings = async function (req, res) {
    let yearsetupid = req.query.yearsetupid;
    let canlenderyearsetup = await calender_year_setings.findOne({ where: { id_calender_year: yearsetupid } });
    res.render('./admin/calender/calender_year_settings_upd', {
        canlenderyearsetup, yearsetupid
    })
}
module.exports.UpdateCalender_year_settings = async function (req, res) {
    let yearsettings = {
        calender_year_name: req.body.calender_year_name,
        start_date: req.body.start_date,
        end_date: req.body.end_date,
    }
    let yearsetupid = req.query.yearsetupid;
    let canlenderyearsetup = await calender_year_setings.findOne({ where: { id_calender_year: yearsetupid } });
    canlenderyearsetup.update(yearsettings, { new: true }).then(result => {
        req.flash('info', "Updated!")
        res.redirect('back');
    })
    // res.render('./admin/calender/calender_year_settings_upd', {
    //     canlenderyearsetup
    // })
}
// calender_year_settings  delete
module.exports.deleteCalender_year_settings = async function (req, res) {
    let yearsetupid = req.query.yearsetupid;
    let canlenderyearsetup = await calender_year_setings.findOne({ where: { id_calender_year: yearsetupid } });
    canlenderyearsetup.destroy().then(function (result) {
        req.flash('danger', "Deleted!"),
            res.redirect('back')
    })
}

module.exports.calender_year_period_setting = async function (req, res) {
    let canlenderyearsetup = await calender_year_setings.findAll({ where: { school_id: req.user.organization_id } });
    let calendertypep = await calender_type_periods.findAll({ where: { school_id: req.user.organization_id } });
    let calenderT = await calender_type.findAll({ where: { schoolid: req.user.organization_id } });
    let schoolCategories = await schoolcategory.findAll();
    db.query(`SELECT * FROM calender_year_period_setting INNER JOIN calender_type_periods ON calender_year_period_setting.period_id = calender_type_periods.id_term
    INNER JOIN calender_year_setting ON calender_year_period_setting.calender_year_id = calender_year_setting.id_calender_year INNER JOIN schoolcategory ON
    calender_year_period_setting.schoolcategory_id = schoolcategory.schoolcategoryid where calender_year_period_setting.school_id = ${req.user.organization_id};`, { type: QueryTypes.SELECT }).then(function (YearPeriodSettings) {
        res.render('./admin/calender/calender_year_period_setting', {
            canlenderyearsetup, calendertypep, calenderT, schoolCategories, YearPeriodSettings
        })
    })

}

module.exports.addcalender_year_period_settings = async function (req, res) {
    try {
        let periodsetting = {
            period_id: req.body.period_id,
            calender_year_id: req.body.calender_year_id,
            period_start_date: req.body.period_start_date,
            period_end_date: req.body.period_end_date,
            schoolcategory_id: req.body.schoolcategory_id,
            no_of_days_opened: req.body.no_of_days_opened,
            next_term_begins: req.body.next_term_begins,
            no_of_weeks_opened: req.body.no_of_weeks_opened,
            school_id: req.user.organization_id
        };
        let newCalenderYearPeriodSettings = new calender_year_period_setting(periodsetting);
        newCalenderYearPeriodSettings.save();
        req.flash('success', 'Successful!!')
        res.redirect('/admin/settings/calender/calender_year_period_setting')
    } catch (e) {
        req.flash('success', e.message)
        res.redirect('/admin/settings/calender/calender_year_period_setting')
    }
}




// Grade Books 
module.exports.get_gradebook_columns = async function (req, res) {
    let gradebookcol = await gradebook_columns.findAll({ where: { school_id: req.user.organization_id } });
    res.render('./admin/gradebooks/gradebook_columns', {
        gradebookcol
    });
}

module.exports.add_gradebook_columns = async function (req, res) {
    try {
        let gradebookcolums = {
            gradebook_columns_title: req.body.gradebook_columns_title,
            school_id: req.user.organization_id,
            registered_by: req.user.name,
            registered_on: Date.now(),
        };
        let newGradebookcolumns = new gradebook_columns(gradebookcolums);
        newGradebookcolumns.save();
        req.flash('info', 'Gradebook Columns added!');
        res.redirect('/admin/settings/gradebook/gradebook_columns')
    } catch (e) {
        req.flash('danger', e.message);
        res.redirect('/admin/settings/gradebook/gradebook_columns')
    }
}

// get_gradebook_template
module.exports.get_gradebook_template = async function (req, res) {
    // let gradebooktemplate = await gradebook_template.findAll({where:{school_id: req.user.organization_id}});
    let gradebookcol = await gradebook_columns.findAll({ where: { school_id: req.user.organization_id } });
    let schoolCategory = await schoolcategory.findAll();
    db.query(`SELECT * from gradebook_template INNER JOIN schoolcategory on gradebook_template.schoolcategory_id = schoolcategory.schoolcategoryid 
    INNER JOIN gradebook_columns ON gradebook_template.gradebook_columnname_id = gradebook_columns.idgradebook_columns WHERE gradebook_template.school_id =${req.user.organization_id}`, { type: QueryTypes.SELECT })
        .then(function (result) {
            res.render('./admin/gradebooks/gradebook_template', {
                schoolCategory, gradebookcol, result
            });
        })
}

module.exports.add_gradebook_template = async function (req, res) {
    try {
        let gradebooktemplate = {
            school_year_period_id: req.body.school_year_period_id,
            school_id: req.user.organization_id,
            schoolcategory_id: req.body.schoolcategory_id,
            schoolclass_id: req.body.schoolclass_id,
            score: req.body.score,
            gradebook_columnname_id: req.body.gradebook_columnname_id,
            registered_by: req.user.registered_by,
            registered_on: Date.now(),
            templatename: req.body.templatename,
            template_order_id: req.body.template_order_id,
        };
        let newGradebooktemplate = new gradebook_template(gradebooktemplate);
        newGradebooktemplate.save();
        req.flash('info', 'Gradebook template  added!');
        res.redirect('/admin/settings/gradebook/gradebook_template')
    } catch (e) {
        req.flash('danger', e.message);
        res.redirect('/admin/settings/gradebook/gradebook_template')
    }
}


// gradebook_setting
module.exports.get_gradebook_setting = async function (req, res) {
    let gradebooksetting = await gradebook_setting.findAll({ where: { school_id: req.user.organization_id } });
    // let gradebookcol = await gradebook_columns.findAll({where:{school_id: req.user.organization_id}});
    // let year_period = await calender_year_period_setting.findAll({where:{school_id:req.user.organization_id}})
    let schoolCategory = await schoolcategory.findAll();
    db.query(`select * FROM calender_year_period_setting INNER JOIN calender_type_periods ON calender_year_period_setting.period_id = calender_type_periods.id_term WHERE calender_year_period_setting.school_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(year_period => {
        db.query(`SELECT * FROM gradebook_setting INNER JOIN schoolcategory ON gradebook_setting.schoolcategory_id = schoolcategory.schoolcategoryid
        INNER JOIN calender_year_period_setting ON gradebook_setting.calender_year_period_id = calender_year_period_setting.id_schoolterm INNER JOIN
        calender_type_periods ON calender_year_period_setting.period_id = calender_type_periods.id_term WHERE gradebook_setting.school_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(gradesettings => {
            res.render('./admin/gradebooks/gradebook_setings', {
                gradebooksetting, schoolCategory, year_period, gradesettings
            });
        })
    })

}

module.exports.add_gradebook_setting = async function (req, res) {
    try {
        let gradebooksetting = {
            school_id: req.user.organization_id,
            schoolcategory_id: req.body.schoolcategory_id,
            calender_year_period_id: req.body.calender_year_id,
            grade_title: req.body.grade_title,
            grade: req.body.grade,
            grade_min: req.body.grade_min,
            grade_max: req.body.grade_max,
            gradepoint: req.body.gradepoint,
            grade_color: req.body.grade_color,
            registered_by: req.user.registered_by,
        };
        let newGradebooksetting = new gradebook_setting(gradebooksetting);
        newGradebooksetting.save();
        req.flash('info', 'Gradebook settings  added!');
        res.redirect('/admin/settings/gradebook/gradebook_setings')
    } catch (e) {
        req.flash('danger', e.message);
        res.redirect('/admin/settings/gradebook/gradebook_setings')
    }
};

module.exports.getassessments = async function (req, res) {
    let classts = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let schoolCategorys = await schoolcategory.findAll();
    db.query(`SELECT * FROM assessment  INNER JOIN studentregistration on assessment.student_id = studentregistration.studentId  INNER JOIN classtable ON assessment.schoolclass_id = classtable.Id INNER JOIN subjecttable ON assessment.subject_id
    = subjecttable.Id INNER JOIN schoolcategory on assessment.schoolcategory_id = schoolcategory.schoolcategoryid INNER JOIN  termsetup ON 
    assessment.term_id = termsetup.setup_id WHERE assessment.school_id = ${req.user.organization_id} ORDER BY assessment.subject_id AND  assessment.schoolclass_id ASC`, { type: QueryTypes.SELECT }).then(results => {
        res.render('./admin/assessment/assessments', {
            classts, subjects, terms, schoolCategorys, results, schsession
        })
    })

}
module.exports.uploadCountiniousAssessment = async function (req, res) {
    let schsession = await shlsession.findOne({ where: { acdsession: req.body.schsession } })
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // `rows` is an array of rows
        // each row being an array of cells.   
        // console.log(rows);
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach(async(rows) => {

            
            // let subjscore = rows[1] + rows[2] + rows[3] + rows[4] + rows[5] + rows[6];
            // let scoredetails = await gradebook_setting.findOne({where: {}})
            let upload = {
                schoolclass_id: req.body.schoolclass_id,
               
                subject_id: req.body.subject_id,
                school_id: req.user.organization_id,
                term_id: req.body.term_id,
                schoolcategory_id: req.body.schoolcategory_id,
                class_id: req.body.schoolclass_id,
                school_session_id: schsession.id,
                session_id: req.body.schsession,
                studen_reg_no: rows[0],
                student_id:  rows[0],
                
                A1: rows[1],
                A2: rows[2],
                A3: rows[3],
                A4: rows[4],
                A5: rows[5],
                A6: rows[6],
                // term_subject_position: rows[25],
            };
            // console.log(upload)
            uploads.push(upload);
            // console.log(uploads.push(upload))
        });
        console.log(uploads)
        assessment.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
                req.flash('info', 'Uploaded')
                res.redirect('/admin/assessment/upload_assessment')
            })
            
        }).catch(err => {
            console.log(err)
        });

    })
}

async function student(id) {
    
    let student  = await studentregistration.findOne({ where: { studentId: rows[0] } });
    var id = student.student_reg_Id;
    return id
}