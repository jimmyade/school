const { staffregistration } = require('../../src/models/staffModel/staffmodel')
const {_lgas, _countries, _states, titletable, termsetup, subjecttable, _designations } = require('../../src/models/basicTablesModel')
const { Users } = require('../../src/models/usersMosel');
const { staffRegistration, infostaff, staffAcademicInfo } = require('../validation/joiValidation');
const db = require("../datab/db");
var bcrypt =  require('bcryptjs');
const saltRounds = 10;

module.exports.getApplicatepage = async function (req, res) {
    let applicant = await staffregistration.findAll({ order: [['staff_reg_id', 'DESC']], where:{schoolId: req.user.organization_id}});
    let titles = await titletable.findAll();
    let designations = await _designations.findAll()
    res.render('./admin/applicant/view_applicant_details', {
        applicant: applicant,
        titles, designations
    })
}

module.exports.createApplicant = async function (req, res) {
    try {
        var sercode = req.user.service_code.toUpperCase();
        function genID(len) {
            len = len || 100;
            var nuc = "0123456789";
            var i = 0;
            var n = 0;
            s = `${sercode}`;
            while (i <= len - 1) {
                n = Math.floor(Math.random() * 7);
                s += nuc[n];
                i++;
            }
            return s;
        }

        let stffInfo = await staffRegistration.validateAsync(req.body)
        var applicant = {
            staff_id : genID(8),
            schoolId : req.user.organization_id,
            title : stffInfo.title,
            surname : stffInfo.surname,
            firstname : stffInfo.firstname,
            middlename : stffInfo.middlename,
            gender : stffInfo.gender,
            dateofbirth : stffInfo.dateofbirth,
            activity: stffInfo.activity,
            email_address: stffInfo.email,
            kin_name : stffInfo.kin_name,
            kin_address : stffInfo.kin_address,
            kin_phone : stffInfo.kin_phone,
            registered_by : req.user.name,
            date_Of_Entry : stffInfo.date_Of_Entry,
            personal_phone_no : stffInfo.personal_phone_no,
            home_address : stffInfo.home_address,
            designation : stffInfo.designation,
            specialization : stffInfo.specialization,
            highest_qualification : stffInfo.highest_qualification,
            qualification_year : stffInfo.qualification_year,
            serviceid : req.user.service_id,
            photo : req.file.filename,
        };
    //    console.log(applicant)
        let newApplicant = new  staffregistration(applicant);
        newApplicant.save().then(function(result){
            newStaff(req, res);
            req.flash('info', "staff added!")
            res.redirect("back");
        })

    } catch (e) {
        // res.send(e.message)
        req.flash('warning', e.message)
        res.redirect("back");
    }
}

async function newStaff(req, res) {

    var createStaffLogin = {
        group_id : 333333,
        firstname : stffInfo.firstname,
        surname : stffInfo.surname,
        middlename : stffInfo.middlename,
        name: `${rstffInfo.surname} ${stffInfo.surname} ${stffInfo.middlename}`,
        email : stffInfo.email,
        username: stffInfo.email,
        user_phone : stffInfo.phone,
        service_code : req.user.service_code,
        service_id : req.user.serviceid,
        password : await bcrypt.hash(text, saltRounds),
        organization_id: req.user.organization_id
    }

    let  newStaffLogin = new Users(createStaffLogin);
    newStaffLogin.save()
   
}

module.exports.viewStaffDetails = async function(req, res) {
    var staffID = req.query.applicantno;
    let staff = await staffregistration.findOne({where:{staff_reg_id:staffID}});
    res.render('./admin/applicant/view_staff_info', {
        staff
    })
}

module.exports.updStaffInform = async (req, res) => {
    try{
        const staffInfo = await infostaff.validateAsync(req, res);
        let updUser = {
            surname: stdUpdetail.surname,
            firstname: stdUpdetail.firstname,
            middlename: stdUpdetail.middlename,
            username: stdUpdetail.email_address,
            email_address: stdUpdetail.email_address,
            personal_phone_no: stdUpdetail.personal_phone_no,
        };
        console.log(staffInfo)
        // let staff = await staffregistration.findOne({where:{staff_reg_id:staffInfo.staff_id}});
        // let staffUser = await Users.findOne({where: {user_phone: staff[0].personal_phone_no }});
        // staff.update(staffInfo, {new:true}).then(() => {
        //     staffUser.update(updUser, {new:true}).then(() => {
        //         res.json({ msg: 'success' });
        //     })
        // })
    } catch (err) {
        res.json({ msg: err.message });
    }
    

}


module.exports.staffAcdInfo = async (req, res) => {
    try{
        const staffInfo = await staffAcademicInfo.validateAsync(req, res);
        console.log(staffInfo)
    } catch (err) {
        res.json({ msg: err.message });
    }
}

module.exports.getStaffUpdatePage = async function(req, res) {
    var staffID = req.query.applicantno;
    let staff = await staffregistration.findOne({where:{staff_id:staffID}});
    res.render('', {
        staff
    })
}

// module.exports.updateStaffinfo = async function (req, res) {
//     try{
//         var staffID = req.query.applicantno;
//         let staffinfo = {
    
//         }
//         let staff = await staffregistration.findOne({where:{staff_id:staffID}});
//        staff.update(staffinfo, {new:true}).then(result => {
//            res.render('', {
    
//            })
//        })
//     } catch (e) {
//         req.flash('danger', e.message);
//         res.redirect('');
//     }
    
// }

module.exports.deleteStaff = async function(req, res) {
    var staffID = req.query.applicantno;;
    let staff = await staffregistration.findOne({where:{staff_id:staffID}});
   staff.destroy().then(function(result){
       req.flash('danger', "staff info deleted!");
       res.redirect('back')
   })
}