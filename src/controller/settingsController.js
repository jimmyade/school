const { titletable, termsetup, subjecttable, sessionsetting, shlsession, timetable, periodsettings, noticeboard, user_groups } = require('../../src/models/basicTablesModel');
const { classtable, result, scorerecord } = require('../../src/models/classModel/classModel');
const { teachersetting } = require('../../src/models/teacherModel/teachersModel')
const { staffregistration } = require('../../src/models/staffModel/staffmodel');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db');
const { termSetupV } = require('../validation/joiValidation');
//sesion settings
module.exports.sesionSetup = async function (req, res) {
    let sessioons = await sessionsetting.findAll({ order: [['id', 'DESC']], where: { schoolId: req.user.organization_id } });
    let classess = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll();
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/settings/sesionsetting', {
        sessioons: sessioons,
        classess: classess,
        subjects: subjects,
        terms: terms,
        acdsession: acdsession
    })
}

module.exports.createSesionSetup = async function (req, res) {
    try {
        let classess = await classtable.findOne({ where: { className: req.body.className } });
        let sessions = {
            className: req.body.className,
            subjects: req.body.subjects,
            term: req.body.term,
            sesion: req.body.name,
            schoolId: req.user.organization_id,
            classCode: classess.classCode
        }

        let Newsessionsetiings = new sessionsetting(sessions);
        Newsessionsetiings.save().then(function (result) {
            req.flash('info', `New Session has been setup for ${sessions.className}`)
            res.redirect('back')
        })
    } catch (e) {

    }
}

// subject settings

module.exports.getSubjectPage = async function (req, res) {
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/settings/subject', {
        subjects
    })
}

module.exports.createSubject = async function (req, res) {
    try {
        var sub = {
            subjectName: req.body.subject,
            schoolId: req.user.organization_id
            // registeredBy: req.user.name
        }
        let newSubject = new subjecttable(sub);
        newSubject.save().then(function (result) {
            req.flash('info', "New Subject added")
            res.redirect('back')
        })
    } catch (e) {

    }

}

// term setup
module.exports.getTermPage = async function (req, res) {
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/settings/termsetup', {
        terms, acdsession
    });

}

module.exports.AddTermSetup = async function (req, res) {
    try {
        let trmval = await termSetupV.validateAsync(req.body);
        let acdsession = await shlsession.findOne({ where: { id: trmval.sesion } });
        // let
        var terms = {
            term: trmval.term,
            sesion: acdsession.id,
            term_ends: trmval.term_ends,
            term_begins: trmval.term_begins,
            term_name: trmval.term_name,
            schoolId: req.user.organization_id,
            session: acdsession.acdsession,
            no_of_times_opened: trmval.no_of_times_opened
        }

        console.log(terms)
        let newTermsetup = new termsetup(terms);
        newTermsetup.save().then(() => {
            res.json({msg: "successful"})
        })
    } catch (e) {
        res.json({msg: e.message})
    }
}


module.exports.getUpdateTermSetup = async (req, res) => {
    var setting_term_id = req.query.setting_term_id;
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let term = await termsetup.findOne({ where: { setup_id: setting_term_id } });
    res.render('./admin/settings/termsetup_edit', {
        term, setting_term_id, acdsession
    })
}

module.exports.updateTermSetup = async (req, res) => {
    let acdsession = await shlsession.findOne({ where: { id: req.body.sesion } });
    var terms = {
        term: req.body.term,
        sesion: req.body.sesion,
        term_ends: req.body.term_ends,
        term_begins: req.body.term_begins,
        term_name: req.body.term_name,
        session: acdsession.acdsession,
        no_of_times_opened: req.body.no_of_times_opened
    }

    console.log(terms)

    var setting_term_id = req.query.setting_term_id;
    let term = await termsetup.findOne({ where: { setup_id: setting_term_id } });
    term.update(terms, { new: true }).then((result) => {
        req.flash('success', 'Updated')
        res.redirect('back')
    }).catch(e => {
        req.flash('danger', e.message)
        res.redirect('back')
    })

}

module.exports.deleteTermSetup = async (req, res) => {
    var setting_term_id = req.query.setting_term_id;
    let terms = await termsetup.findOne({ where: { setup_id: setting_term_id } });
    terms.destroy().then(function (result) {
        req.flash('danger', "Term setup deleted!");
        res.redirect('back');
    })
}

// teacher setup 
module.exports.getTeachersetup = async function (req, res) {
    var teachers = await teachersetting.findAll({ where: { schoolId: req.user.organization_id } });
    let staff = await staffregistration.findAll({ attributes: ['staff_reg_id', 'title', 'surname', 'firstname', 'middlename'], where: { schoolId: req.user.organization_id } });
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });
    let schoolclass = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    var terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/settings/teacherssettings', {
        teachers, staff, acdsession, subjects, schoolclass, terms
    })
}
module.exports.assignSubjectToTeacher = async function (req, res) {
    try {
        let assign = {
            staff_reg_id: req.body.staff_reg_id,
            // className: req.body.className,
            subjects: req.body.subjects,
            term: req.body.term,
            sesion: req.body.sesion,
            className: req.body.classname,
            schoolId: req.user.organization_id,
            registeredBy: req.user.registeredBy

        }
        let newTeachersetup = new teachersetting(assign);
        newTeachersetup.save().then(function (result) {
            req.flash('info', 'Added!')
            res.redirect('back');
        })
    } catch (e) {

    }
}

module.exports.getUpdateTeachersS = async (req, res) => {
    var settingsid = req.query.settingsid;
    var staff = await await teachersetting.findOne({ where: { schoolId: req.user.organization_id, staff_setting_id: settingsid } });
    var terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let teacher = await staffregistration.findAll({ attributes: ['staff_reg_id', 'title', 'surname', 'firstname', 'middlename'], where: { schoolId: req.user.organization_id } });
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });
    let schoolclass = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/settings/teacherssettings_up', {
        teacher, staff, acdsession, subjects, schoolclass, settingsid, terms
    })
}

module.exports.updateTeacherSettings = async (req, res) => {
    var settingsid = req.query.settingsid;
    let assign = {
        staff_reg_id: req.body.staff_reg_id,
        className: req.body.className,
        subjects: req.body.subjects,
        term: req.body.term,
        sesion: req.body.sesion,
    }
    console.log(assign)
    var teachers = await teachersetting.findOne({ where: { schoolId: req.user.organization_id, staff_setting_id: settingsid } })
    teachers.update(assign, { new: true }).then(function (result) {
        req.flash('info', 'Successfully updated!')
        res.redirect('back')
    })
}

module.exports.deleteTeacherSettings = async (req, res) => {
    var settingsid = req.query.settingsid;
    var teachers = await teachersetting.findOne({ where: { schoolId: req.user.organization_id, staff_setting_id: settingsid } })
    teachers.destroy().then(result => {
        req.flash('danger', "Deleted Successful!");
        res.redirect('back')
    })
}


// get class

module.exports.getTimeTableClass = async (req, res) => {
    let classMember = await classtable.findAll({ where: { schoolId: req.user.organization_id } }),
        acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    // periods = await periodsettings

    res.render('./admin/timetable/get_class', {
        classMember, acdsession
    })

}

// time table controller 
module.exports.getTimeTablePage = async (req, res) => {
    var terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } }),
        acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } }),
        teachers = await staffregistration.findAll({ attributes: ['staff_reg_id', 'title', 'surname', 'firstname', 'middlename'], where: { schoolId: req.user.organization_id } }),
        subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });

    classt = await classtable.findOne({ where: { schoolId: req.user.organization_id, Id: req.query.classid } })
    // console.log(req.query.session)
    res.render('./admin/timetable/create_time_table', {
        terms, acdsession, teachers, subjects, classt, acdsession: req.query.session
    })
}
// new time table 
module.exports.addTimeTable = async (req, res) => {
    try {
        var item1 = [req.body],
            classid = req.body.classid;
        let periodss = await periodsettings.findOne({ where: { acdsessions: req.body.sessionN, schoolId: req.user.organization_id } });

        let uploads = [];
        for (var a of item1) {
            let subject = a.subjectId,
                staff = a.StaffId,
                altstaff = a.AltStaffId,
                Week = a.WeekDay,
                period = a.PeriodNum,
                // periodid = period.id,
                startT = a.PeriodTimeStart,
                endT = a.PeriodTimeEnd,
                date = a.PeriodDate;

            for (var i = 0; i < subject.length; i++) {
                var SUBI = subject[i];
                var STI = staff[i];
                var ASI = altstaff[i];
                var PEI = period[i];
                var WEI = Week[i];
                // var PDI = periodid[i];
                var PTS = startT[i];
                var PIE = endT[i];
                var DATE = date[i];

                let upload = {
                    subjectId: SUBI,
                    StaffId: STI,
                    AltStaffId: ASI,
                    PeriodNum: PEI,
                    WeekDay: WEI,
                    periodid: periodss.id,
                    PeriodTimeStart: PTS,
                    PeriodTimeEnd: PIE,
                    PeriodDate: DATE,
                    ClassId: classid,
                    schoolId: req.user.organization_id,
                    acdsesion: req.body.sessionN
                };

                uploads.push(upload)
            }
        }
        console.log(uploads)
        timetable.bulkCreate(uploads).then(function (result) {
            req.flash('info', 'Created Successful');
            res.redirect(`/admin/create/school_general_time_table?classid=${classid}$session=${req.body.sessionN}`)
        })
    } catch (e) {
        console.log(e)
        req.flash('info', e.message);
        res.redirect(`/admin/create/school_general_time_table?classid=${classid}$session=${req.body.sessionN}`)

    }
}


module.exports.viewTimeTable = async (req, res) => {
    let timetable = await db.query(`
    SELECT * from timetable 
        INNER JOIN periodSettings ON timetable.periodid = periodSettings.id
        INNER JOIN staffregistration ON timetable.StaffId = staffregistration.staff_reg_id
        INNER JOIN subjecttable on timetable.subjectId = subjecttable.Id
        INNER JOIN classtable on timetable.ClassId = classtable.Id
        where timetable.schoolId = ${req.user.organization_id} AND timetable.acdsesion = '2019/2020'`, { type: QueryTypes.SELECT })
    res.render('./admin/timetable/view_time_table', {
        timetable
    })
}

module.exports.viewPeriodDuration = async (req, res) => {
    let period = await periodsettings.findAll({ where: { schoolId: req.user.organization_id } }),
        acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/settings/periodsettings', {
        period, acdsession
    })
}

module.exports.addPeriodDuration = async (req, res) => {
    try {
        let prd = {
            periods: req.body.periods,
            duraton: req.body.duration,
            acdsessions: req.body.sessionid,
            schoolId: req.user.organization_id,

        }
        var period = await periodsettings.findAll({ where: { acdsessions: req.body.sessionid, schoolId: req.user.organization_id } })
        if (period !== "") {
            req.flash('info', 'already exist')
            res.redirect('back')
        }
        let newPeriod = new periodsettings(prd);
        newPeriod.save().then(result => {
            req.flash('info', 'Succesfully')
            res.redirect('back')
        })
    } catch (err) {
        req.flash('info', err.message)
        res.redirect('back')
    }
}

// school notice board
module.exports.getSchoolNotice = async (req, res) => {
    let notices = await db.query(`SELECT * from noticeboard INNER JOIN user_groups on noticeboard.viewby = user_groups.group_id where noticeboard.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    let viewers = await user_groups.findAll()
    res.render('./admin/settings/notice_board', {
        notices, viewers
    })
}
module.exports.addSchoolNotice = async (req, res) => {
    try {
        // var staff = await staffregistration.findOne({where: {personal_phone_no: req.user.user_phone}})
        // console.log(staff)
        let items = [req.body];
        let uploads = [];
        for (var a of items) {
            var notice = a.notice;
            var date_time = a.date_time;
            var viewby = a.viewby
            for (var i = 0; i < notice.length; i++) {
                let Not = notice[i];
                let DT = date_time[i];
                let VB = viewby[i];

                let upload = {
                    registeredBy: req.user.username,
                    notice: Not,
                    date: DT,
                    schoolId: req.user.organization_id,
                    viewby: VB
                };
                uploads.push(upload)
            }
        }

        console.log(uploads)
        noticeboard.bulkCreate(uploads).then(function(result) {
            req.flash('info', "Notice added");
            res.redirect('back')
        }).catch(err =>{
            req.flash('info', err.message);
            res.redirect('back')
        })
        
    } catch (err) {
        req.flash('info', err.message);
            res.redirect('back')
    }
}