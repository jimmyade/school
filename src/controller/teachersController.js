const db = require('../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;

const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');


const { classtable } = require("../models/classModel/classModel")
const { continiousassesment, assessment } = require("../models/continuesassess/continuesassessmentModel")
const { socialdevelopment, pychomotorskills, workhabit, attendance, attentionskills, punctuality } = require("../models/performance/performanceModel")
const { studentregistration, } = require("../models/studentModel/studentModel");
const { subjecttable, termsetup, schoolcategory } = require('../models/basicTablesModel');
const { teachersetting } = require('../models/teacherModel/teachersModel');





module.exports.teachersDashboard = async function (req, res) {
    let subjectTeacher = await db.query(`SELECT * FROM teachersetting INNER JOIN staffregistration ON teachersetting.staff_reg_id = staffregistration.staff_reg_id 
    WHERE staffregistration.personal_phone_no = ${req.user.user_phone}`, { type: QueryTypes.SELECT });
    let notices = await db.query(`SELECT * from noticeboard INNER JOIN user_groups on noticeboard.viewby = user_groups.group_id where noticeboard.schoolId = ${req.user.organization_id} AND user_groups.group_id = 333333 OR user_groups.group_id = 101010`, { type: QueryTypes.SELECT });
    // let classcount = await studentregistration.count({where: {className:}})
    res.render('./teacher/dashboard', {
        subjectTeacher, notices
    })
}

module.exports.studentRecords = async function (req, res) {
    let teachesettings = await db.query(`SELECT classsettings.className as classname from classsettings INNER JOIN staffregistration ON classsettings.staff_reg_id = classsettings.staff_reg_id 
    where staffregistration.personal_phone_no = ${req.user.user_phone}` , { type: QueryTypes.SELECT });
    // console.log(teachesettings[0].classname)
    let studentRecords = await studentregistration.findAll({ where: { className: teachesettings[0].classname, schoolId: req.user.organization_id } })
    
    res.render('./teacher/records/student_records', {
        studentRecords,
        classname : teachesettings[0].classname
    })
}

module.exports.attendanceRecord = async function (req, res) {
    let student = await studentregistration.findAll({ attributes: ['surname', 'firstname', 'middlename'], where: { className: 'Nursery Two Charis', schoolId: req.user.organization_id } });
    res.render('./teacher/attendance/attendance', {
        student
    })
}


module.exports.continiousAssessments = async function (req, res) {
    let assessment = await continiousassesment.findall({ where: { subject } })
}


module.exports.workHabit = async function (req, res) {
    var count = await workhabit.count({where: { schoolId:req.user.organization_id}});
    var rows = await db.query(`SELECT * from workhabit INNER JOIN studentregistration on studentregistration.student_reg_Id = workhabit.student_reg_Id where  workhabit.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // var count = await workhabit.count();
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/performance/workhabit', {
        workH: rows, classt, count
    });
}

module.exports.addWorkHabit = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var habit = {
            className: classn.className,
            student_reg_Id: req.body.student,
            term: req.body.term,
            sesion: req.body.sesion,
            care_for_materials: req.body.care_for_materials,
            follows_instructions: req.body.follows_instructions,
            works_independently: req.body.works_independently,
            does_not_disturb: req.body.does_not_disturb,
            completes_task: req.body.completes_task,
            // classname : req.body.classname,
        }

        let newWorkhabit = new workhabit(habit);
        newWorkhabit.save().then(function (result) {
            req.flash('info', `Work habit successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}

module.exports.attentionsSkils = async function (req, res) {
    var count = await attentionskills.count({where: { schoolId:req.user.organization_id}});
    var rows = await db.query(`SELECT * from attentionskills INNER JOIN studentregistration on studentregistration.student_reg_Id = attentionskills.student_reg_Id where  attentionskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // var count = await workhabit.count();
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/performance/attentionskils', {
        attention: rows, classt, count
    });
}

module.exports.addAttentionsSkils = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var skills = {
            className: classn.className,
            student_reg_Id: req.body.student,
            term: req.body.term,
            sesion: req.body.sesion,
            concentration_for_3_minutes: req.body.concentration_for_3_minutes,
            concentration_for_5_minutes: req.body.concentration_for_5_minutes,
            concentration_for_10_minutes: req.body.concentration_for_10_minutes,
            easily_distracted: req.body.easily_distracted,
            respond_to_correction: req.body.respond_to_correction,
            overcome_difficulties: req.body.overcome_difficulties,
            enjoy_listening: req.body.enjoy_listening,
            willing_to_paticipate: req.body.willing_to_paticipate,
            // classname : req.body.classname,
            // classname : req.body.classname,
        }

        let newAttentionskils = new attentionskills(skills);
        newAttentionskils.save().then(function (result) {
            req.flash('info', `Attention Skills successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}

module.exports.pychomotorSkills = async function (req, res) {
    var count = await pychomotorskills.count({where: { schoolId:req.user.organization_id}});
    var rows = await db.query(`SELECT * from pychomotorskills INNER JOIN studentregistration on studentregistration.student_reg_Id = pychomotorskills.student_reg_Id where  pychomotorskills.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // var count = await workhabit.count();
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/performance/pychomotorskills', {
        pychomotorskills: rows, classt, count
    });
}

module.exports.addPychomotorskills = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var skills = {
            className: classn.className,
            student_reg_Id: req.body.student,
            term: req.body.term,
            schoolId: req.user.organization_id,
            sesion: req.body.sesion,
            drawing_and_painting: req.body.drawing_and_painting,
            games: req.body.games,
            flexibility: req.body.flexibility,
            verbal_fluency: req.body.verbal_fluency,
            overall_progress: req.body.overall_progress,
        }

        let newPychomotorskills = new pychomotorskills(skills);
        newPychomotorskills.save().then(function (result) {
            req.flash('info', `Pychomotor Skills successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}


module.exports.punctuality = async function (req, res) {
    var count = await punctuality.count({where: { schoolId:req.user.organization_id}});
    var rows = await db.query(`SELECT * from punctuality INNER JOIN studentregistration on studentregistration.student_reg_Id = punctuality.student_reg_Id where  punctuality.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // var count = await workhabit.count();
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/performance/punctuality', {
        punctuality: rows, classt, count
    });
}

module.exports.addPunctuality = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var skills = {
            className: classn.className,
            student_reg_Id: req.body.student,
            schoolId: req.user.organization_id,
            term: req.body.term,
            sesion: req.body.sesion,
            no_student_attended: req.body.no_student_attended,
            totals: req.body.totals,
        }

        let newPunctuality = new punctuality(skills);
        newPunctuality.save().then(function (result) {
            req.flash('info', `Punctuality successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}


module.exports.socialdevelopment = async function (req, res) {
    var count = await pychomotorskills.count({where: { schoolId:req.user.organization_id}});
    var rows = await db.query(`SELECT * from socialdevelopment INNER JOIN studentregistration on studentregistration.student_reg_Id = socialdevelopment.student_reg_Id where  socialdevelopment.schoolId = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    // var count = await workhabit.count();
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/performance/socialdevelopment', {
        social: rows, classt, count
    });
}

module.exports.addSocialdevelopment = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var skills = {
            className: classn.className,
            student_reg_Id: req.body.student,
            term: req.body.term,
            sesion: req.body.sesion,
            schoolId: req.user.organization_id,
            is_courteous: req.body.is_courteous,
            shows_respect: req.body.shows_respect,
            self_control: req.body.self_control,
            respond_to_correction: req.body.respond_to_correction,
            relates_to_adults: req.body.relates_to_adults
        }

        let newSocialdevelopment = new socialdevelopment(skills);
        newSocialdevelopment.save().then(function (result) {
            req.flash('info', `social development successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}
continiousassesment

module.exports.continiousAssesment = async function (req, res) {
    let { count, rows } = await assessment.findAndCountAll({where: { schoolId:req.user.organization_id}})
    let subjects = await subjecttable.findAll({ where: { schoolId:req.user.organization_id } })
    let classt = await classtable.findAll({where: { schoolId:req.user.organization_id}});
    console.log(count)
    res.render('./teacher/continiousassessment/continiousassesment', {
        assessments: rows, classt, count, subjects
    });
}

module.exports.addContiniousAssesment = async function (req, res) {
    try {
        let classn = await classtable.findOne({ where: { id: req.body.classname } });
        var skills = {
            className: classn.className,
            student_reg_Id: req.body.student,
            term: req.body.term,
            sesion: req.body.sesion,
            schoolId: req.user.organization_id,
            is_courteous: req.body.is_courteous,
            shows_respect: req.body.shows_respect,
            self_control: req.body.self_control,
            respond_to_correction: req.body.respond_to_correction,
            relates_to_adults: req.body.relates_to_adults
        }

        let newSocialdevelopment = new socialdevelopment(skills);
        newSocialdevelopment.save().then(function (result) {
            req.flash('info', `social development successfully submited!`)
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }

}


module.exports.getstudentsbyclass = async function (req, res) {
    var classid = req.query.classassesmentid;
    let classifo = await classtable.findOne({ where: { id: classid, schoolId:req.user.organization_id } });
    let { count, rows } = await studentregistration.findAndCountAll({ where: { className: classifo.className, schoolId:req.user.organization_id } });
    res.render('./teacher/continiousassessment/getstudentbyclass', {
        students: rows, classid, count, classifo
    });
}

module.exports.addContiniousAssesmentPage = async function (req, res) {
    let classts = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll()
    let schoolCategorys = await schoolcategory.findAll();
    db.query(`SELECT * FROM assessment  INNER JOIN studentregistration on assessment.student_id = studentregistration.student_reg_Id  INNER JOIN classtable ON assessment.schoolclass_id = classtable.Id INNER JOIN subjecttable ON assessment.subject_id
    = subjecttable.Id INNER JOIN schoolcategory on assessment.schoolcategory_id = schoolcategory.schoolcategoryid INNER JOIN  termsetup ON 
    assessment.term_id = termsetup.setup_id WHERE assessment.school_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(results => {
        res.render('./teacher/continiousassessment/assessment_form', {
            classts, subjects, terms, schoolCategorys, results
        })
    })
   
}
module.exports.getStudents = function (req, res) {
    var classid = req.params.classid;
    classtable.findOne({ where: { id: classid,schoolId:req.user.organization_id } }).then(function (lassresult) {
        studentregistration.findAll({ where: { className: lassresult.className, schoolId:req.user.organization_id } }).then(function (result) {
            res.send(result);
        })
    })
}


module.exports.uploadCountiniousAssessment = async function(req, res) {
    readXlsxFile('uploads/' + req.file.filename).then((rows) => {
        // `rows` is an array of rows
        // each row being an array of cells.   
        // console.log(rows);
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach((rows) => {
            let upload = {
                schoolclass_id: req.body.schoolclass_id,
                subject_id: req.body.subject_id,
                school_id: req.user.organization_id,
                term_id: req.body.term_id,
                schoolcategory_id: req.body.schoolcategory_id,
                class_id: req.body.schoolclass_id,
                school_session_id: schsession.id,
                session_id: req.body.schsession,
                studen_reg_no: rows[0],
                student_id:  rows[0],
                
                A1: rows[1],
                A2: rows[2],
                A3: rows[3],
                A4: rows[4],
                A5: rows[5],
                A6: rows[6],
            };
            uploads.push(upload);
        });
        console.log(uploads)
        assessment.bulkCreate(uploads).then(result => {
            var file = req.file.filename;
            var deleteFile = `./uploads/${file}`;
            fs.unlink(deleteFile, function (err) {
                if (err) {
                    console.log(err.message)
                }
            })
            res.send(result)
        }).catch(err => {
            console.log(err)
        });

    })
}


