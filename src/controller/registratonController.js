var bcrypt = require('bcryptjs');;
const saltRounds = 10;
// const UserModel = require('../../models/client_service_and_bufferModel');
const { client_services } = require('../../src/models/client_service_and_bufferModel');
const { _states, _residential_address_status, _countries, _lgas, _cities, user_groups } = require('../../src/models/basicTablesModel');
const request = require('request');
// const { _cities, _countries,_lgas}= require('../../models/taxPayersModels/texPayersmodels');
// const { cache } = require('ejs');
const { Users } = require('../../src/models/usersMosel');
// require('dotenv').config()

const nodemailer = require("nodemailer");
// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    //host: "smtp.mailtrap.io",
    //port: 2525,
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    secure: false, // use SSL
    auth: {
        // user: "709b0038a45240",
        // pass: "dad9baf5795764"
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

module.exports.getClientRegPage = async function (req, res) {
    _countries.findAll().then(function (country) {
        console.log(process.env.EMAIL_USER)
        res.render('./registration/create_account', {
            country,
            err: '', Error: '',
            // link:''

        })
    });
}

module.exports.clientRegistration = async function (req, res) {
    res.locals.login = req.originalUrl;
    try {
        res.locals.link = req.originalUrl;
        const client = req.body.client;
        const service_code = req.body.service_code;
        const client_phone = req.body.client_phone;

        const client_email = req.body.client_email;
        const country = req.body.country_id;
        const state = req.body.state_id;

        const lga = req.body.lga_id;
        const city = req.body.city_id;
        const admin_surname = req.body.admin_surname;

        const admin_firstname = req.body.admin_firstname;
        const admin_middlename = req.body.admin_middlename;
        const client_admin_phone = req.body.client_admin_phone;

        const client_admin_email = req.body.client_admin_email;
        const client_address = req.body.client_address;
        const supervisor_surname = req.body.supervisor_surname;

        const supervisor_firstname = req.body.supervisor_firstname;
        const supervisor_middlename = req.body.supervisor_middlename;
        const client_supervisor_phone = req.body.client_supervisor_phone;

        const client_supervisor_email = req.body.client_supervisor_email;
        const client_admin_pwd = "1234567";
        const client_supervisor_pwd = "1234567";
        const service_logo = req.file.filename;
        var Error = [];

        await CreateNewClient({ client, service_code, client_phone, client_email, country, state, lga, city, admin_surname, admin_firstname, admin_middlename, client_admin_phone, client_admin_email, client_address, supervisor_surname, supervisor_firstname, supervisor_middlename, client_supervisor_phone, client_supervisor_email, client_admin_pwd, client_supervisor_pwd, service_logo });

    } catch (err) {
        _countries.findAll().then(function (country) {
            res.render("./registration/create_account", {
                err: err,
                country: country,
                Error: '',
                // link
            });
        });
    }

    async function _Encrypt(text) {
        return await bcrypt.hash(text, saltRounds);
    }

    async function CreateNewClient(args) {
        if (args.client_admin_pwd) {
            args.client_admin_pwd = await _Encrypt(args.client_admin_pwd);
        }
        if (args.client_supervisor_pwd) {
            args.client_supervisor_pwd = await _Encrypt(args.client_supervisor_pwd);
        }
        return await CreateClient(args)
    }
    async function CreateClient(args) {
        try {
            if (!args.client) Error.push('Invalid argument: client');
            // if (!args.service_code) Error.push({ msg: 'Invalid argument: first_name' });
            if (!args.client_phone) Error.push({ msg: 'Invalid argument: client_phone' });
            if (!args.client_email) Error.push({ msg: 'Invalid argument: client_email' });
            if (!args.country) Error.push({ msg: 'Invalid argument: country_id' });

            if (!args.state) Error.push({ msg: 'Invalid argument: state' });
            if (!args.lga) Error.push({ msg: 'Invalid argument: lga' });
            // if(!args.city_id)  Error.push({msg:'Invalid argument: city_id'});
            if (!args.admin_surname) Error.push({ msg: 'Invalid argument: admin_surname' });
            if (!args.admin_firstname) Error.push({ msg: 'Invalid argument: admin_firstname' });

            // if (!args.admin_middlename) Error.push({ msg: 'Invalid argument: admin_middlename' });
            if (!args.client_admin_phone) Error.push({ msg: 'Invalid argument: client_admin_phone' });
            // if (!args.client_admin_email) Error.push({ msg: 'Invalid argument: client_admin_email' });
            if (!args.client_address) Error.push({ msg: 'Invalid argument: client_address' });
            // if (!args.supervisor_surname) Error.push({ msg: 'Invalid argument: supervisor_surname' });

            // if (!args.supervisor_firstname) Error.push({ msg: 'Invalid argument: supervisor_firstname' });
            // if (!args.supervisor_middlename) Error.push({ msg: 'Invalid argument: supervisor_middlename' });
            // if (!args.client_supervisor_phone) Error.push({ msg: 'Invalid argument: client_supervisor_phone' });
            // if (!args.client_supervisor_email) Error.push({ msg: 'Invalid argument: client_supervisor_email' });
            if (!args.client_supervisor_pwd) Error.push({ msg: 'Invalid argument: client_supervisor_pwd' });
            if (!args.client_admin_pwd) Error.push({ msg: 'Invalid argument: client_admin_pwd' });
            if (!args.service_logo) Error.push({ msg: 'Invalid argument: service_logo' });
            if (Error.length > 0) {
                _countries.findAll().then(function (country) {
                    res.render("./registration/create_account", {
                        err: "",
                        country: country,
                        Error: Error,
                        // link
                    });
                });
            } else {
                var str = args.client;
                var matches = str.match(/\b(\w)/g); //['j', 's', 'o', 'n']
                var acronym = matches.join(''); //json
                var acronyml = acronym.toLowerCase();
                console.log(acronym);
                let RegisterClient = new client_services({
                    client: args.client,
                    service_code: acronyml,
                    client_phone: args.client_phone,
                    client_email: args.client_email,
                    country: args.country,
                    state: args.state,
                    lga: args.lga,
                    city: args.city,
                    admin_surname: args.admin_surname,
                    admin_firstname: args.admin_firstname,
                    admin_middlename: args.admin_middlename,
                    client_admin_phone: args.client_admin_phone,
                    // client_admin_email: args.client_admin_email,
                    client_address: args.client_address,
                    supervisor_surname: 'suppervisor',
                    supervisor_firstname: "suppervisor",
                    // supervisor_middlename: args.supervisor_middlename,
                    // client_supervisor_phone: args.client_supervisor_phone,
                    // client_supervisor_email: args.client_supervisor_email,
                    client_admin_pwd: args.client_admin_pwd,
                    client_supervisor_pwd: args.client_supervisor_pwd,
                    setup_by: Date.now(),
                    service_logo: args.service_logo
                });
                RegisterClient.save().then(client_se => {
                    // console.log(client_services.service_id)
                    Users.findOne({ where: { service_id: client_se.service_id } }).then(user => {

                        var message = `Dear Subscriber, the following is your login detail, username: admin@${acronyml}, password: 1234567 Thank you for signing up.`;
                        var phoneNumber = args.username;
                        var smsSender = `YAFD`;
                        request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phoneNumber}&body=${message}&dnd=2`, function (error, ress, body) {
                            if (error) {
                                console.error('error:', error); // Print the error if one occurred
                            } else {
                                console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
                                console.log('body:', body); // Print the HTML for the Google homepage.
                                req.flash('info', " Account created Successfully, your login details has been sent to your client phone number inputed during registration");
                                res.redirect('back')
                            }


                        });
                    })
                })

            }
        } catch (err) {
            req.flash('success', err.message);
            res.redirect('back')
        }
    }
}

module.exports.userLoginControl = async function (req, res) {

    // let userG = await user_groups.findOne({where:{  group_id: req.user.group_id}});
    if (req.user.group_id == 111111) {
        res.redirect('/admin/dashboard')
    } else if (req.user.group_id == 222222) {
        res.redirect('/api_supervisor/dashboard')
    } else if (req.user.group_id == 333333) {
        res.redirect('/api/teachers/dashboard')
    }
    else if (req.user.group_id == 444444) {
        res.redirect('/api/parents/dashboard')
    } else if (req.user.group_id == 555555) {
        res.redirect('/api_student/dashboard')
    }
}


