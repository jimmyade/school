const db = require('../../src/datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { classtable, result, school_classes } = require('../../src/models/classModel/classModel')
const { subjecttable, subject_groups, schoolcategory, shlsession } = require('../models/basicTablesModel');
const { staffregistration } = require('../models/staffModel/staffmodel');
const { addclassVal } = require('../validation/joiValidation');
// const { SELECT } = require('sequelize/types/lib/query-types');

module.exports.GetCreateClassPage = async function (req, res) {
    let classt = await db.query(`SELECT * from classtable INNER JOIN schoolcategory on classtable.classCategory = schoolcategory.schoolcategoryid where classtable.schoolId = ${req.user.organization_id} `, { type: QueryTypes.SELECT });
    let categories = await schoolcategory.findAll();
    let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/settings/class_setup', {
        classt, categories, acdsession
    })
}

module.exports.CreateClass = async  (req, res) => {
    try {

        // let validate = await addclassVal.validateAsync()
        var item = [req.body];
        let uploads = [];
        let schcl = [];
        for (var a of item) {
            var classname = a.className;
            for (var i = 0; i < classname.length; i++) {
                var LL = classname[i];
                let upload = {
                    className: LL,
                    classCategory: req.body.schCategory,
                    schoolId: req.user.organization_id,
                    registeredBy: req.user.username
                };
                uploads.push(upload);
                // schcl.push(schClass)
            }
        }
        console.log(uploads);
        classtable.bulkCreate(uploads).then(result => {
            req.flash('primary', "Class created!")
            res.redirect('back')
        })
    } catch (e) {
        req.flash('primary', e.message)
        res.redirect('back')
    }
}


module.exports.getEdit = async (req, res) => {
    let categories = await schoolcategory.findAll();
    let classt = await classtable.findOne({where: {Id: req.query.class_id}})
    res.render('./admin/settings/update_class', {
        classt , categories
    })
}

module.exports.updateClass = async (req, res) => {
    let newDetails = {
        className: req.body.className,
        classCategory: req.body.schCategory
    }
    let classt = await classtable.findOne({where: {Id: req.query.class_id}});
    let schclass = await school_classes.findOne({where: {schoolclass_id: req.query.class_id}});
    schclass.update(newDetails, {new:true});
    classt.update(newDetails, {new:true}).then(() => {
        req.flash('info', "Updated!!")
        res.redirect('/admin/create_and_view_classes')
    })
    
    // res.render('', {
    //     classt
    // })
}

module.exports.deleteClass = async (req, res) => {
    // let categories = await schoolcategory.findAll();
    let classt = await classtable.findOne({where: {Id: req.query.class_id}})
    let sholcla = await school_classes.findOne({where: {schoolclass_id: req.query.class_id}})
    classt.destroy().then(() => {
        sholcla.destroy()
        req.flash('info', "Destroyed!!")
        res.redirect('/admin/create_and_view_classes')
    })
}

module.exports.courseSubject = async function (req, res) {
    let subjects = await subjecttable.findAll();
    let classt = await classtable.findAll();
    let teachers = await staffregistration.findAll({ attributes: ['staff_reg_id', 'surname', 'firstname', 'middlename'], where: { schoolId: req.user.organization_id, } });
    res.render('./admin/subject/create_subject_group', {
        classt, subjects, teachers
    })
}

module.exports.addCourseSubject = async function (req, res) {
    try {
        var item1 = [req.body];
        var item2 = [req.body];
        var item3 = [req.body];

        let uploads = [];
        for (var a of item1) {
            for (var b of item2) {
                for (var c of item3) {
                    var classid = a.classid;
                    var subjectid = b.subject;
                    var teacherid = c.teacher_id;

                    for (var i = 0; i < classid.length; i++) {
                        var CI = classid[i];
                        var SI = subjectid[i];
                        var TI = teacherid[i];

                        let upload = {
                            created_at: Date.now(),
                            class_id: CI,
                            // class_group_id: req.body.class_group_id,
                            subject_id: SI,
                            teacher_id: TI,
                            school_id: req.user.organization_id
                        };
                        uploads.push(upload);
                    }

                }
            }
        }
        console.log(uploads);
        subject_groups.bulkCreate(uploads).then(result =>{
            req.flash('info', 'Successful');
            res.redirect('back')
        })
    } catch (err) {
        req.flash('info', err.message);
        res.redirect('back')
    }
}

module.exports.getCourseSubjectByClass = async function (req, res) {
    var class_id = req.params.classid;
    db.query(`SELECT * from subject_groups INNER JOIN subjecttable ON  subjecttable.id = subject_groups.subject_id INNER JOIN staffregistration ON  staffregistration.staff_reg_id = subject_groups.teacher_id where subject_groups.class_id = ${class_id} and subject_groups.school_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(result => {
        res.send(result)
    })

}