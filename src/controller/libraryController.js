const db = require('../datab/db')

const fs = require('fs');


const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { lms_book_issue, lms_suppliers_details, lms_book_details, lms_members } = require("../models/library/library");
const { studentRegistration } = require('./studentRegController');
const { studentregistration } = require('../models/studentModel/studentModel');

module.exports.dashbaord = async (req, res) => {
    const library = await lms_book_issue.findAll({ where: { schoolid: req.user.organization_id } });
    res.render('./library/library', {
        library
    })
}

// add book controller
module.exports.getAddBookPage = async (req, res) => {
    let books = await db.query('SELECT * from lms_book_details INNER JOIN lms_suppliers_details on lms_suppliers_details.SUPPLIER_ID = lms_book_details.SUPPLIER_ID', { type: QueryTypes.SELECT });
    let suppliers = await lms_suppliers_details.findAll();
    res.render('./library/book_detail', {
        books, suppliers
    })
}
module.exports.addBook = async (req, res) => {
    try {
        function genID(len) {
            len = len || 100;
            var nuc = "0123456789";
            var i = 0;
            var n = 0;
            s = "BL";
            while (i <= len - 1) {
                n = Math.floor(Math.random() * 7);
                s += nuc[n];
                i++;
            }
            return s;
        }
        let BOOK_CODE = genID(6)
        let books = {
            BOOK_CODE: BOOK_CODE,
            BOOK_TITLE: req.body.BOOK_TITLE,
            CATEGORY: req.body.CATEGORY,
            AUTHOR: req.body.AUTHOR,
            PUBLICATION: req.body.PUBLICATION,
            PUBLISH_DATE: req.body.PUBLISH_DATE,
            BOOK_EDITION: req.body.BOOK_EDITION,
            PRICE: req.body.PRICE,
            RACK_NUM: `${req.body.RACK_NUM}`,
            DATE_ARRIVAL: req.body.DATE_ARRIVAL,
            SUPPLIER_ID: req.body.SUPPLIER_ID,
        }
        console.log(books)
        var newBook = new lms_book_details(books);
        newBook.save().then(function (result) {
            req.flash('info', "New Book Added");
            res.redirect('/api/labrary/book_registration')
        })

    } catch (e) {
        req.flash('info', e.message);
        res.redirect('/api/labrary/book_registration')
    }
}


module.exports.viewBookDetail = async (req, res) => {
    let bcode = req.query.book_code
    let info = await lms_book_details.findOne({ where: { BOOK_CODE: bcode } });
    res.render('./library/b_detail', {
        info
    })
}

module.exports.updateMslBook = async (req, res) => {
    try {
        var deatial = req.body;
        let info = await lms_book_details.findOne({ where: { BOOK_CODE: deatial.BOOK_CODE } });
        console.log(info)
        info.update(deatial, { new: true }).then(function (result) {
            res.json({ msg: 'Successfully Updated' });
        })
    } catch (err) {
        res.json({ msg: err.message });
    }
}


module.exports.updatePhoto = async (req, res) => {
    let info = await lms_book_details.findOne({ where: { BOOK_CODE: req.query.ebook_code } });
    info.update({ photo: req.file.filename }, { new: true }).then(() => {
        var PrevFile = './uploads/' + info.photo;
        fs.unlink(PrevFile, function (err) {
            if (err) {
                console.log(err)
                req.flash('info', 'Uploaded!!');
                res.redirect('back');
            } else {
                req.flash('info', 'Uploaded!!');
                res.redirect('back');
            }
        })
    })
}
// member
module.exports.getLmMember = async (req, res) => {
    let members = await lms_members.findAll();
    let stud = await studentregistration.findAll({ where: { schoolid: req.user.organization_id } })
    res.render('./library/lms_library_member', {
        members, stud
    })
}

module.exports.LmMember = async (req, res) => {
    try {
        let stud = await studentregistration.findOne({ where: { student_reg_Id: req.body.MEMBER_ID } })
        // let members = await lms_members.findOne({where:{MEMBER_ID: req.body.MEMBER_ID}});
        let member = {
            MEMBER_ID: stud.student_reg_Id,
            MEMBER_NAME: `${stud.surname} ${stud.firstname} ${stud.middlename}`,
            CITY: req.body.CITY,
            DATE_REGISTER: req.body.DATE_REGISTER,
            DATE_EXPIRE: req.body.DATE_EXPIRE,
            MEMBERSHIP_STATUS: req.body.MEMBERSHIP_STATUS,
        }
        // console.log(member)
        let newMember = new lms_members(member);
        newMember.save().then(function (result) {
            req.flash('info', 'Member Created');
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', v.message);
        res.redirect('back')
    }
}

// supplier controller 
module.exports.getSupplierpage = async (req, res) => {
    let suppliers = await lms_suppliers_details.findAll();
    res.render('./library/supplier', {
        suppliers
    })
}
module.exports.addNewSupplier = async (req, res) => {
    try {
        function genID(len) {
            len = len || 100;
            var nuc = "0123456789";
            var i = 0;
            var n = 0;
            s = "S";
            while (i <= len - 1) {
                n = Math.floor(Math.random() * 7);
                s += nuc[n];
                i++;
            }
            return s;
        }
        let supplierid = genID(11)
        let supplier_details = {
            SUPPLIER_ID: supplierid,
            SUPPLIER_NAME: req.body.SUPPLIER_NAME,
            ADDRESS: req.body.ADDRESS,
            CONTACT: req.body.CONTACT,
            EMAIL: req.body.EMAIL,
            schoolid: req.user.organization_id
        };
        let newSupplier = new lms_suppliers_details(supplier_details);
        newSupplier.save().then(function (result) {
            req.flash('info', 'Supplier Added!!')
            res.redirect('back');
        })
    } catch (e) {
        req.flash('info', e.message)
        res.redirect('back')
    }
};

module.exports.getUpdateSupplier = async (req, res) => {
    var supplier = req.query.supplie_id;
    let suppl = await lms_suppliers_details.findOne({ where: { SUPPLIER_ID: supplier } })
    res.render('', {
        suppl
    })
}

module.exports.updateSupplier = async (req, res) => {
    var supplier = req.query.supplie_id;
    let supplier_details = {
        SUPPLIER_ID: supplierid,
        SUPPLIER_NAME: req.body.SUPPLIER_NAME,
        ADDRESS: req.body.ADDRESS,
        CONTACT: req.body.CONTACT,
        EMAIL: req.body.EMAIL,
        schoolid: req.user.organization_id
    };
    let suppl = await lms_suppliers_details.findOne({ where: { SUPPLIER_ID: supplier } })
    suppl.update(supplier_details, { new: true }).then(function (result) {
        req.flash('info', 'Supplier Detail Updated');
        res.redirect('back')
    })
        .catch(err => {
            req.flash('info', err.message)
            res.redirect('back')
        })

}

module.exports.deleteSupplier = async (req, res) => {
    var supplier = req.query.supplie_id;
    let suppl = await lms_suppliers_details.findOne({ where: { SUPPLIER_ID: supplier } })
    suppl.destroy().then(function (result) {
        req.flash('danger', 'Supplier deleted')
        res.redirect('back')
    })
}


module.exports.getIssuedBook = async (req, res) => {
    let issuedBooks = await lms_book_issue.findAll();
    let books = await lms_book_details.findAll()
    res.render('./library/lms_issued_book', {
        issuedBooks, books
    })
}

module.exports.addIssueBook = async (req, res) => {
    try {
        let issuedBooks = {
            BOOK_ISSUE_NO: BOOK_ISSUE_NO,
            BOOK_CODE: req.body.BOOK_CODE,
            DATE_ISSUE: req.body.DATE_ISSUE,
            DATE_RETURN: req.body.DATE_RETURN,
            MEMBER_ID: req.body.MEMBER_ID,
        };

        let newIssuedBook = await lms_book_issue(issuedBooks);
        newIssuedBook.save().then(function (result) {
            req.flash('info', 'Books issued successfully registered');
            res.redirect('back')
        })
    } catch (e) {
        req.flash('info', e.message);
        res.redirect('back')
    }
}