const db = require('../datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { classtable, school_classes } = require("../models/classModel/classModel")
const { continiousassesment, assessment } = require("../models/continuesassess/continuesassessmentModel")
const { socialdevelopment, pychomotorskills, workhabit, attendance, attentionskills, punctuality } = require("../models/performance/performanceModel")
const { studentregistration, } = require("../models/studentModel/studentModel");
const { subjecttable, shlsession, termsetup } = require('../models/basicTablesModel');
const { exam } = require('../models/examModel/exammodel');


module.exports.examPanel = async function (req, res) {
    res.render('./admin/exam_panel', {

    })
}

module.exports.getCreateExampage = async function (req, res) {
    let classt = await classtable.findAll();
    res.render('./admin/exam/exam', {
        classt
    })
}

module.exports.getExamClass = async function (req, res) {
    let classid = req.query.generated;
    let classt = await classtable.findOne({ where: { Id: classid, schoolId: req.user.organization_id } });
    let subjects = await subjecttable.findAll({ where: { schoolId: req.user.organization_id } });
    let term_session = await shlsession.findAll({ where: { schoolId: req.user.organization_id } });
    let terms = await termsetup.findAll();
    let realExm = await exam.count({ where: { schoolId: req.user.organization_id, className: classid } });
    let exams = await db.query(` SELECT classtable.className as classN, termsetup.term as term, shlsessions.acdsession as asession, subjecttable.subjectName as subjectN  from exam LEFT JOIN classtable ON exam.className = classtable.Id LEFT JOIN subjecttable ON exam.subjects = subjecttable.Id
    LEFT JOIN termsetup ON exam.term = termsetup.setup_id LEFT JOIN shlsessions ON exam.sesion = shlsessions.id WHERE exam.schoolId = ${req.user.organization_id} AND exam.className = ${classid}`, { type: QueryTypes.SELECT });
    res.render('./admin/exam/create_exam', {
        classt, classid, subjects, term_session, terms, exams, realExm
    })
}

module.exports.addExamClassSubjects = async function (req, res) {
    try {
        var classid = req.query.id;
        var yearsession = req.body.yearsession;
        var term = req.body.term;
        var subj = [req.body];
        let uploads = [];
        for (var n of subj) {
            var m = n.subject;
            for (var i = 0; i < m.length; i++) {
                var mm = m[i];
                let upload = {
                    className: classid,
                    subjects: mm,
                    term: term,
                    sesion: yearsession,
                    schoolId: req.user.organization_id
                };
                uploads.push(upload);
            }
        }
        console.log(uploads)
        exam.bulkCreate(uploads).then(function (result) {
            req.flash('info', 'Exam Subject created');
            res.redirect('/admin/exam/exam_management/create_exam');
        }).catch(err => {
            req.flash('danger', err.mesage);
            res.redirect('/admin/exam/exam_management/create_exam');
        })

    } catch (e) {
        req.flash('danger', e.mesage);
        res.redirect('/admin/exam/exam_management/create_exam');
    }
}


// EXAMS REPORT 
module.exports.examSummarypage = async (req, res) => {
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./reports/exam_report/search_result_summary', {
        schsession, terms
    })
}

module.exports.examSummary = async (req, res) => {
    var ses = req.body.schsession;
    var tem = req.body.term_id;
    let result = await db.query("SELECT DISTINCT assessment.schoolclass_id, classCategory, className FROM `classtable` RIGHT JOIN `assessment` ON `assessment`.`schoolclass_id` = `classtable`.`Id` GROUP BY assessment.schoolclass_id ;", { type: QueryTypes.SELECT });
    let result1 = await db.query("SELECT `subjectName`,  subject_id FROM `subjecttable` INNER JOIN `assessment` ON `subjecttable`.`Id` = `assessment`.`subject_id` WHERE   assessment.term_id ='" + tem + "' AND assessment.session_id = '" + ses + "' GROUP BY subjectName ORDER BY subject_id ASC;", { type: QueryTypes.SELECT })
    let result2 = await db.query(`
        SELECT
        studentregistration.surname,
        studentregistration.firstname,
        studentregistration.middlename,
        studentregistration.studentId,
        studentregistration.class_id,
        studentregistration.student_reg_Id,
        assessment.student_id AS studsids
        
        FROM
            (
                SELECT DISTINCT
                    student_id
                FROM
                    assessment
                WHERE
                    assessment.term_id = '${tem}'
                AND assessment.session_id = '${ses}'
            ) assessment
        LEFT JOIN (
            SELECT
                *
            FROM
                studentregistration
        ) studentregistration ON assessment.student_id = studentregistration.studentId
        ` , { type: QueryTypes.SELECT });


    let calresults = await db.query("SELECT student_id, term_id, subject_id,  subject_score  From assessment where  assessment.term_id='" + tem + "' AND assessment.session_id='" + ses + "' ORDER BY subject_id ASC", { type: QueryTypes.SELECT })
    let terms = await termsetup.findOne({ where: { setup_id: tem } });
    res.render('./reports/exam_report/result_summary', {
        schclass: result, subjects: result1, student_details: result2, calresults, tem, ses, terms
    })

    // res.send({
    //     result:result, result1:result1, student_details:result2, calresults:calresults
    // })
};


// CLASS EXAM REPORT 
module.exports.classExamReport = async (req, res) => {
    let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
    let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
    let classt = await school_classes.findAll({ where: { school_id: req.user.organization_id } });
    res.render('./reports/exam_report/search_class_result', {
        schsession, terms, classt, error: ''
    })
}


module.exports.examReport = async (req, res) => {
    var ses = req.body.schsession;
    var tem = req.body.term_id;
    var cls_id = req.body.cls_id;

    let result2 = await db.query(`
        SELECT
        studentregistration.surname,
        studentregistration.firstname,
        studentregistration.middlename,
        studentregistration.studentId,
        studentregistration.class_id,
        studentregistration.student_reg_Id,
        assessment.student_id AS studsids
        FROM
            (
                SELECT DISTINCT
                    student_id
                FROM
                    assessment
                WHERE
                    assessment.term_id = '${tem}'
                AND assessment.session_id = '${ses}'
                AND assessment.schoolclass_id = '${cls_id}'
            ) assessment
        LEFT JOIN (
            SELECT
                *
            FROM
                studentregistration
        ) studentregistration ON assessment.student_id = studentregistration.studentId 
        ` , { type: QueryTypes.SELECT });


    let calresults = await db.query("SELECT student_id, term_id, subject_id,  subject_score  From assessment where assessment.schoolclass_id='" + cls_id + "' AND  assessment.term_id='" + tem + "' AND assessment.session_id='" + ses + "' ORDER BY subject_score DESC", { type: QueryTypes.SELECT })

    let terms = await termsetup.findOne({ where: { setup_id: tem } });
    let classt = await school_classes.findOne({ where: { schoolclass_id: cls_id } });
    res.send({ student_details: result2, calresults, ses, termname: terms.term, classname: classt.className })


}

module.exports.resultSheet = async (req, res) => {
    var studentid = req.query.studentsid;
    var term = req.query.tem;
    var termid = req.query.temid;
    var sess = req.query.sesions;
    var studentp = req.query.position;
    var clssid = req.query.classid;
    var error = [];
    let student = await studentregistration.findOne({ where: { studentId: studentid } })
    let result1 = await db.query("SELECT `subjectName`,  subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title FROM `subjecttable` INNER JOIN `assessment` ON `subjecttable`.`Id` = `assessment`.`subject_id` WHERE  assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id ='" + termid + "' AND assessment.session_id = '" + sess + "' GROUP BY subjectName ORDER BY subject_id ASC;", { type: QueryTypes.SELECT })

    let calresults = await db.query("SELECT student_id, term_id, subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title  From assessment where assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id='" + termid + "' AND assessment.session_id='" + sess + "' ORDER BY subject_score DESC", { type: QueryTypes.SELECT })
    let classN = await classtable.findOne({ where: { Id: clssid } })
    var termdetail = await termsetup.findOne({ where: { term: term, schoolId: req.user.organization_id, session: sess } })
    let puntual = await punctuality.findOne({ where: { student_reg_Id: student.student_reg_Id, term: term, sesion: sess } });
    let attention = await attentionskills.findOne({ where: { student_reg_Id: student.student_reg_Id, term: term, sesion: sess } });
    let pycho = await pychomotorskills.findOne({ where: { student_reg_Id: student.student_reg_Id, term: term, sesion: sess } });
    let workh = await workhabit.findOne({ where: { student_reg_Id: student.student_reg_Id, term: term, sesion: sess } });

    if (puntual == null) {
        error.push({ msg: 'Puntuality skills records not found' })
    }
    if (attention == null) {
        error.push({ msg: 'Attention skills records not found' })
    }
    if (pycho == null) {
        error.push({ msg: 'Pychomotor skills records not found' })
    }
    if (workh == null) {
        error.push({ msg: 'Work habit skills records not found' })
    }
    if (error.length > 0) {
        let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
        // let lads = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
        let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
        let classt = await school_classes.findAll({ where: { school_id: req.user.organization_id } });
        res.render('/reports/exam_report/search_class_result', {
            lads, schsession, terms, classt, error
        })
    } else {
        res.render('./reports/exam_report/result_sheets/result_sheet1', {
            studentid, term, session: sess, studentp, student, result1, calresults,
            puntual, attention, pycho, workh, termdetail, classN
        })
    }

}


//  // parents view wards result
// module.exports.classExamReport = async (req, res) => {
//     let kids = await studentregistration.findAll({ where: { guardian_email: req.user.username } });
//     let schsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
//     let terms = await termsetup.findAll({ where: { schoolId: req.user.organization_id } });
//     let classt = await school_classes.findAll({ where: { school_id: req.user.organization_id } });
//     res.render('./reports/exam_report/parent_search_ward_result', {
//         kids, schsession, terms, classt
//     })
// }

// module.exports.printWardsResult = async (req, res) => {
//     var studentid = req.query.studentsid;
//     var term = req.query.tem;
//     var termid = req.query.temid;
//     var sess = req.query.sesions;
//     var studentp = req.query.position;
//     var clssid = req.query.classid;
//     let student = await studentregistration.findOne({where: {studentId: studentid}})
//     let result1 = await db.query("SELECT `subjectName`,  subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title FROM `subjecttable` INNER JOIN `assessment` ON `subjecttable`.`Id` = `assessment`.`subject_id` WHERE  assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id ='" + termid + "' AND assessment.session_id = '" + sess + "' GROUP BY subjectName ORDER BY subject_id ASC;", { type: QueryTypes.SELECT })

//     let calresults = await db.query("SELECT student_id, term_id, subject_id, a1, a2, a3, a4, a5, a6,  subject_score, grade_title  From assessment where assessment.student_id ='" + studentid + "' AND assessment.schoolclass_id='" + clssid + "' AND  assessment.term_id='" + termid + "' AND assessment.session_id='" + sess + "' ORDER BY subject_score DESC", { type: QueryTypes.SELECT })
//     let classN = await classtable.findOne({where: {Id: clssid}})
//     var termdetail = await termsetup.findOne({where: {term:term, schoolId:req.user.organization_id, session:  sess}})
//     let puntual = await punctuality.findOne({where:{student_reg_Id:student.student_reg_Id, term:term, sesion:sess}});
//     let attention = await attentionskills.findOne({where:{student_reg_Id:student.student_reg_Id, term:term, sesion:sess}});
//     let pycho = await pychomotorskills.findOne({where:{student_reg_Id:student.student_reg_Id, term:term, sesion:sess}});
//     let workh = await workhabit.findOne({where:{student_reg_Id:student.student_reg_Id, term:term, sesion:sess}});
//     res.render('./reports/exam_report/result_sheets/result_sheet1', {
//       studentid, term, session:sess, studentp, student, result1, calresults,
//       puntual, attention, pycho, workh, termdetail, classN
//     })
// }


