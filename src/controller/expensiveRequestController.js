const db = require('../../src/datab/db')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const { _designations } = require("../models/basicTablesModel");
const { expense_heads, expense_limit_level, expense_limit_setup } = require("../models/inventory/expenseModel");
const { staffregistration } = require('../models/staffModel/staffmodel');
const { inventory_item_categories, inventory_items, raw_material_inventory, raw_material_orders, raw_material_requests } = require('../models/inventory/inventoryModel');

function generatedID(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
        n = Math.floor(Math.random() * 4);
        s += nuc[n];
        i++;
    }
    return s;
}


module.exports.getExpensiveHead = async function (req, res) {
    let expensiveHead = await expense_heads.findAll({ where: { service_id: req.user.organization_id } });
    res.render('./inventory/expensive/expense_heads', {
        expensiveHead
    })
}

module.exports.addExpenseHead = async function (req, res) {
    try {
        var expenseHead = [req.body];
        var expenseDes = [req.body];
        var code = parseInt(generatedID(6));
        //    console.log(expenseHead)
        let uploads = [];
        let expcode = 1
        for (var a of expenseHead) {
            for (var b of expenseDes) {
                var expHead = a.expense_head;
                var expDescription = b.description;
                for (var i = 0; i < expHead.length; i++) {
                    var EH = expHead[i];
                    var ED = expDescription[i];
                    expcode = code + i;
                    let upload = {
                        expense_head: EH,
                        description: ED,
                        expense_code: expcode,
                        service_id: req.user.organization_id,
                    };
                    uploads.push(upload);
                }
            }
        }
        console.log(uploads);
        expense_heads.bulkCreate(uploads).then(result => {
            req.flash('primary', "expense heads successfully added!")
            res.redirect('back')
        })
    } catch (e) {
        console.log(e)
    }
}

module.exports.getExpense_limit_level = async function (req, res) {
    let expLimitLevel = await expense_limit_level.findAll();
    res.render('./inventory/expensive/expense_limit_level', {
        expLimitLevel
    })
}

module.exports.addExpense_limit_level = async function (req, res) {
    try {
        var limit_level = [req.body];
        //    console.log(expenseHead)
        let uploads = [];
        for (var a of limit_level) {
            var limitSlevel = a.expense_limit_level;
            for (var i = 0; i < limitSlevel.length; i++) {
                var LL = limitSlevel[i];
                let upload = {
                    expense_limit_level: LL,
                    created_by: req.user.username,
                };
                uploads.push(upload);
            }
        }
        console.log(uploads);
        expense_limit_level.bulkCreate(uploads).then(result => {
            req.flash('primary', "expense limit level successfully added!")
            res.redirect('back')
        })
    } catch (e) {
        console.log(e)
    }
}

module.exports.getExpense_limit_setup = async function (req, res) {
    let designation = await _designations.findAll();
    let staffs = await staffregistration.findAll({ where: { schoolId: req.user.organization_id } });
    let limitLevel = await expense_limit_level.findAll();
    let expHead = await expense_heads.findAll({ where: { service_id: req.user.organization_id } });
    let limitsetup = await db.query(`SELECT * FROM expense_limit_setup LEFT JOIN staffregistration ON expense_limit_setup.employee_no = staffregistration.staff_id
    LEFT JOIN expense_heads ON expense_limit_setup.expense_head_id = expense_heads.expense_head_id LEFT JOIN _designations ON 
    expense_limit_setup.designation_id = _designations.designation_id WHERE expense_limit_setup.service_id = ${req.user.organization_id}`, { type: QueryTypes.SELECT });
    res.render('./inventory/expensive/expense_limit_setup', {
        designation, limitLevel, expHead, limitsetup, staffs
    })
}

module.exports.addExpense_limit_setup = async function (req, res) {
    try {
        var employee = [req.body];
        var expense_head = [req.body];
        var limit_level = [req.body];
        var designation = [req.body];
        var limit_amount = [req.body];

        let uploads = [];
        let expcode = 1
        for (var a of expense_head) {
            for (var b of limit_level) {
                for (var c of designation) {
                    for (var d of limit_amount) {
                        for (var e of employee) {
                            var expHead = a.expense_head_id;
                            var limitlevel = b.expense_limit_level;
                            var design = c.designation_id;
                            var limitamount = d.limit_amount;
                            var EmplNumb = e.employee_no;
                            for (var i = 0; i < expHead.length; i++) {
                                var EH = expHead[i];
                                var LL = limitlevel[i];
                                var D = design[i];
                                var LM = limitamount[i];
                                var EN = EmplNumb[i];

                                let upload = {
                                    expense_head_id: EH,
                                    expense_limit_level: LL,
                                    designation_id: D,
                                    limit_amount: LM,
                                    employee_no: EN,
                                    service_id: req.user.organization_id,
                                };
                                uploads.push(upload);
                            }
                        }
                    }
                }
            }
        }
        console.log(uploads);
        expense_limit_setup.bulkCreate(uploads).then(result => {
            req.flash('primary', "expense limit setup successfully added!")
            res.redirect('back')
        })
    } catch (e) {
        console.log(e)
    }
}
// inventory

module.exports.get_inventory_item_categories = async function (req, res) {
    let inventItem = await inventory_item_categories.findAll({ where: { service_id: req.user.organization_id } });
    res.render('./inventory/inventory/inventory_item_categories', {
        inventItem
    })
}

module.exports.add_inventory_item_categories = async function (req, res) {
    try {
        var parentItem = [req.body];
        var itemCategory = [req.body];
        var designation = [req.body];

        let uploads = [];
        for (var a of parentItem) {
            for (var b of itemCategory) {
                for (var c of designation) {
                    var parent_item_category = a.parent_item_category;
                    var item_category = b.item_category;
                    var description = c.description;

                    for (var i = 0; i < parent_item_category.length; i++) {
                        var PIC = parent_item_category[i];
                        var IC = item_category[i];
                        var D = description[i];


                        let upload = {
                            parent_item_category: PIC,
                            item_category: IC,
                            description: D,
                            service_id: req.user.organization_id,
                        };
                        uploads.push(upload);
                    }

                }
            }
        }
        console.log(uploads);
        inventory_item_categories.bulkCreate(uploads).then(result => {
            req.flash('primary', "successfully added!")
            res.redirect('back')
        })

    } catch (e) {

    }
}

module.exports.get_inventory_items = async function (req, res) {
    let inventoryI = await inventory_items.findAll();
    let rawmaterialInv = await raw_material_inventory.findAll({ where: { service_id: req.user.organization_id } })
    res.render('./inventory/inventory/raw_material_inventory', {
        inventoryI, rawmaterialInv
    })
}
module.exports.add_inventory_items = async function (req, res) {

    var item1 = [req.body];
    var item2 = [req.body];
    var item3 = [req.body];
    var item4 = [req.body];

    let uploads = [];
    for (var a of item1) {
        for (var b of item2) {
            for (var c of item3) {
                for (var d of item4) {
                    var itmeid = a.item_id;
                    var minmeas = b.min_measurement;
                    var maxmeas = c.max_measurement;
                    var quant = d.quantity;

                    for (var i = 0; i < itmeid.length; i++) {
                        var II = itmeid[i];
                        var NM = minmeas[i];
                        var MM = maxmeas[i];
                        var Q = quant[i];

                        let upload = {
                            item_id: II,
                            min_measurement: NM,
                            max_measurement: MM,
                            quantity: Q,
                            service_id: req.user.organization_id,
                        };
                        uploads.push(upload);
                    }

                }
            }
        }
    }
    console.log(uploads);
    raw_material_inventory.bulkCreate(uploads).then(result => {
        req.flash('primary', "successfully added!")
        res.redirect('back')
    }).catch(err => {
        req.flash('danger', err.message);
        res.redirect('back')
    })
}


module.exports.get_raw_material_orders = async function (req, res) {
    let inventoryI = await inventory_items.findAll();
    let staffs = await staffregistration.findAll({ where: { schoolId: req.user.organization_id } });
    let rawmaterialOrders = await db.query(`SELECT * FROM raw_material_orders LEFT JOIN staffregistration ON raw_material_orders.user_id = staffregistration.staff_id LEFT JOIN inventory_items ON raw_material_orders.item_id = inventory_items.item_id WHERE raw_material_orders.service_id =  ${req.user.organization_id};`, {type:QueryTypes.SELECT});
    res.render('./inventory/inventory/raw_material_orders', {
        inventoryI, rawmaterialOrders, staffs
    })
}
module.exports.add_raw_material_orders = async function (req, res) {

    var item1 = [req.body];
    var item2 = [req.body];
    var item3 = [req.body];


    let uploads = [];
    for (var a of item1) {
        for (var b of item2) {
            for (var c of item3) {
                var itmeid = a.item_id;
                var userid = b.user_id;
                var quant = c.quantity;

                for (var i = 0; i < itmeid.length; i++) {
                    var II = itmeid[i];
                    var UI = userid[i];
                    var Q = quant[i];

                    let upload = {
                        item_id: II,
                        user_id: `${UI}`,
                        quantity: Q,
                        service_id: req.user.organization_id,
                    };
                    uploads.push(upload);
                }

            }
        }
    }
    console.log(uploads);
    raw_material_orders.bulkCreate(uploads).then(result => {
        req.flash('primary', "successfully added!")
        res.redirect('back')
    })
}


module.exports.get_raw_material_request = async function (req, res) {
    let inventoryI = await inventory_items.findAll();
    let staffs = await staffregistration.findAll({ where: { schoolId: req.user.organization_id } });
    let rawmaterialOrders = await db.query(`SELECT * FROM raw_material_requests LEFT JOIN staffregistration ON raw_material_requests.user_id = staffregistration.staff_id LEFT JOIN inventory_items ON raw_material_requests.product_item_id = inventory_items.item_id WHERE raw_material_requests.service_id =  ${req.user.organization_id};`, {type:QueryTypes.SELECT});
    res.render('./inventory/inventory/raw_material_requests', {
        inventoryI, rawmaterialOrders, staffs
    })
}
module.exports.add_raw_material_request = async function (req, res) {

    var item1 = [req.body];
    var item2 = [req.body];
    var item3 = [req.body];
    var code = parseInt(generatedID(6));
    

    let uploads = [];
    for (var a of item1) {
        for (var b of item2) {
            for (var c of item3) {
                var itmeid = a.product_item_id;
                var userid = b.user_id;
                var quant = c.quantity;

                for (var i = 0; i < itmeid.length; i++) {
                    var II = itmeid[i];
                    var UI = userid[i];
                    var Q = quant[i];
                    expcode = code + i;

                    let upload = {
                        product_item_id: II,
                        user_id: `${UI}`,
                        batch_no: expcode,
                        quantity: Q,
                        service_id: req.user.organization_id,
                    };
                    uploads.push(upload);
                }

            }
        }
    }
    console.log(uploads);
    raw_material_requests.bulkCreate(uploads).then(result => {
        req.flash('primary', "successfully added!")
        res.redirect('back')
    })
}