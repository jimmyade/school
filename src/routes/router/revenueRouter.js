const express = require('express');
//const flash = require('connect-flash');
const revenue = express.Router();
 
const {
    revenue_categories,
    revenue_dimensions,
    revenue_frequencies,
    revenues } = require('../models/revenueModels/allRevenueModels');
    const {tax_payer_items} = require('../models/taxPayersModels/texPayersmodels')
    const db = require('../datab/db')
// revenue_categories route
revenue.get('/api/revenue/revenue_categories/add_revenue_categories',(req, res)=>{
    revenue_categories.findAll().then(function(result){
        res.render('./revenue/add_revenue_category', {
            result:result
        })
    })
    
})

revenue.post('/api/revenue/revenue_categories/add_revenue_categories',(req, res)=>{
    function revenue_category_code(len) {
        len = len || 100
        var nuc = "0123456789";
        var i = 0;
        var n = 0;
        s = ''
        while (i <= len -1 ) {
          n = Math.floor(Math.random() * 4)
          s += nuc[n]
          i++
        }
        return s
      }
      var revenue_category_code = revenue_category_code(4);
    var service_id = 22222
    let revenue_category = new revenue_categories({
        revenue_category : req.body.revenue_category,
        service_id,
        revenue_category_code
        
    });
    revenue_category.save().then(function(result){
        if(result){
            res.redirect('/api/revenue/revenue_categories/add_revenue_categories')
        }
    })
});

// revenue_dimensions route
revenue.get('/api/revenue/revenue_dimensions/add_revenue_dimension',(req, res)=>{
    revenue_dimensions.findAll().then(function(result){
        res.render('./revenue/add_revenue_dimension', {
            result:result
        })
    })
    
})

revenue.post('/api/revenue/revenue_dimensions/add_revenue_dimension',(req, res)=>{
    function dimension_code(len) {
        len = len || 100
        var nuc = "0123456789";
        var i = 0;
        var n = 0;
        s = ''
        while (i <= len -1 ) {
          n = Math.floor(Math.random() * 4)
          s += nuc[n]
          i++
        }
        return s
      }
      var dimension_code = dimension_code(4);
    var service_id = 22222
    let revenue_dimension = new revenue_dimensions({
        dimension : req.body.dimension,
        service_id,
        dimension_code
        
    });
    revenue_dimension.save().then(function(result){
        if(result){
            res.redirect('/api/revenue/revenue_dimensions/add_revenue_dimension')
        }
    })
})

// revenue_frequencies route
revenue.get('/api/revenue/revenue_frequencies/add_revenue_frequencies',(req, res)=>{
    revenue_frequencies.findAll().then(function(result){
        res.render('./revenue/add_revenue_frequencies', {
            result:result
        })
    })
    
})

revenue.post('/api/revenue/revenue_frequencies/add_revenue_frequencies',(req, res)=>{
    
    let revenue_frequency = new revenue_frequencies({
        revenue_frequency : req.body.revenue_frequency,
        
        
    });
    revenue_frequency.save().then(function(result){
        if(result){
            res.redirect('/api/revenue/revenue_frequencies/add_revenue_frequencies')
        }
    })
})


// revenue_frequencies route
revenue.get('/api/revenue/add_revenue', (req, res)=>{
    revenue_frequencies.findAll().then(function(revenue_frequency){
       revenue_categories.findAll().then(function(revenue_category){
           revenue_dimensions.findAll().then(function(dimension){
               db.query(`SELECT
               revenues.revenue_id, revenues.revenue_category_id, revenues.revenue_dimension_id, revenues.revenue_name, revenues.revenue_number, revenues.revenue_code, revenues.teller_exclude, revenues.available_for_p4me, revenues.roadtax, revenues.exclude_in_bulk_payment, revenues.lands, revenues.water_works, revenues.waste_mgt, revenues.nation_revenue, revenues.immigrations,revenues.customs, revenues.commerce, revenues.service_id, revenues.session_id, revenues.registered_by, revenues.registered_on, revenues.authorized_by, revenues.authorized_on, revenues.auto_generate_code, revenues.revenue_category_code, revenues.dimension_code, revenues.invoice_required, revenues.withholding_tax, revenues.revenue_frequency, revenues.assignable, revenues.fixed_rate, revenues.amount, revenue_categories.revenue_category_id, revenue_categories.revenue_category, revenue_dimensions.revenue_dimension_id, revenue_dimensions.dimension, revenue_frequencies.revenue_frequency
               FROM
               revenues ,
               revenue_categories ,
               revenue_dimensions ,
               revenue_frequencies
               WHERE
               revenues.revenue_category_id = revenue_categories.revenue_category_id AND
               revenues.revenue_dimension_id = revenue_dimensions.revenue_dimension_id AND
               revenues.revenue_frequency = revenue_frequencies.revenue_frequency`).then(function(result){
                    res.render('./revenue/add_revenue', {
                        revenue_frequency:revenue_frequency,
                        revenue_category:revenue_category,
                        dimension:dimension,
                        result:result[0]
                    })
               })
           })
       }) 
    })
})

revenue.post('/api/revenue/add_revenue',(req, res)=>{
    function revenue_code(len) {
        len = len || 100
        var nuc = "0123456789";
        var i = 0;
        var n = 0;
        s = ''
        while (i <= len -1 ) {
          n = Math.floor(Math.random() * 4)
          s += nuc[n]
          i++
        }
        return s
      }
      var revenue_code = revenue_code(4);
    var service_id = 22222;
    let Revenues = new revenues({
        // revenue_frequency : req.body.revenue_frequency,
        revenue_category_id : req.body.revenue_category_id,
        revenue_dimension_id : req.body.revenue_dimension_id,
        revenue_name : req.body.revenue_name,
        revenue_number : req.body.revenue_number,
        revenue_code : revenue_code,
        service_id : service_id,
        revenue_frequency : req.body.revenue_frequency,
        amount : req.body.amount 
    });
    Revenues.save().then(function(result){
        if(result){
            res.redirect('/api/revenue/add_revenue')
        }
    })
})

// tax_payer_items route
revenue.get('/api/tax_payer/tax_payer_items/add_tax_payer_items',(req, res)=>{
    db.query(`select * from _cities`).then(function(city){
        db.query(`select * from _streets`).then(function(street){
            db.query(`select revenue_id, revenue_name  from revenues`).then(function(revenue){
                db.query(`SELECT
                tax_payer_items.tax_payer_items_id, tax_payer_items.tax_id, tax_payer_items.revenue_name, tax_payer_items.revenue_id, tax_payer_items.date_logged, tax_payer_items.registered_by, tax_payer_items.service_id, tax_payer_items.approved, tax_payer_items.approved_date, tax_payer_items.approved_by, tax_payer_items.authorized, tax_payer_items.authorized_date, tax_payer_items.authorized_by, tax_payer_items.taxpayer_name, tax_payer_items.street_id, tax_payer_items.city_id, tax_payer_items.test_id, tax_payer_items.revenue_amount, revenues.revenue_id, revenues.revenue_category_id, _cities.city_id, _cities.city, _streets.idstreet, _streets.street
                FROM
                tax_payer_items ,
                revenues ,
                _cities ,
                _streets
                WHERE
                tax_payer_items.revenue_id = revenues.revenue_id AND
                tax_payer_items.street_id = _streets.idstreet AND
                _cities.city_id = tax_payer_items.city_id`).then(function(result){
                    res.render('./tax_payers_item/tax_payer_item', {
                        result:result[0],
                        city:city[0],
                        street:street[0],
                        revenue:revenue[0],
                    })

                })
            })
        })
    })
})

revenue.post('/api/tax_payer/tax_payer_items/add_tax_payer_items',(req, res)=>{
    
    let Tax_payer_items = new tax_payer_items({
        tax_id : req.body.tax_id,
        revenue_name : req.body.revenue_name,
        revenue_id : req.body.revenue_id,
        taxpayer_name: req.body.taxpayer_name,
        street_id : req.body.street_id,
        city_id : req.body.city_id,
        revenue_amount : req.body.revenue_amount,
        
        
    });
    Tax_payer_items.save().then(function(result){
        if(result){
            res.redirect('/api/tax_payer/tax_payer_items/add_tax_payer_items')
        }
    })
})
module.exports = revenue