const express = require('express');
const db = require('../../datab/db')


const {Bulk_promotions, Calculation_methods, Categories, Client_accounts, Deduction_details, Deduction_value_types, Departments, Education_docs } = require('../../models/basic_salary_cal_catigoryModel');
const {Pension_deduction_config, Pension_deduction_log, Pension_payment, Pension_payment_config,Pension_payments_approved, Pension_verifications, Pension_zones, Pensions_employee_info, Pensions_enroll, Pensions_nok_info} = require('../../models/all_pensionModel')
const {Ministries} = require('../../models/mda_isw_allMinistry_allnet_allnoM')
const {_months} = require('../../models/deduction_designation_genderModel');
const {employee_info} = require('../../models/employeeModel')
const pay_register = express.Router();

pay_register.get('/api/generate/generate_bulk_pay_register', (req, res)=>{
    Ministries.findAll().then(function(ministries){
        Departments.findAll().then(function(departments){
            _months.findAll().then(function(months) {
                res.render('./pay_register/generate_bulk_pay_register',{
                    ministries:ministries,
                    departments:departments,
                    months:months,
                    errors:''
                })
            }).catch(function(errors){
                Ministries.findAll().then(function(ministries){
                    Departments.findAll().then(function(departments){
                        _months.findAll().then(function(months) {
                            res.render('./pay_register/generate_bulk_pay_register',{
                                ministries:ministries,
                                departments:departments,
                                months:months,
                                errors:errors
                            })
                        })
                    })
                })    
            })
        })
    })
});

pay_register.get('/api/generate/generate_bulk_payregister_by_period', (req, res)=>{
    Ministries.findAll().then(function(ministries){
        Departments.findAll().then(function(departments){
            _months.findAll().then(function(months) {
                res.render('./pay_register/generate_bulk_payregister_by_period',{
                    ministries:ministries,
                    departments:departments,
                    months:months,
                    errors:''
                })
            }).catch(function(errors){
                Ministries.findAll().then(function(ministries){
                    Departments.findAll().then(function(departments){
                        _months.findAll().then(function(months) {
                            res.render('./pay_register/generate_bulk_payregister_by_period',{
                                ministries:ministries,
                                departments:departments,
                                months:months,
                                errors:errors
                            })
                        })
                    })
                })    
            })
        })
    })
});

pay_register.get('/api/generate/generate_individual_pay_register', (req, res)=>{
    Ministries.findAll().then(function(ministries){
        Departments.findAll().then(function(departments){
            _months.findAll().then(function(months) {
                res.render('./pay_register/generate_individual_pay_register',{
                    ministries:ministries,
                    departments:departments,
                    months:months,
                    errors:'',
                    success:""
                })
            }).catch(function(errors){
                Ministries.findAll().then(function(ministries){
                    Departments.findAll().then(function(departments){
                        _months.findAll().then(function(months) {
                            res.render('./pay_register/generate_individual_pay_register',{
                                ministries:ministries,
                                departments:departments,
                                months:months,
                                errors:errors, 
                                success:""
                            })
                        })
                    })
                })    
            })
        })
    })
});

module.exports= pay_register;