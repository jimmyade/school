const express = require('express');
const bodyparser = require('body-parser');
const path = require ('path');
const db = require('../../datab/db')
const {Sequelize, QueryTypes} = require('sequelize');
const Op = Sequelize.Op;
const report = express.Router();
report.use(bodyparser.json());
const {_buildings, 
   building_images,
   _businesses,
   _building_categories,
   _building_types,
   _business_categories,
   _business_operations,
   _business_sectors,
   _business_sizes,
   _business_types,
   businesses} = require('../../models/buildingModels/allBuildingAndBusinessModels');
const { Api_Payments, Payer_balances } = require('../../models/revenueModels/allRevenueModels');
const {
   revenue_categories,
   revenue_dimensions,
   revenue_frequencies,
   revenues,
   RevenuesInvoices,
   Revenuesettings,
   Revenue_upload,
 } = require("../../models/revenueModels/allRevenueModels");
const { client_services } = require('../../models/client_service_and_bufferModel');
const { _states } = require('../../models/lga_state_country_cities_streetModel');
const { Signature } = require('../../models/taxPayersModels/texPayersmodels');
const { query } = require('express');

report.get('/api/reports/dashboard', (req, res)=>{
 res.render('./report/dashboard_copy', {

 })
})

// report.get('/api/reports/dashboard', (req, res)=>{
//    res.render('./report/dashboard_copy', {
  
//    })
// })



report.get('/api/report/view/report_mandate', (req, res)=>{
   db.query(`select * from revenue_invoices INNER JOIN businesses on revenue_invoices.tin = businesses.taxpayer_rin where revenue_invoices.service_id= ${req.user.service_id}`, {type: QueryTypes.SELECT}).then( result =>{
      db.query('select SUM(amount) as amount from revenue_invoices', {type: QueryTypes.SELECT}).then(re =>{
         res.render('./report/mandate/mandate_report',{
            result:result,
            re:re
         })
      })
   })
})

report.get('/api/report/view/api_payment', async(req, res)=>{
   var result = await db.query(`SELECT
   api_payments.idapi_payments, api_payments.service_id, api_payments.PaymentLogId,  api_payments.CustReference, api_payments.Amount, api_payments.PaymentMethod, api_payments.PaymentReference, api_payments.TerminalId, api_payments.ChannelName, api_payments.Location, api_payments.PaymentDate,  api_payments.InstitutionId, api_payments.BranchName, api_payments.BankName,  api_payments.CustomerName, api_payments.OtherCustomerInfo, api_payments.ReceiptNo,  api_payments.CollectionsAccount, api_payments.BankCode, api_payments.DepositorName, api_payments.DepositSlipNumber, api_payments.ItemName, api_payments.ItemCode, api_payments.locked,  api_payments.Teller,  api_payments.synch_status, api_payments.logged_date,  api_payments.service_charge, api_payments.amount_net, api_payments.sources, api_payments.taxpayer_rin,  revenue_upload.id, revenue_upload.assessment_no, revenue_upload.revenue_code, revenue_upload.bill_ref_no, revenue_upload.name_of_business, revenue_upload.address_of_property, revenue_upload.type_of_property, revenue_upload.annaul_value, revenue_upload.rate_payable,  revenue_upload.arrears,  revenue_upload.grand_total,  revenue_upload.remark,  revenue_upload.service_id as real_service_id,  revenue_upload.invoice_number, revenue_upload.rate_year,  revenue_upload.rate_district,  revenue_upload.previous_balance,  revenue_upload.penalty, revenue_upload.goodwill,  revenue_upload.payment_status, revenue_upload.date_uploaded, revenue_upload.phone_number from api_payments INNER JOIN revenue_upload on revenue_upload.assessment_no = api_payments.CustReference  WHERE revenue_upload.service_id = ${req.user.service_id}` , {type: QueryTypes.SELECT});
   
  var invoce_payment = await Api_Payments.findAll({where:{service_id:req.user.service_id}});
      res.render('./report/payment/apipayment_report', {
         result:result,
         invoce_payment:invoce_payment
      })
   
})
report.get('/api/report/view/payeer/view_balance', (req, res)=>{
   Payer_balances.findAll({where:{service_id:req.user.service_id}}).then(function(result){
      res.render('./report/payment/payer_balance', {
         result:result
      })
   })
})
report.get('/api/report/view/api_payment/view_balance', async(req, res)=>{
   var reference = req.query.ref;
   let apiPayment = await Api_Payments.findOne({where:{ CustReference:reference}});
   let notice = await Revenue_upload.findOne({where:{assessment_no:reference}});
   let service = await client_services.findOne({where:{service_id:req.user.service_id}})
   let client_state = await _states.findOne({where:{state_id:service.state}});
   // let amountP = await db.query(`SELECT SUM(amount) as amountPaid from api_payments where CustReference = '${reference}'`, {type: QueryTypes.SELECT});
    const sign = await Signature.findAll({where:{service_id: req.user.service_id}});
    db.query('SELECT SUM(amount) as amountPaid from api_payments where service_id = "'+req.user.service_id+'" and CustReference = "'+reference+'"', {type: QueryTypes.SELECT}).then(amountP =>{
      console.log(amountP[0].amountPaid); 
      res.render('./report/payment/receipt', {
         apiPayment:apiPayment,
         client_state:client_state,
         notice:notice,
         service:service,
         amountP:amountP[0],
         sign:sign
      })
   })
      
})

report.get('/api/report/view/api_payment/taxpayer_receipt', async(req, res)=>{
   var reference = req.query.ref;
   let item = await db.query(`SELECT * from revenue_invoices where invoice_number = ${reference} `, {type: QueryTypes.SELECT});
   var taxpayrin = [];
   var sumAmount = 0;
   item.forEach(function(r){
      taxpayrin.push(r.tin);
      sumAmount += +r.amount;
    })
   let apiPayment = await Api_Payments.findOne({where:{service_id:req.user.service_id, CustReference:reference}});
   let notice = await businesses.findOne({where:{taxpayer_rin:taxpayrin[0]}});
   let service = await client_services.findOne({where:{service_id:req.user.service_id}})
   let client_state = await _states.findOne({where:{state_id:service.state}});
   // let amountP = await db.query(`SELECT SUM(amount) as amountPaid from api_payments where CustReference = '${reference}'`, {type: QueryTypes.SELECT});
    const sign = await Signature.findAll({where:{service_id: req.user.service_id}});
    console.log(notice.business_name)
    db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+reference+'"', {type: QueryTypes.SELECT}).then(amountP =>{
      console.log(amountP[0].amountPaid); 
      res.render('./report/payment/tax_payer_receipt', {
         apiPayment:apiPayment,
         client_state:client_state,
         notice:notice,
         service:service,
         amountP:amountP[0],
         sign:sign,
         sumAmount
      })
   })
      
})

report.get('/api/business/business_report/search', (req, res)=>{
   db.query(`select * from businesses`).then(function(bus){
      db.query(`select * from _business_categories`).then(function(bus_category){
          db.query(`select * from _business_types`).then(function(business_types){
              db.query(`select * from _business_operations`).then(function(business_operations){
                  db.query(`select * from _business_sectors`).then(function(business_sectors){
                      db.query(`select * from _business_sizes`).then(function(business_sizes){
                          res.render('./report/business_report/search_report', {
                              bus:bus[0],
                              bus_category:bus_category[0],
                              business_types:business_types[0],
                              business_operations:business_operations[0],
                              business_sectors:business_sectors[0],
                              business_sizes:business_sizes[0],
                          })
                      })
                  })
              }) 
          })
      }) 
  })
})

report.post('/api/business/business_report/search',(req, res)=>{
   var business_type = req.body.business_type,
   business_name = req.body.business_name,
   business_lga = req.body.business_lga,
   business_category = req.body.business_category,
   business_sector = req.body.business_sector,
   business_structure = req.body.business_structure,
   Status = req.body.Status;
   if(business_type !=="" && business_name !=="" && business_lga !=="" && business_category !=="" && business_sector !=="" && business_structure !=="" && Status !=="") {
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE businesses.business_type = '${business_type}' AND businesses.business_name = '${business_name}' AND businesses.business_lga = '${business_lga}' AND businesses.business_category = '${business_category}' AND businesses.business_sector = '${business_sector}' AND businesses.business_structure = '${business_structure}' AND businesses.Status = '${Status}'`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   } else if(business_type !=="" && business_name !=="" && business_lga !=="" && business_category !=="" && business_sector !=="" && business_structure !=="" ){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE businesses.business_type = "${business_type}" AND businesses.business_name = "${business_name}" AND businesses.business_lga = "${business_lga}" AND businesses.business_category = "${business_category}" AND businesses.business_sector = "${business_sector}" AND businesses.business_structure = "${business_structure}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
           
            result:result[0]
         })
      })
   }
   else if( business_lga !=="" && business_category !=="" && business_sector !=="" && business_structure !=="" && Status !==""){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE   businesses.business_lga = "${business_lga}" AND businesses.business_category = "${business_category} " AND businesses.business_sector = "${business_sector}" AND businesses.business_structure = "${business_structure}" AND businesses.Status = "${Status}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   } else if( business_category !=="" &&    business_sector !=="" && business_structure !=="" && Status !==""){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE  businesses.business_category =" ${business_category}" AND businesses.business_sector = "${business_sector}" AND businesses.business_structure ="${business_structure}" AND businesses.Status = "${Status}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   }
   else if(    business_sector !=="" && business_structure !=="" && Status !==""){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE  businesses.business_sector = "${business_sector}" AND businesses.business_structure = "${business_structure}" AND businesses.Status = "${Status}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   }
   else if( business_structure !=="" && Status !==""){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE  businesses.business_structure = "${business_structure}" AND businesses.Status = "${Status}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   }
   else if( Status !==""){
      db.query(`SELECT businesses.business_id, businesses.id_business, businesses.business_rin, businesses.business_type, businesses.asset_type, businesses.business_name, businesses.business_lga, businesses.business_category, businesses.business_sector, businesses.business_sub_sector, businesses.business_structure, businesses.business_operation, businesses.Profile_ref, businesses.Status, businesses.service_id, businesses.created_by, businesses.created_at, businesses.updated_by, businesses.updated_at,businesses.tax_id, businesses.taxpayer_rin, businesses.system_business_rin, businesses.business_size, businesses.business_address, businesses.contact_person, businesses.businessnumber, businesses.business_email, businesses.passwordr,businesses.photo_url,businesses.apartment_id, businesses.building_id, businesses.organization_id, businesses.tax_office_id, businesses.business_ownership, businesses.business_street_id, businesses.business_tag FROM businesses WHERE businesses.Status ="${Status}"`).then(function(result){
         res.render('./report/business_report/busines_search_output',{
            result:result[0]
         })
      })
   }
  
})
module.exports = report;