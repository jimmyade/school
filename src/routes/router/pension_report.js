const express = require('express');
const pension_report = express.Router();
const db = require('../../datab/db')
// include model pages
const {_months} = require('../../models/deduction_designation_genderModel');
const {Pension_deduction_config, Pension_deduction_log, Pension_payment, Pension_payment_config,Pension_payments_approved, Pension_verifications, Pension_zones, Pensions_employee_info, Pensions_enroll, Pensions_nok_info} = require('../../models/all_pensionModel');
const {_lgas, _cities, _states,  _countries, _residential_address_status }= require('../../models/lga_state_country_cities_streetModel');



//pension reports routers 
//pension_variation report 
pension_report.get('/api/pension/report/pension_variation_report', (req, res)=>{
    _months.findAll().then( function(months){
        Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/pension_reports/pension_variation_report', {
                months:months,
                pension_zone:pension_zone
            })
        })
    })

})
pension_report.post('/api/pension/report/pension_variation_report', (req, res)=>{
     
})
// pension payment report
pension_report.get('/api/pension/report/pension_payment_report', (req, res)=>{
    _months.findAll().then( function(months){
        res.render('./pensions/pension_reports/pension_payment_report', {
            months:months,
        })
    })

})

//generate_pen_variation_report
pension_report.get('/api/pension/report/generate_pen_variation_report', (req, res)=>{
    _months.findAll().then( function(months){
        Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/pension_reports/generate_pen_variation_report', {
                months:months,
                pension_zone:pension_zone
            })
        })
    })

})
pension_report.post('/api/pension/report/generate_pen_variation_report', (req, res)=>{
   var year = req.body.year;
   var month = req.body.month;
   var pension_zone_id = req.body.pension_zone;
   var serviceID = req.user.service_id;
   var created_by = req.user.username;
   db.query(`call generate_pension_payment(${serviceID}, '${pension_zone_id}','${month}','${year}','${created_by}',@res,@batch_no); select @res as result, @batch_no as batch_no`, function(err, result){
       if(err) throw err;
       console.log(result)
   })
})

//manege pension routers
//manage_pension_payment route
pension_report.get('/api/pension/manage/manage_pension_payment', (req, res)=>{
    _lgas.findAll().then(function(lga){
        Pension_zones.findAll().then( function(pension_zones ){
            res.render('./pensions/manage_pensions/manage_pension_payment', {
                pension_zones :pension_zones,
                lga:lga
            })
        })
    })
});

// individual_pension_payregister
pension_report.get('/api/pensions/pension_pay_register', (req, res)=>{
    _months.findAll().then(function(months) {
        Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/pension_payregister',{
                pension_zone:pension_zone,
                months:months,
                errors:''
            })
        })
     })
               
 })
 pension_report.post('/api/pensions/pension_pay_register', (req, res)=>{
    var year = req.body.year;
    var month = req.body.month;
    var pension_zone_id = req.body.pension_zone;
    var serviceID = req.user.service_id;
    var created_by = req.user.username;
    db.query(`call generate_pension_payment(${serviceID}, '${pension_zone_id}','${month}','${year}','${created_by}',@res,@batch_no); select @res as result, @batch_no as batch_no`).then(function(resut){
        if(resut){
            res.send(resut)
        }
    }) .catch(function(err){
        console.log(err)
    })         
 })

 // individual_pension_payregister
pension_report.get('/api/pensions/individual_pension_pay_register', (req, res)=>{
    _months.findAll().then(function(months) {
        //Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/individual_pension_pay_register',{
                //pension_zone:pension_zone,
                months:months,
                errors:''
            })
        //})
     })
               
 })
 pension_report.post('/api/pensions/individual_pension_pay_register', (req, res)=>{
     var serviceID = req.user.service_id;
     var created_by = req.user.username;
     var month = req.body.month;
     var year = req.body.year;
     var employee_no = req.body.employee_no
    db.query(`call generate_pension_payement_emp(${serviceID},'${month}','${year}',${employee_no}, '${created_by}', @res, @batch_no )`).then(function(result){
        if(result){
            res.send(result)
        }
    }) .catch(function(err){
        console.log(err)
    })         
 })

 //Generate variable report
 pension_report.get('/api/pensions/report/generate_variation_report', (req, res)=>{
    _months.findAll().then(function(months) {
            res.render('./pensions/pension_reports/generate_pen_variation_report',{
                months:months,
                errors:''
            })
     })
               
 })
 pension_report.post('/api/pensions/report/generate_variation_report', (req, res)=>{
    var serviceId = req.user.service_id;
    //var created_by = req.user.username;
    var month = req.body.month;
    var year = req.body.year;
    db.query(`delete from pension_variation_report_zone where service_id =${serviceId} and current_month='${month}' and year='${year}'; delete from pension_variation_report_zone where service_id =${serviceId} and current_month='${month}' and year='${year}'`).then(function(result){
        if(result){
            res.send(result)
        }
    }) .catch(function(err){
        console.log(err)
    })
              
 });

  //pension payment report
  pension_report.get('/api/pensions/report/pension_payment_report', (req, res)=>{
    _months.findAll().then(function(months) {
        Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/pension_reports/pension_payment_report',{
                months:months,
                pension_zone:pension_zone,
                errors:''
            })
        })
     })
               
 })
 pension_report.post('/api/pensions/report/pension_payment_report', (req, res)=>{
    var serviceId = req.user.service_id;
    var pension_zone = req.body.pension_zone;
    var month = req.body.month;
    var year = req.body.year;
    var locked = req.body.locked
    if(year != "" && month != ""){
        let where = `service_id = ${serviceId} AND month_name LIKE '%${month}%' AND year = '${year}'`;
        var variationDataSet = "";
        //var mdaDataList = array();

        db.query(`SELECT * FROM pension_payments WHERE ".${where}." ORDER BY name`).then(function(result){
            if(result){
                res.send(result)
            }
        }).catch(function(err){
            console.log()
        });
    }
    else if(year != "" && month != "" && pension_zone != ""){
            let where = `AND pension_zone LIKE '%${pension_zone}%'`;
            db.query(`SELECT * FROM pension_payments WHERE ".${where}." ORDER BY name`).then(function(result){
                if(result){
                    res.send(result)
                }
            }).catch(function(err){
                console.log()
            });
        }
    else if(year != "" && month != "" && locked != ""){
            var where = `AND locked = ${locked}` ;
        }    
        db.query(`SELECT * FROM pension_payments WHERE ".${where}." ORDER BY name`).then(function(result){
            if(result){
                res.send(result)
            }
        }).catch(function(err){
            console.log()
        });
 })

  //pension_variation_report
  pension_report.get('/api/pensions/report/pension_variation_report', (req, res)=>{
    _months.findAll().then(function(months) {
        Pension_zones.findAll().then(function(pension_zone){
            res.render('./pensions/pension_reports/pension_variation_report',{
                months:months,
                pension_zone:pension_zone,
                errors:''
            })
        })
     })
               
 })
 pension_report.post('/api/pensions/report/pension_variation_report', (req, res)=>{
     var year = req.body.year;
     var month = req.body.month;
     var serviceId = req.user.service_id;
     var variation_from = req.body.variation_from;
     var pension_zone = req.body.pension_zone;
    if(year != "" && month !=""){
        //$where = "service_id = '".serviceId()."' AND current_month LIKE '%".$month."%' AND current_year = '".$year."' AND variation >= '".$variation_from."'";
    var where = `service_id = ${serviceId} AND current_month LIKE '%.${month}.%' AND current_year = '.${year}.'`;          
        //var graphWhere = "service_id = '".serviceId()."';
        db.query(`SELECT pension_zone, sum(current_net_pension) as sum_current_net_pay, sum(previous_net_pension) as sum_prev_net_pay FROM pension_variation_report WHERE ".${where}." GROUP BY pension_zone`).then(function(result){
            if(result){
                res.send(result)
            }
        }).catch(function(err){
            console.log(err)
        })
    }
    else if(variation_from != ""){
        var where = `AND variation >= '.${variation_from}.'`; 
        db.query(`SELECT pension_zone, sum(current_net_pension) as sum_current_net_pay, sum(previous_net_pension) as sum_prev_net_pay FROM pension_variation_report WHERE ".${where}." GROUP BY pension_zone`).then(function(result){
            if(result){
                res.send(result)
            }
        }).catch(function(err){
            console.log(err)
        })
    } else if(pension_zone != ""){
            var where = `AND pension_zone LIKE '%".${pension_zone}."%'` ;
            //$graphWhere .= " AND pension_zone LIKE '%".$pension_zone."%'";
            db.query(`SELECT pension_zone, sum(current_net_pension) as sum_current_net_pay, sum(previous_net_pension) as sum_prev_net_pay FROM pension_variation_report WHERE ".${where}." GROUP BY pension_zone`).then(function(result){
                if(result){
                    res.send(result)
                }
            }).catch(function(err){
                console.log(err)
            })      
    }       
 })
module.exports = pension_report;