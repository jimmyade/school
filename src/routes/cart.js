const express = require('express');
//const flash = require('connect-flash');
const cart = express.Router();
const { Sequelize, QueryTypes } = require('sequelize');
const { paymentSettings } = require('../controller/paymentController');
const { studentRegistration } = require('../controller/studentRegController');
const Op = Sequelize.Op;
const db = require('../datab/db');
const { shlsession } = require('../models/basicTablesModel');
const { classtable } = require('../models/classModel/classModel');
const { client_services } = require('../models/client_service_and_bufferModel');
const { parent } = require('../models/parentModel/parentModel');
const { staffregistration } = require('../models/staffModel/staffmodel');

// cart.route("*")
//     .get(async (req, res, next) => {
//         let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
//         // res.locals.clientName =  c.client;
//         let pInfo = await parent.findOne({ where: { schoolId: req.user.organization_id } })
//         res.locals.clientCode = c.service_code;
//         res.locals.parentInfo = pInfo;
//         next();
//     }).post(async (req, res, next) => {
//         let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
//         let pInfo = await parent.findOne({ where: { schoolId: req.user.organization_id } })
//         res.locals.clientCode = c.service_code;
//         res.locals.parentInfo = pInfo;
//         next();
//     });

cart.get('/add', (req, res) => {
    var paymentid = req.query.paymentid;
    var studentid = req.query.studentid;
    db.query(`select * from studentregistration left JOIN payment_settings on studentregistration.className = payment_settings.classname where studentregistration.schoolId = ${req.user.organization_id} AND studentregistration.student_reg_Id = ${studentid} and payment_settings.paymentsetup_id = ${paymentid}`, { type: QueryTypes.SELECT }).then(function (result) {
        if (typeof req.session.cart == "undefined") {
            req.session.cart = [];
            req.session.cart.push({
                class_id: result[0].class_id,
                classname: result[0].classname,
                paymentID: paymentid,
                studentId: studentid,
                amount: result[0].amount,
                term: result[0].termid,
                options: result[0].options,
                item: result[0].item,
                acdsession: result[0].sesion_id,
                studentname: `${result[0].surname} ${result[0].firstname} ${result[0].middlename}`,
                phone:  result[0].phone


            });
            console.log(req.session.cart)

        } else {
            var cart = req.session.cart;
            var newItem = true;
            for (var i = 0; i < cart.length; i++) {
                if (cart[i].paymentID == paymentid) {
                    // cart[i].qty++;
                    newItem = false;
                    req.flash('warning', "Exist Already ");
                    res.redirect('back');
                    break;
                }
            }
            if (newItem) {
                cart.push({
                    class_id: result[0].class_id,
                    classname: result[0].classname,
                    paymentID: paymentid,
                    studentId: studentid,
                    amount: result[0].amount,
                    term: result[0].termid,
                    options: result[0].options,
                    item: result[0].item,
                    acdsession: result[0].sesion_id,
                    studentname: `${result[0].surname} ${result[0].firstname} ${result[0].middlename}`,
                    phone:  result[0].phone

                });
            }
        }
        req.flash('success', "New Fee Added! ");
        res.redirect('back');



    });
});

/*
* Get add checkout 
*/
cart.get('/cart/checkout', async (req, res) => {
    let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
    let pInfo = await parent.findOne({ where: { schoolId: req.user.organization_id } })
    res.locals.clientCode = c.service_code;
    res.locals.parentInfo = pInfo;
    if (req.session.cart && req.session.cart.length == 0) {
        delete req.session.cart;
        res.redirect('/cart/checkout');
    } else {
        let cart = req.session.cart;
        let students = await staffregistration.findAll({ where: { schoolId: req.user.organization_id } });
        let acdsession = await shlsession.findAll({ where: { schoolId: req.user.organization_id } })
        res.render('./parent/checkpoint', {
            tit: 'My Cart',
            cart: cart, students, acdsession
        });

    }

});

/*
* Get Update  Product 
*/
cart.get('/cart/update/:paymentID', function (req, res) {
    var id = req.params.paymentID;
    var cart = req.session.cart;
    var action = req.query.action;

    for (var i = 0; i < cart.length; i++) {
        if (cart[i].paymentID == id) {
            switch (action) {
                case "add":
                    cart[i].qty++;
                    break;
                case "remove":
                    cart[i].qty--;
                    if (cart[i].qty < 1) cart.splice(i, 1);
                    break;
                case "clear":
                    cart.splice(i, 1);
                    if (cart.length == 0) delete req.session.cart;
                    break;
                default:
                    console.log("update problem");
                    break;
            }
            break;
        }
    }
    req.flash('success', "Cart Item Removed ");
    res.redirect('/cart/checkout');
});


/*
* Get clear cart 
*/
cart.get('/cart/clear', (req, res) => {


    delete req.session.cart;
    req.flash('success', "Cart Cleared ");
    res.redirect('/cart/checkout');


});


module.exports = cart;