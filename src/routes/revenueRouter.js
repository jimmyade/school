const express = require("express");
const multer = require("multer");
const mysql = require("mysql");
const fs = require('fs');

const readXlsxFile = require('read-excel-file/node');
const request = require('request');
const {Sequelize, QueryTypes} = require('sequelize');
const Op = Sequelize.Op;
var TMClient = require('textmagic-rest-client');

// const smpp = require('smpp');
//const session = new smpp.Session({host: '0.0.0.0', port: 9500});

var bcrypt =  require('bcryptjs');;
const saltRounds = 10;
//const flash = require('connect-flash');
const revenue = express.Router();

const {
  revenue_categories,
  revenue_dimensions,
  revenue_frequencies,
  revenues,
  RevenuesInvoices,
  Revenuesettings,
  Revenue_upload,
  Api_Payments,
} = require("../models/revenueModels/allRevenueModels");
const {
  _business_operations,businesses
} = require("../models/buildingModels/allBuildingAndBusinessModels");
const {
  tax_payer_items,
  tax_payers,
  _lgas,
  _cities,
  Signature
} = require("../models/taxPayersModels/texPayersmodels");
const { client_services } = require("../models/client_service_and_bufferModel");
const db = require("../datab/db");
var service = require("../datab/service");
var {Users} = require('../models/userModel/userModels')

// access control uthentication middleware 
const accessControl = require ('../lib/service');
const { get } = require("http");
const { _states } = require("../models/lga_state_country_cities_streetModel");
var isAdmin = accessControl.isAdmin;

// function for uploading picture via multer
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

// const fileFilter = (req, file, cb) => {
//   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//     cb(null, true);
//   } else {
//     cb(null, fulse);
//   }
// };
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 2,
  },
  //fileFilter: fileFilter,
});


// sms configuration
// let isConnected = false
// session.on('connect', () => {
//   isConnected = true;

//   session.bind_transceiver({
//       system_id: 'USER_NAME',
//       password: 'USER_PASSWORD',
//       interface_version: 1,
//       system_type: '380666000600',
//       address_range: '+380666000600',
//       addr_ton: 1,
//       addr_npi: 1,
//   }, (pdu) => {
//     if (pdu.command_status == 0) {
//         console.log('Successfully bound')
//     }

//   })
// })

// session.on('close', () => {
//   console.log('smpp is now disconnected') 
   
//   if (isConnected) {        
//     session.connect();    //reconnect again
//   }
// })

// session.on('error', error => { 
//   console.log('smpp error', error)   
//   isConnected = false;
// });




revenue.get('/api/admin/sign/profile', (req, res)=>{
  client_services.findOne({where: {service_id : req.user.service_id}}).then(result=>{
    res.render('./adminProfile',{
      err:'',
      tit: "",
      result:result,
      errors:""
    })
  })
});

revenue.post('/api/admin/update/client_info', (req, res)=>{
  try{
    var client = {
      client_email: req.body.client_email,
      client_phone: req.body.client_phone
    }
    client_services.findOne({where: {service_id : req.user.service_id}}).then(result=>{
      result.update(client, {new: true}).then(result1 =>{
        req.flash('info', 'client detail updated');
        res.redirect('back')
      })
    })
  } catch (err) {
    res.flash('info', err.message);
    res.redirect('back')
  }
});

revenue.post('/api/admin/update/personal_info_profile', (req, res)=>{
  let firstname = req.body.firstname,
      surname = req.body.surname,
      middlename = req.body.middlename,
      email = req.body.email,
      user_phone = req.body.user_phone;
      personalInfo = {name: `${surname} ${firstname} ${middlename}`, username: email, email, user_phone, surname, firstname, middlename };
      Users.findOne({where: {service_id:req.user.service_id, id : req.user.id}}).then(result1 =>{
        // r.update({ client_admin_email:email, client_admin_phone:user_phone,admin_surname:surname, admin_firstname: firstname, admin_middlename:middlename }, {new: true}).then(result =>{
          result1.update(personalInfo, {new: true}).then(result =>{
          if(result){
            req.flash('success', "Info Update ");
            res.redirect('/api/admin/sign/profile');
          } else {
            req.flash('danger', "Not Update ");
             res.redirect('/api/admin/sign/profile');
          }
        }).catch(err =>{
          req.flash('danger', err);
             res.redirect('/api/admin/sign/profile');
        })
      }) 

})


revenue.post('/api/admin/update/organization_info_profile', (req, res)=>{
        var  client = req.body.client;
        var client_phone = req.body.client_phone;
        var client_email = req.body.client_email;
        var client_address = req.body.client_address;
        var spc_beneficiary = req.body.spc_beneficiary;
        var created_by = req.body.created_by;
        var personal = {client, client_phone, client_email, client_address, spc_beneficiary, created_by};
        console.log( {client, client_phone, client_email, client_address, created_by});
      client_services.findOne({where: {service_id:req.user.service_id}}).then(r =>{
        r.update(personal, {new: true}).then(result =>{
          if(result){
            req.flash('success', "Organisational info Update ");
            res.redirect('/api/admin/sign/profile');
          } else {
            req.flash('danger', "Not Update ");
             res.redirect('/api/admin/sign/profile');
          }
        }).catch(err =>{
          req.flash('danger', err);
             res.redirect('/api/admin/sign/profile');
        })
      }) 
})

revenue.post('/api/change/client_service_logo', upload.single("service_logo_url"), (req, res)=>{
  var PrepreviousImg = req.body.PrepreviousImg;
  var service_logo_url = req.file.filename;
  let logo = {service_logo_url};
  client_services.findOne({where: {service_id:req.user.service_id}}).then(r =>{
    r.update(logo, {new: true}).then(result =>{
      var PrevFile = './uploads/'+PrepreviousImg;
      fs.unlink(PrevFile, function(err){
        if (err) {
          console.log(err)
        } else {
          req.flash('success', "Logo Changed ");
          res.redirect('/api/admin/sign/profile');
        }
      })
    }).catch(err =>{
      req.flash('danger', err);
         res.redirect('/api/admin/sign/profile');
    })
  }) 
});

revenue.post('/api/admin/update/admin_reset_password', (req, res)=>{
  let {email,  oldpassword, newpassword, newpassword2 } = req.body;
    let errors = [];
      if( !email|| !oldpassword  || !newpassword || !newpassword2){
          errors.push({msg : 'Please enter all fields'})
      }
      if (newpassword != newpassword2) {
          errors.push({ msg: 'New Password do not match Confirm password' });
      }
      if (newpassword.length < 6) {
        errors.push({ msg: 'NewPassword must be at least 6 characters Long' });
      }
      if (errors.length < 0) {
        client_services.findOne({where: {service_id : req.user.service_id}}).then(result=>{
          res.render('./adminProfile',{
            err:'',
            tit: "",
            result:result,
            errors:errors
          })
        })
        //console.log(errors)
      } else {
        Users.findOne({where: {service_id:req.user.service_id, email: email}}).then(admin =>{
          if(admin){
              bcrypt.compare(oldpassword, admin.password, function(err, isMatch){
                  if (err) console.log(err);

                  if(isMatch) {
                    bcrypt.genSalt(10, (err, salt) => {
                      bcrypt.hash(newpassword, salt, (err, hash) => {
                        if (err) throw err;
                        newpassword = hash;
                        admin.update({password:newpassword}, {new:true}).then(result =>{
                          if(result){
                            req.flash('success', "Password Reset Successful ");
                            res.redirect('back');
                          } else {
                            errors.push({ msg: 'Password reset failed' });
                             res.redirect('back');
                          }
                        }).catch(err =>{
                          client_services.findOne({where: {service_id : req.user.service_id}}).then(result=>{
                            res.render('./adminProfile',{
                              err:err,
                              tit: "",
                              errors:errors,
                              result:result,
                            })
                          })
                        })
                          
                      });
                    });
                  } else{
                    req.flash('danger', "Password Reset failed! ");
                    errors.push({ msg: 'Old password incorect' });
                    client_services.findOne({where: {service_id : req.user.service_id}}).then(result=>{
                      res.render('./adminProfile',{
                        err:"",
                        tit: "",
                        errors:errors,
                        result:result,
                      })
                    })
                  }
              })
          }
        })
      }
})



revenue.get('/api/dasboard/admin/sign', (req, res)=>{
  Signature.findAll({where :{service_id: req.user.service_id}}).then(function(result){
    res.render('./admin_signatures.ejs',{
      err:'',
      tit: "",
      result:result
    })
  })
});

revenue.get('/api/dasboard/admin/sign/:signature_id', isAdmin, (req, res, next)=>{
  Signature.findOne({where :{signature_id: req.params.signature_id}}).then(function(result){
    result.destroy().then(function(del){
      var fileName = `./uploads/${result.OfficersSign}`;
      fs.unlink(fileName, function (err) {
        if (err) throw err;
        // if no error, file has been deleted successfully
        console.log('File deleted!');
        res.redirect('/api/dasboard/admin/sign');
    }); 
      
    });
  });
});

revenue.post('/api/dasboard/admin/sign', isAdmin, upload.single('OfficersSign'), (req, res)=>{
  let AdminSign = new Signature({
      OfficersName: req.body.OfficersName ,
      OfficersDesign: req.body.OfficersDesign ,
      area_council: req.body.area_council ,
      OfficersSign: req.file.filename ,
      position: req.body.position ,
      service_id: req.user.service_id 
  });
  AdminSign.save()
  .then(function(result){
    res.redirect('/api/dasboard/admin/sign');
  })
  .catch(function(err){
    Signature.findAll({where :{service_id: req.user.service_id}}).then(function(result){
      res.render('./admin_signatures.ejs',{
        err:err.message,
        tit: "",
        result:result
      })
    })
  })
});
//update admin signature 
revenue.route('/api/dasboard/admin/sign/edit/:signature_id')
.get(isAdmin, async(req, res)=>{
  var id = req.params.signature_id;
  Signature.findOne({where :{signature_id: req.params.signature_id}}).then(function(result){
    res.render('./update_admin_signatures',{
      result:result,
      err:"",
      tit:'',
      id
    })
  })
}) 
.post(isAdmin, async(req, res, next)=>{
  try {
    let AdminSign = {
      OfficersName: req.body.OfficersName ,
      OfficersDesign: req.body.OfficersDesign ,
      area_council: req.body.area_council ,
      position: req.body.position ,
      service_id: req.user.service_id 
    };
    Signature.findOne({where :{signature_id: req.params.signature_id}}).then(function(result){
      result.update(AdminSign, {new:true}).then(function(result){
        req.flash('info', 'updated');
        res.redirect('back');
      });
    });
  } catch(err) {
    req.flash('info', err);
    res.redirect('back');
  }
});
// update signature img
revenue.post('/api/dasboard/admin/sign/edit/img/:id', upload.single('OfficersSign'), (req, res)=>{
  try{
    var previousImg = req.body.previousImg;
    Signature.findOne({where :{signature_id: req.params.id}}).then(function(result){
      result.update({OfficersSign: req.file.filename}, {new:true}).then(function(result){
        var file= `./uploads/${previousImg}`;
            fs.unlink(file, function (err) {
              if (err){ 
                console.log(err)
              } else {
                req.flash('info', 'New Signature uploaded.');
                res.redirect('back');
              }
            })
      });
    });
  } catch (err){
    req.flash('info', err);
    res.redirect('back');
  } 
})
// revenue_categories route
revenue.get(
  "/api/revenue/revenue_categories/add_revenue_categories",
  (req, res) => {
    revenue_categories.findAll().then(function (result) {
      res.render("./revenue/add_revenue_category", {
        result: result,
      });
    });
  }
);

revenue.post(
  "/api/revenue/revenue_categories/add_revenue_categories", isAdmin,
  (req, res) => {
    var service_id = req.user.service_id;
    let revenue_category = new revenue_categories({
      revenue_category: req.body.revenue_category,
      service_id,
      revenue_category_code: 2222,
    });
    revenue_category.save().then(function (result) {
      if (result) {
        req.flash('success', "Cart Updated ");
        res.redirect("/api/revenue/revenue_categories/add_revenue_categories");
      }
    });
  }
);

// revenue_dimensions route
revenue.get(
  "/api/revenue/revenue_dimensions/add_revenue_dimension",
  (req, res) => {
    revenue_dimensions.findAll().then(function (result) {
      res.render("./revenue/add_revenue_dimension", {
        result: result,
      });
    });
  }
);

revenue.post(
  "/api/revenue/revenue_dimensions/add_revenue_dimension", isAdmin,
  (req, res) => {
    function dimension_code(len) {
      len = len || 100;
      var nuc = "0123456789";
      var i = 0;
      var n = 0;
      s = "";
      while (i <= len - 1) {
        n = Math.floor(Math.random() * 4);
        s += nuc[n];
        i++;
      }
      return s;
    }
    var dimension_code = dimension_code(4);
    var service_id = req.user.service_id;
    let revenue_dimension = new revenue_dimensions({
      dimension: req.body.dimension,
      service_id,
      dimension_code: dimension_code,
    });
    revenue_dimension.save().then(function (result) {
      if (result) {
        res.redirect("/api/revenue/revenue_dimensions/add_revenue_dimension");
      }
    });
  }
);

// revenue_frequencies route
revenue.get(
  "/api/revenue/revenue_frequencies/add_revenue_frequencies",
  (req, res) => {
    revenue_frequencies.findAll().then(function (result) {
      res.render("./revenue/add_revenue_frequencies", {
        result: result,
      });
    });
  }
);

revenue.post(
  "/api/revenue/revenue_frequencies/add_revenue_frequencies", isAdmin,
  (req, res) => {
    let revenue_frequency = new revenue_frequencies({
      revenue_frequency: req.body.revenue_frequency,
    });
    revenue_frequency.save().then(function (result) {
      if (result) {
        res.redirect(
          "/api/revenue/revenue_frequencies/add_revenue_frequencies"
        );
      }
    });
  }
);

// revenue_frequencies route
revenue.get("/api/revenue/add_revenue", (req, res) => {
  revenue_frequencies.findAll().then(function (revenue_frequency) {
    revenue_categories.findAll().then(function (revenue_category) {
      revenue_dimensions.findAll().then(function (dimension) {
        db.query(`SELECT * FROM revenues`).then(function (result) {
          res.render("./revenue/add_revenue", {
            revenue_frequency: revenue_frequency,
            revenue_category: revenue_category,
            dimension: dimension,
            result: result[0],
          });
        });
      });
    });
  });
});

revenue.get("/api/revenue/update_revenue/:id", (req, res) => {
  var id = req.params.id;
  revenues.findOne({ where: { id: id } }).then(function (result) {
    revenue_frequencies.findAll().then(function (revenue_frequency) {
      revenue_categories.findAll().then(function (revenue_category) {
        revenue_dimensions.findAll().then(function (dimension) {
          res.render("./revenue/update_revenue/update_revenue", {
            result: result,
            revenue_frequency: revenue_frequency,
            revenue_category: revenue_category,
            dimension: dimension,
            id,
          });
        });
      });
    });
  });
});

// Update Revenue
revenue.put("/api/revenue/update_revenue/:id", isAdmin, (req, res) => {
  var id = req.params.id;
  revenues.findOne({ where: { id: id } }).then(function (revenue) {
    revenue.update(req.body, { new: true }).then(function (result) {
      if (result) {
        res.redirect(`/api/revenue/add_revenue`);
      }
    });
  });
});

// Delete Revenue
revenue.delete("/api/revenue/delete_revenue/:id", isAdmin, (req, res) => {
  var id = req.params.id;
  revenues.findOne({ where: { id: id } }).then(function (revenue) {
    revenue.destroy().then(function (result) {
      
      if (result) {
        res.redirect(`/api/revenue/add_revenue`);
      }
    });
  });
});

revenue.post("/api/revenue/add_revenue", isAdmin, (req, res) => {
  var service_id = req.user.service_id;
  let Revenues = new revenues({
    // revenue_frequency : req.body.revenue_frequency,
    revenue_category_id: req.body.revenue_category_id,
    revenue_dimension_id: req.body.revenue_dimension_id,
    revenue_name: req.body.revenue_name,
    revenue_number: req.body.revenue_number,
    //revenue_code : revenue_code,
    service_id: service_id,
    revenue_frequency: req.body.revenue_frequency,
    amount: req.body.amount,
  });
  Revenues.save().then(function (result) {
    if (result) {
      res.redirect("/api/revenue/add_revenue");
    }
  });
});

// tax_payer_items route
revenue.get("/api/tax_payer/dashboard", (req, res) => {
  res.render("./tax_payers_item/dashboard_copy");
});
revenue.get("/api/tax_payer/tax_payers/tax_payers", (req, res) => {
  var service_id = req.user.service_id;
  tax_payers
    .findAll({ where: { service_id: service_id } })
    .then(function (result) {
      res.render("./tax_payers_item/tax_payers_view", {
        result: result,
      });
    });
});

revenue.get("/api/tax_payer/tax_payers/add_tax_payers", (req, res) => {
  var service_id = req.user.service_id;
  db.query(`SELECT  * from tax_payers INNER JOIN businesses on  tax_payers.taxpayer_rin = businesses.taxpayer_rin where tax_payers.service_id = ${service_id}`).then(function (result) {
      res.render("./tax_payers_item/add_taxpayer", {
        result: result[0],
      });
    });
});
revenue.post(
  "/api/tax_payer/tax_payers/add_tax_payers", isAdmin,
  upload.single("photo_url"),
  (req, res) => {
    let TaxPayers = new tax_payers({
      taxpayer_rin: req.body.taxpayer_rin,
      taxpayer_name: req.body.taxpayer_name,
      taxpayer_tin: req.body.taxpayer_tin,
      mobile_number: req.body.mobile_number,
      email_addresss: req.body.email_addresss,
      contactaddress: req.body.contactaddress,
      tax_office: req.body.tax_office,
      tax_payer_type: req.body.tax_payer_type,
      economic_activity: req.body.economic_activity,
      photo_url: req.file.filename,
      service_id: req.user.service_id,
    });

    TaxPayers.save().then(function (result) {
      if (result) {
        res.redirect("/api/tax_payer/tax_payers/add_tax_payers");
      }
    });
  }
);
//get update taxpayer pag
revenue.get(
  "/api/tax_payer/tax_payers/update_tax_payers/:taxpayer_id",
  (req, res) => {
    var taxpayer_id = req.params.taxpayer_id;
    var service_id = req.user.service_id;
    tax_payers
      .findOne({ where: { taxpayer_id: taxpayer_id, service_id: service_id } })
      .then(function (result) {
        res.render("./tax_payers_item/updates/taxpayers_update", {
          taxpayer_id,
          result: result,
        });
      });
  }
);

//update taxpayer
revenue.put(
  "/api/tax_payer/tax_payers/update_tax_payers/:taxpayer_id", isAdmin,
  (req, res) => {
    var taxpayer_id = req.params.taxpayer_id;
    var service_id = req.user.service_id;
    tax_payers
      .findOne({ where: { taxpayer_id: taxpayer_id, service_id: service_id } })
      .then(function (result) {
        result.update(req.body, { new: true }).then(function (result1) {
          res.redirect("/api/tax_payer/tax_payers/tax_payers");
        });
      });
  }
);

//delect taxpayer
revenue.delete(
  "/api/tax_payer/tax_payers/delete_tax_payers/:taxpayer_id", isAdmin,
  (req, res) => {
    var taxpayer_id = req.params.taxpayer_id;
    var service_id = req.user.service_id;
    tax_payers
      .findOne({ where: { taxpayer_id: taxpayer_id, service_id: service_id } })
      .then(function (result) {
        result.destroy().then(function (result1) {
          var fileName = `./uploads/${result.photo_url}`;
            fs.unlink(fileName, function (err) {
              if (err) throw err;
              // if no error, file has been deleted successfully
              console.log('File deleted!');
              res.redirect("/api/tax_payer/tax_payers/add_tax_payers");
          }); 
          
        });
      });
  }
);

// tax_payer_items route
revenue.get(
  "/api/tax_payer/tax_payers/view_tax_payers/:taxpayer_id",
  (req, res) => {
    var taxpayer_id = req.params.taxpayer_id;
    var service_id = req.user.service_id;
    tax_payers
      .findOne({ where: { taxpayer_id: taxpayer_id, service_id: service_id } })
      .then(function (result) {
        res.render("./tax_payers_item/view_individual_tax_payers", {
          result: result,
        });
        //console.log(result.photo_url)
      });
  }
);

// tax_payer_items route
revenue.get(
  "/api/tax_payer/tax_payer_items/add_tax_payer_items",
  (req, res) => {
    db.query(`select * from _cities`).then(function (city) {
      db.query(`select * from _streets`).then(function (street) {
        db.query(`select revenue_id, revenue_name  from revenues`).then(
          function (revenue) {
            db.query(`SELECT * from tax_payer_items `).then(function (result) {
              res.render("./tax_payers_item/tax_payer_item", {
                result: result[0],
                city: city[0],
                street: street[0],
                revenue: revenue[0],
              });
            });
          }
        );
      });
    });
  }
);

revenue.post( "/api/tax_payer/tax_payer_items/add_tax_payer_items", isAdmin,
  (req, res) => {
    let Tax_payer_items = new tax_payer_items({
      tax_id: req.body.tax_id,
      revenue_name: req.body.revenue_name,
      revenue_id: req.body.revenue_id,
      taxpayer_name: req.body.taxpayer_name,
      street_id: req.body.street_id,
      // city_id : req.body.city_id,
      revenue_amount: req.body.revenue_amount,
    });
    Tax_payer_items.save().then(function (result) {
      if (result) {
        res.redirect("/api/tax_payer/tax_payer_items/add_tax_payer_items");
      }
    });
  }
);

//update individual taxPayer item
revenue.get("/api/tax_payer_item/view_payer_item", (req, res) => {
  var service_id = req.user.service_id;
  Tax_payer_items.findAll({ where: { service_id: service_id } }).then(function (
    result
  ) {
    res.render("", {});
  });
});

//update individual taxPayer item
revenue.get(
  "/api/tax_payer_item/view_payer_item/:tax_payer_items_id",
  (req, res) => {
    var tax_payer_items_id = req.params.tax_payer_items_id;
    var service_id = req.user.service_id;
    tax_payer_items
      .findOne({ where: { tax_payer_items_id: tax_payer_items_id } })
      .then(function (result) {
        db.query(`select * from _cities`).then(function (city) {
          db.query(`select * from _streets `).then(function (street) {
            db.query(`select *  from revenues `).then(function (revenue) {
              res.render("./tax_payers_item/updates/update_taxpayer_item", {
                city: city,
                street: street,
                revenue: revenue[0],
                result: result,
                tax_payer_items_id,
              });
            });
          });
        });
      });
  }
);

//update individual taxPayer item
revenue.put(
  "/api/tax_payer_item/update_taxpayer_item/:tax_payer_items_id", isAdmin,
  (req, res) => {
    var tax_payer_items_id = req.params.tax_payer_items_id;
    var service_id = req.user.service_id;
    tax_payer_items
      .findAll({
        where: {
          service_id: service_id,
          tax_payer_items_id: tax_payer_items_id,
        },
      })
      .then(function (success) {
        success.update(req.body, { new: true }).then(function (result) {
          if (result) {
            res.redirect("/api/tax_payer_item/view_payer_item");
          }
        });
      });
  }
);

//delete individual taxPayer item
revenue.delete(
  "/api/tax_payer_item/delete_taxpayer_item/:tax_payer_items_id", isAdmin,
  (req, res) => {
    var tax_payer_items_id = req.params.tax_payer_items_id;
    var service_id = req.user.service_id;
    tax_payer_items
      .findAll({
        where: {
          service_id: service_id,
          tax_payer_items_id: tax_payer_items_id,
        },
      })
      .then(function (success) {
        success.destroy().then(function (result) {
          if (result) {
            res.redirect("/api/tax_payer_item/view_payer_item");
          }
        });
      });
  }
);

// view mandate list
revenue.get("/api/revenue/mandate/dashboard", (req, res) => {
  res.render("./mandate/dashboard_copy");
});

// view mandate list
revenue.get("/api/revenue/view_mandate", (req, res) => {
  var service_id = req.user.service_id;
  db.query(`select * from revenue_invoices where service_id = ${service_id} and invoice_number = invoice_number ORDER BY revenue_invoice_id DESC`).then(function (result) {
    res.render("./mandate/view_mandate", {
      result: result[0],
    });
  });
});
// print invoice route
revenue.get("/api/revenue/view_mandate/print/:revenue_invoice_id/:invoice_number/:tin",
  (req, res) => {
    var revenue_invoice_id = req.params.revenue_invoice_id;
    var taxpayer_rin = req.params.tin;
    var invoice_number = req.params.invoice_number;
    var service_id = req.user.service_id;
    var lga_id = req.user.lga_id;
    client_services.findOne({ where: { service_id: service_id } }).then(function (service) {
        db.query(`select  * from revenue_invoices where revenue_invoice_id= ${revenue_invoice_id} and service_id= ${service_id}` ).then(function (result) {
           tax_payers.findOne({where: {taxpayer_rin:taxpayer_rin}}).then(function(taxpayer){
          db.query(`select * from revenue_invoices where  service_id= ${service_id} and invoice_number = ${invoice_number}`).then(function (result1) {
            Signature.findAll({where : {service_id:service_id}}).then(function(signature){
              res.render("./mandate/print_mandate", {
                service: service,
                result: result[0],
                result1: result1[0],
                taxpayer:taxpayer,
                signature:signature
              });
            })
            
          });

           })
        });
      });
  }
);

// view revenue invoice
revenue.get(
  "/api/revenue/view_mandate/print/:revenue_invoice_id",
  (req, res) => {
    var revenue_invoice_id = req.params.revenue_invoice_id;
    var service_id = req.user.service_id;
    db.query(
      `select * from revenue_invoices where revenue_invoice_id= ${revenue_invoice_id} and service_id= ${service_id}`
    ).then(function (revenue_invoice) {
      revenue_invoice.update(req.body, { new: true }).then(function (result) {
        if (result) {
          res.redirect("/api/revenue/view_mandate");
        }
      });
    });
  }
);

revenue.route('/api/revenue/mandate_payment')
.get(async(req, res)=>{
 
  let invoice = req.query.invoice;
  

  let apipay = await Api_Payments.findAll({where:{CustReference:invoice}})
  let result = await db.query(`SELECT * from revenue_invoices where invoice_number = ${invoice} `, {type: QueryTypes.SELECT})
    db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+invoice+'"', {type: QueryTypes.SELECT}).the
  client_services.findOne({where:{service_id:req.user.service_id}}).then(function(service){
      db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+invoice+'"', {type: QueryTypes.SELECT}).then(amountP =>{
        var grand_total = 0;
        result.forEach(function(r){
            grand_total += +r.amount
        })
        res.render('./mandate/revenue/demand_notice_manual_payment',{
          result:result,
          service:service,
          invoice,
          grand_total,
          apipay:apipay,
          amountP:amountP[0]
        })
        
      })
      
  })
})
.post(async(req, res)=>{
  function invoice(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
      n = Math.floor(Math.random() * 7);
      s += nuc[n];
      i++;
    }
    return s;
  }
  var invoice_number = invoice(7);
  var taxPayer_invoice = req.query.invoice;
  // console.log(assessment)
  let service = await client_services.findOne({where:{service_id:req.user.service_id}});
  let itemname = await db.query(`SELECT * from revenue_invoices where invoice_number = ${taxPayer_invoice} `, {type: QueryTypes.SELECT});
  var item = [];
  var taxpayrin = [];
  var grand_total = 0;
  itemname.forEach(function(r){
    item.push(r.revenue_id);
    taxpayrin.push(r.tin);
    grand_total += +r.revenue_id;
  })
  var commasep = item.join(",");
  var amt = req.body.amount;
  let buz = await businesses.findOne({where:{taxpayer_rin:taxpayrin[0]}});
  // let service = await client_services.findOne({where:{service_id:req.user.service_id}});
  db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+taxPayer_invoice+'"', {type: QueryTypes.SELECT}).then(prev =>{
    var previouseAMount =  +prev[0].amountPaid;
    console.log(previouseAMount)
    var currentAm = parseInt(previouseAMount) + parseInt(req.body.amount);
    console.log(currentAm)
    let Payment = new Api_Payments({
      service_id: req.user.service_id,
      CustReference: taxPayer_invoice,
      Amount: req.body.amount,
      PaymentLogId: Date.now(),
      PaymentMethod: "Cash",
      PaymentReference: `Cash${invoice_number}`,
      ChannelName: service.client,
      Location: service.client,
      CustomerName: service.client,
      ReceiptNo: invoice_number,
      DepositorName: req.body.depositorname,
      ItemName: commasep,
      sources: "manual"
    });
    Payment.save().then(function(result){
     
      
        var msg = `
            msg: Payment Successfull,
            Previouse Amount: ${previouseAMount}
            Current Amount: ${req.body.amount}
            Total: ${currentAm}
          `;
          var smsSender = `${service.spc_beneficiary}`;
          var message = `Dear Tax Payer, this is to confirm the payment of N${amt} for your Buz rate for the 2019/2020. Thank you`;
          console.log(buz.businessnumber)
          console.log(message)
          request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${buz.businessnumber}&body=${message}&dnd=2`, function (error, ress, body) {
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
          });
      
      req.flash('info', "Successful");
      res.redirect('back');
    }).catch(err =>{
     console.log(err)
    })
  })
})
//update revenue invoice page
// revenue.get('/api/revenue/update_mandate/change/:revenue_invoice_id', (req, res)=>{
//     var revenue_invoice_id = req.params.revenue_invoice_id;
//     var service_id = req.user.service_id;
//     db.query(`select * from revenue_invoices where revenue_invoice_id= ${revenue_invoice_id} and service_id= ${service_id}`).then(function(revenue_invoice){
//         revenues.findAll().then(function(revenues){
//             res.render('./mandate/update_revenue_invoice', {
//                 revenue_invoice:revenue_invoice[0],
//                 revenues:revenues,
//                 revenue_invoice_id
//             })
//        })
//     })

// })
//update revenue invoice
// revenue.put('/api/revenue/update_mandate/change/:revenue_invoice_id', (req, res)=>{
//     var revenue_invoice_id = req.params.revenue_invoice_id;
//     var service_id = req.user.service_id;
//     db.query(`select * from revenue_invoices where revenue_invoice_id= ${revenue_invoice_id}`)
//     .then(function(revenue_invoice){
//         revenue_invoice.update(req.body, {new:true}).then(function(result){
//             if(result){
//                 res.redirect('/api/revenue/view_mandate')
//             }
//         })
//     })

// })

//Update Bank record
revenue.get(
  "/api/revenue/update_mandate/change/:revenue_invoice_id",
  (req, res) => {
    var revenue_invoice_id = req.params.revenue_invoice_id;
    var service_id = req.user.service_id;
    let query = `select * from revenue_invoices where revenue_invoice_id= ${revenue_invoice_id} and service_id = ${service_id}`;
    db.query(query).then(function (revenue_invoice) {
      revenues.findAll().then(function (revenues) {
        res.render("./mandate/update_revenue_invoice", {
          revenue_invoice: revenue_invoice[0],
          revenues: revenues,
          revenue_invoice_id,
        });
      });
    });
  },
  // updating
  revenue.post(
    "/api/revenue/update_mandate/change/:revenue_invoice_id", isAdmin,
    async (req, res) => {
      var revenue_invoice_id = req.params.revenue_invoice_id;
      var revenue_id = req.body.revenue_id;

      let query = `UPDATE revenue_invoices SET revenue_id = "${revenue_id} " where revenue_invoice_id = ${revenue_invoice_id}`;

      db.query(query).then(function (result) {
        res.redirect("/api/revenue/view_mandate");
      });
    }
  )
);
// delect revenue involce
revenue.delete(
  "/api/revenue/delete_mandate/delete/:revenue_invoice_id", isAdmin,
  async (req, res) => {
    var revenue_invoice_id = req.params.revenue_invoice_id;

    let query = `DELETE FROM revenue_invoices WHERE  revenue_invoice_id = ${revenue_invoice_id}`;

    db.query(query).then(function (result) {
      res.redirect("/api/revenue/view_mandate");
    });
  }
);

// search mandate list
revenue.post("/api/revenue/view_mandate", isAdmin, (req, res) => {
  var tin = req.body.tin;
  var invoice_number = req.body.invoice_number;
  var taxpayer_name = req.body.taxpayer_name;
  var tin = req.body.tin;
  var tin = req.body.tin;
  RevenuesInvoices.findAll({ where: {} });
});
revenue.get("/api/revenue/revenue_settings", (req, res) => {
  _business_operations.findAll().then(function (buzoperation) {
    res.render("./revenue/revenue_settings", {
      buzoperation: buzoperation,
    });
  });
});

revenue.post("/api/revenue/revenue_settings", isAdmin, (req, res) => {
  var RevenueSettings = new Revenuesettings({
    BusinessOperation: req.body.BusinessOperation,
    Maximum: req.body.Maximum,
    Minimum: req.body.Minimum,
    Medium: req.body.Medium,
    ExtraLarge: req.body.ExtraLarge,
    service_id: req.user.service_id,
  });
  RevenueSettings.save().then(function (result) {
    if (result) {
    } else {
    }
  });
});

//generate individual mandate route
revenue.get("/api/revenue/generate_individual_mandate", (req, res) => {
  revenues.findAll().then(function (taxpayeritem) {
    tax_payers.findAll().then(function (taxpayer) {
      res.render("./mandate/generate_individual_mandate", {
        taxpayeritem: taxpayeritem,
        taxpayer: taxpayer,
      });
    });
  });
});
;
revenue.post("/api/revenue/generate_individual_mandate", isAdmin, (req, res) => {
  
  var items = [req.body];

  var taxpayer_name = req.body.taxpayer_name;
  var year = req.body.year;
  var month = req.body.month;
  var day = req.body.day;

  var entered_by = req.user.name;

  var service_id = req.user.service_id;

  // generating invoice number
  function invoice(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
      n = Math.floor(Math.random() * 7);
      s += nuc[n];
      i++;
    }
    return s;
  }
  var invoice_number = invoice(7);

  for (var n of items) {
    var m = n.revenue_name;
    for (var i = 0; i < m.length; i++) {
      var mm = m[i];
      //console.log(mm)
      revenues.findOne({ where: { revenue_name: mm } }).then(function (revenue) {
        tax_payers.findOne({ where: { taxpayer_name: taxpayer_name } }).then(function (taxpayer) {
          let sql ="INSERT INTO revenue_invoices(tin, taxpayer_name, revenue_id, amount, year, month, day, entered_by, invoice_number, service_id) VALUES (?,?,?,?,?,?,?,?,?,?)";
           db.query(sql, [taxpayer.taxpayer_rin, taxpayer_name,revenue.revenue_name,revenue.amount,year,month,day,entered_by,invoice_number,service_id],(err, result) => {
              if (err) {
                throw err;
              } else {
                console.log(result);
              }
          });
        })
      });
    }
  }
  req.flash('success', `'Processed!' ${taxpayer_name} invoce number: ${invoice_number}`);
  res.redirect("/api/revenue/generate_individual_mandate");
});

//taxpayer generate  mandate route
revenue.get(
  "/api/revenue/taxpayer_generate_mandate/:taxpayer_id/:taxpayer_rin", 
  (req, res) => {
    var taxpayer_id = req.params.taxpayer_id;
    var taxpayer_rin = req.params.taxpayer_rin;
    revenues.findAll().then(function (taxpayeritem) {
      tax_payers.findOne({ where: { taxpayer_id: taxpayer_id, taxpayer_rin: taxpayer_rin }, }).then(function (taxpayer) {
          businesses.findOne({ where: { taxpayer_rin: taxpayer_rin } }).then(function (buz) {
              res.render("./mandate/generateMandate", {
                taxpayeritem: taxpayeritem,
                taxpayer: taxpayer,
                buz: buz,
                taxpayer_id,
                taxpayer_rin,
              });
            });
        });
    });
  }
);
const con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.NAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DATABASE,
});
revenue.post("/api/revenue/taxpayer_generate_mandate/:taxpayer_id/:taxpayer_rin", isAdmin, (req, res) => {
    // var taxpayer_id = req.params.taxpayer_id;
    // var taxpayer = req.params.taxpayer_rin
    var items = [req.body];

    var taxpayer_name = req.body.taxpayer_name;
    var taxpayer_rin = req.body.taxpayer_rin;
    var year = req.body.year;
    var month = req.body.month;
    var day = req.body.day;

    var entered_by = req.user.name;

    var service_id = req.user.service_id;

    // generating invoice number
    function invoice(len) {
      len = len || 100;
      var nuc = "0123456789";
      var i = 0;
      var n = 0;
      s = "";
      while (i <= len - 1) {
        n = Math.floor(Math.random() * 7);
        s += nuc[n];
        i++;
      }
      return s;
    }
    var invoice_number = invoice(7);

    for (var n of items) {
      var m = n.revenue_name;
      for (var i = 0; i < m.length; i++) {
        var mm = m[i];
        //console.log(mm)
        revenues
          .findOne({ where: { revenue_name: mm } })
          .then(function (revenue) {
            let sql ="INSERT INTO revenue_invoices(tin,  taxpayer_name, revenue_id, amount, year, month, day, entered_by,  invoice_number, service_id) VALUES (?,?,?,?,?,?,?,?,?,?)";
            con.query(sql,[taxpayer_rin, taxpayer_name,revenue.revenue_name,revenue.amount,year,month, day, entered_by, invoice_number,service_id,],(err, result) => {
                if (err) throw err;
                console.log(result);
              }
            );
          });
      }
    }
    req.flash('success', `'Processed!' ${taxpayer_name} invoce number: ${invoice_number}`);
    res.redirect(`/api/tax_payer/tax_payers/add_tax_payers`);
  }
);
revenue.get('/api/admin/import/excel_file', async(req, res)=>{
    Revenue_upload.findAll({where:{service_id: req.user.service_id}}).then(result =>{
      var path = 'upload/';
      console.log(path)
      res.render('./mandate/upload_excel', {
        result:result,
        service:service
      });
    })
  
});

revenue.post('/api/admin/import/excel_file', upload.single("uploadfile"), (req, res) =>{
                  // generating invoice number
         
      readXlsxFile( 'uploads/' + req.file.filename).then((rows) => {
        // `rows` is an array of rows
        // each row being an array of cells.   
        // console.log(rows);
        // Remove Header ROW
        rows.shift();
        let uploads = [];
        rows.forEach((rows) => {
          let upload = {
              assessment_no:rows[1],
              revenue_code:rows[2],
              bill_ref_no:rows[3],
              name_of_business:rows[4],
              address_of_property:rows[5],
              type_of_property:rows[6],
              annaul_value:rows[7],
              rate_payable:rows[8],
              arrears:rows[9],
              grand_total:rows[10],
              rate_year:rows[11],
              rate_district:rows[12],
              previous_balance:rows[13],
              penalty:rows[14],
              remark:rows[15],
              service_id:`${req.user.service_id}`,
            };
          uploads.push(upload);
        });
        console.log(uploads)
        Revenue_upload.bulkCreate(uploads).then(result =>{
          var file =  req.file.filename;
          var deleteFile = `./uploads/${file}`;
          fs.unlink(deleteFile, function(err){
            if (err){
              console.log(err.message)
            }
          })
          req.flash('success', `Uploaded`);
          res.redirect('/api/admin/import/excel_file')
        }).catch(err =>{
          req.flash('success', `Filed ${err}`);
          res.redirect('/api/admin/import/excel_file')
        });
        
      })
});

revenue.get('/api/revenue/print/uploaded_revenue/:id', async(req, res)=>{
  var id = req.params.id;
  const sign = await Signature.findAll({where:{service_id: req.user.service_id}});

  Revenue_upload.findOne({where:{id:id, service_id:req.user.service_id}}).then(notice =>{
    client_services.findOne({ where: { service_id: req.user.service_id } }).then(function (service) {
      _states.findOne({where:{state_id:service.state}}).then(function(client_state){
        res.locals.notice = notice;
        res.render('./mandate/demand_notice_invoice', {
          // notice:notice,
          service:service,
          client_state:client_state,
          sign
        
        })
      })
    });
  });
});


//Printing Multiple 
revenue.get('/api/revenue/print/demand_notice_revenue', async(req, res)=>{
  var limit = 5;
  var offset = 0;
  const sign = await Signature.findAll({where:{service_id: req.user.service_id}});
  Revenue_upload.count({where:{service_id:req.user.service_id}}).then(function(count ){
    Revenue_upload.findAll({where:{service_id:req.user.service_id}, limit: limit,
      offset: offset}).then(notice =>{
      client_services.findOne({ where: { service_id: req.user.service_id } }).then(function (service) {
        _states.findOne({where:{state_id:service.state}}).then(function(client_state){
          res.locals.notice = notice;
          res.render('./mandate/demand_notice_invoice_multiple', {
            // notice:notice,
            service:service,
            client_state:client_state,
            sign,
            counts :count, limit
          
          })
        })
      });
    });
  })
});

revenue.get('/api/revenue/print/demand_notice_revenue/:first/:limit', (req, res)=>{
  var limit = parseInt(req.params.limit);
  var offset = parseInt(req.params.first);
  Revenue_upload.findAll({where:{service_id:req.user.service_id}, limit: limit, offset: offset}).then(result =>{
    res.send(result);
  })  
});




// delete assessment 
revenue.get('/api/revenue/delete/uploaded_revenue/:id', (req, res)=>{
  var id = req.params.id;
  Revenue_upload.findOne({where:{id:id, service_id:req.user.service_id}}).then(assess =>{
    assess.destroy().then(result =>{
      req.flash('success', `${assess.name_of_business} Deleted`);
          res.redirect('/api/admin/import/excel_file')
    })
  }).catch(err =>{
    req.flash('success', `${assess.name_of_business} Deleted`);
    res.redirect('/api/admin/import/excel_file')
  });
});


revenue.route('/api/revenue/demand_notice_payment')
.get(async(req, res)=>{
  let assessment_no = req.query.assessment;
  let apipay = await Api_Payments.findAll({where:{CustReference:assessment_no}})
  client_services.findOne({where:{service_id:req.user.service_id}}).then(function(service){
    Revenue_upload.findOne({where:{assessment_no:assessment_no}}).then(function(result){
      db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+assessment_no+'"', {type: QueryTypes.SELECT}).then(amountP =>{
        res.render('./mandate/demand_notice_manual_payment',{
          result:result,
          service:service,
          assessment_no,
          apipay:apipay,
          amountP:amountP[0],
        })
      })
    })
  })
})
.post(async(req, res)=>{
  
  function invoice(len) {
    len = len || 100;
    var nuc = "0123456789";
    var i = 0;
    var n = 0;
    s = "";
    while (i <= len - 1) {
      n = Math.floor(Math.random() * 7);
      s += nuc[n];
      i++;
    }
    return s;
  }
  var invoice_number = invoice(7);
  var assessment = req.query.assessment;
  console.log(assessment)
  let service = await client_services.findOne({where:{service_id:req.user.service_id}});
  let demand = await Revenue_upload.findOne({where:{assessment_no:assessment}});
  db.query('SELECT SUM(amount) as amountPaid from api_payments where CustReference = "'+assessment+'"', {type: QueryTypes.SELECT}).then(prev =>{
    var previouseAMount =  +prev[0].amountPaid;
    console.log(previouseAMount)
    var currentAm = parseInt(previouseAMount) + parseInt(req.body.amount);
    console.log(currentAm)
    let Payment = new Api_Payments({
      service_id: req.user.service_id,
      CustReference: assessment,
      Amount: req.body.amount,
      PaymentLogId: Date.now(),
      PaymentMethod: "Cash",
      PaymentReference: `Cash${invoice_number}`,
      ChannelName: service.client,
      Location: service.client,
      CustomerName: service.client,
      ReceiptNo: invoice_number,
      DepositorName: req.body.depositorname,
      ItemName: demand.type_of_property,
      sources: "manual"
    });
  
    Payment.save().then(function(result){
      Revenue_upload.findOne({where:{assessment_no:assessment}}).then((up)=>{
     
        var total = parseInt(up.grand_total) - parseInt(up.goodwill);
        var msg = `
            msg: Payment Successfull,
            Previouse Amount: ${previouseAMount}
            Current Amount: ${req.body.amount}
            Total: ${currentAm}
          `;
          var message = `Dear Tax Payer, this is to confirm the payment of ${req.body.amount} for your tenement rate for the 2019/2020. Thank you`;
          var phoneNumber = up.phone_number;
          var smsSender = `${service.spc_beneficiary}`;
          console.log(smsSender)
        if(total == currentAm){
          up.update({payment_status:1}, {new:true});
         
          request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phoneNumber}&body=${message}&dnd=2`, function (error, ress, body) {
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
          });
           req.flash('info', msg);
           res.redirect('back');
        } else if(total > currentAm){
          up.update({payment_status:2}, {new:true});
          request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phoneNumber}&body=${message}&dnd=2`, function (error, ress, body) {
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
          });
          req.flash('info', msg);
          res.redirect('back');
        } else {
          up.update({payment_status:3}, {new:true});
          request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phoneNumber}&body=${message}&dnd=2`, function (error, ress, body) {
            console.error('error:', error); // Print the error if one occurred
            console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
          });
          req.flash('info', msg);
          res.redirect('back');
        }
        // up.update()
      })
      
      // req.flash('info', "Successful");
      // res.redirect('back');
    }).catch(err =>{
     console.log(err)
    })
  })
  
})

revenue.get('/api/report/view/api_payment/delete', (req, res)=>{
  var Receipt = req.query.receptNo;
  Api_Payments.findOne({where:{ReceiptNo:Receipt}}).then(function(result){
    result.destroy();
    req.flash('info', "deleted!");
    res.redirect('back');
  })
});
revenue.get('/api/revenue/demand_notice_payment/discount', (req, res)=>{
  var ass = req.query.assessment;
  console.log(ass)
  Revenue_upload.findOne({where:{assessment_no:ass}}).then(function(result){
    res.render('./mandate/discount_deduction',{
      ass,
      result:result
    })
  })
});
// const fetch = require('node-fetch');

revenue.post('/api/revenue/demand_notice_payment/discount', (req, res)=>{
  Revenue_upload.findOne({where:{assessment_no:req.query.assessment}}).then(function(result){
    result.update({goodwill:req.body.amount}, {new:true}).then(r =>{
      var message = `Dear Tax Payer, this is to confirm the payment of your tenement rate for the 2019/2020. Thank you`;
      request("https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=aLPHAcollect&to=2348130043058&body='"+message+"'&dnd=2", function (error, ress, body) {
        console.error('error:', error); // Print the error if one occurred
        console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for the Google homepage.
      });
      
      
      
    })
  
    res.redirect('/api/admin/import/excel_file');
  })
})

// update uploaded excel file
revenue.route('/api/admin/update_tax_payer_details')
.get(async(req, res)=>{
  res.render('./mandate/update_uploade_excel_file')
})
.post(upload.single("updatetaxpayer"), async(req, res)=>{
  readXlsxFile( 'uploads/' + req.file.filename).then((rows) => {
    // `rows` is an array of rows
    // each row being an array of cells.   
    // console.log(rows);
    // Remove Header ROW
    rows.shift();
    let uploads = [];
    rows.forEach((rows) => {
      let upload = {
          assessment_no:rows[1],
          phone_number: rows[2]
        };
      uploads.push(upload);
      Revenue_upload.findOne({where:{service_id:req.user.service_id,assessment_no:upload.assessment_no }}).then(result =>{
        result.update(upload, {new:true});
      })
    });
      var file =  req.file.filename;
      var deleteFile = `./uploads/${file}`;
      fs.unlink(deleteFile, function(err){
        if (err){
          console.log(err.message)
        }
      })
    req.flash('success', `Updated!!!`);
    res.redirect('/api/admin/update_tax_payer_details')
    
    
  })
})




module.exports = revenue;
