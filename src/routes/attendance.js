const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
moment = require('moment')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db');
const { studentregistration } = require('../models/studentModel/studentModel');
const { attendance } = require('../models/performance/performanceModel');
const { client_services } = require('../models/client_service_and_bufferModel');
const { classtable } = require('../models/classModel/classModel');

const attend = express.Router();

attend.route('*')
    .get(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    }).post(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    });
const TODAY_START = new Date().setHours(0, 0, 0, 0);
const NOW = new Date();

attend.get('/attendance', async (req, res) => {
    let classMember = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/attendance/get_class', {
        classMember
    })
})


attend.get('/get_student_in_a_class', async (req, res) => {
    let className = req.query.className;
    let classMember = await studentregistration.findAll({ where: { className: className } });
    let uploads = [];
    db.query(`SELECT * from attendance where classid ='${className}' AND  DAY(registered_date)=DAY(now()) AND schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT }). then(attend =>{
        console.log(attend.length)

        var ccc = attend.length
        if(attend.length < 1) {
            classMember.forEach(function (result) {
                var detail = {
                    schoolid: req.user.organization_id,
                    classid: className,
                    studentid: result.student_reg_Id,
                };
                uploads.push(detail);
            })
            attendance.bulkCreate(uploads).then(function (result) {
                req.flash('success', `${className} Attendance for  ${NOW.toDateString()} created!`);
                res.redirect(`/attendance/studentattendance?classname=${className}`)
            })
        } else {
            req.flash('success', `${className} Attendance for  Today ${NOW.toDateString()}  Already Generated!`);
            res.redirect(`/attendance/studentattendance?classname=${className}`)
        }
    })


})




attend.get('/studentattendance', async (req, res) => {
    let classname = req.query.classname;

    // let classN = await classtable.findOne({ where: { Id: className} });
    let classMember = await studentregistration.findAll({ where: { schoolId: req.user.organization_id } });
    // let attend = await attendance.findAll({
    //     where: {
    //         classid: classname, registered_date: {
    //             [Op.gt]: TODAY_START,
    //             [Op.lt]: NOW
    //         }
    //     }
    // });
    let attend = await db.query(`SELECT * from attendance where classid ='${classname}' AND  DAY(registered_date)=DAY(now()) AND schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT })
    res.render('./admin/attendance/attendance', {
        classMember, attend, moment
    })
});


attend.get('/take_attendance', async (req, res) => {
    try {
        const data = {
            date: Date.now(),
            attendance: 1,
            takenby: req.user.username
        };

        let attendanceid = req.query.attendanceid;
        const checkinUser = await attendance.findOne({ where: { attendanceID: attendanceid } });

        console.log(checkinUser.date)
        if ( checkinUser.date == null) {
            // user.attendance.push(data);
            // await user.save();
            checkinUser.update(data, {new:true})
            req.flash('success', 'You have been signed in for today');
            res.redirect('back')

        } else {
            req.flash("danger", "ATTENDACE HAS BEEN TAKING TODAY");
            res.redirect("back");
        }


    } catch (error) {
        console.log("ATTENDACE HAS BEEN TAKING TODAY");
        console.log(error);
    }
})

module.exports = attend;