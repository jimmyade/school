const express = require('express');
//const flash = require('connect-flash');
const multer = require('multer');
const route = express.Router();
const {_vehicle_capacities, 
    _vehicle_categories,
    _vehicle_colors,
    _vehicle_cost,
    _vehicle_make,
    _vehicle_models,
    _vehicle_types,
    _vehicle_weight, vehicles
} = require('../models/vehiclesModels/allViheclesModels')
const {Signature} = require('../models/taxPayersModels/texPayersmodels')
const db = require('../datab/db')


// function for uploading picture via multer
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "uploads/");
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname);
    },
  });
  
  const fileFilter = (req, file, cb) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
      cb(null, true);
    } else {
      cb(null, fulse);
    }
  };
  var upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 2,
    },
    fileFilter: fileFilter,
  });

  

//Vehicle capacity route
// route.get('/',(req, res)=>{
//     res.render('index')
// })

//Vehicle capacity route
route.get('/dashboard',(req, res)=>{
    
    _vehicle_capacities.findAll().then(function(result){
        res.render('index', {
            result:result
        })
    })
    
})

//Vehicle capacity route
route.get('/api/vehicle/vehicle_capacity/add_vehicle_capacity',(req, res)=>{
    _vehicle_capacities.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_capacity', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_capacity/add_vehicle_capacity',(req, res)=>{
  
    var service_id = ''
    let vehicle_capacity = new _vehicle_capacities({
        capacity : req.body.capacity,
        amount : req.body.amount
    });
    vehicle_capacity.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_capacity/add_vehicle_capacity')
        }
    })
})

// Vehicle categories
route.get('/api/vehicle/vehicle_category/add_vehicle_category',(req, res)=>{
    _vehicle_categories.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_category', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_category/add_vehicle_category',(req, res)=>{
  
    var service_id = ''
    let vehicle_category = new _vehicle_categories({
        vehicle_category : req.body.vehicle_category,
        amount : req.body.amount
    });
    vehicle_category.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_category/add_vehicle_category')
        }
    })
})


// Vehicle colors route
route.get('/api/vehicle/vehicle_colors/add_vehicle_colors',(req, res)=>{
    _vehicle_colors.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_colors', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_colors/add_vehicle_colors',(req, res)=>{
  
    var service_id = ''
    let vehicle_colors = new _vehicle_colors({
        color : req.body.color
        
    });
    vehicle_colors.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_colors/add_vehicle_colors')
        }
    })
})

// Vehicle cost route
route.get('/api/vehicle/vehicle_cost/add_vehicle_cost',(req, res)=>{
    _vehicle_cost.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_cost', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_cost/add_vehicle_cost',(req, res)=>{
  
    var service_id = ''
    let vehicle_cost = new _vehicle_cost({
        vehicle_cost : req.body.vehicle_cost
        
    });
    vehicle_cost.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_cost/add_vehicle_cost')
        }
    })
})

// Vehicle make route
route.get('/api/vehicle/vehicle_make/add_vehicle_make',(req, res)=>{
    _vehicle_make.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_make', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_make/add_vehicle_make',(req, res)=>{
  
    var service_id = ''
    let vehicle_make = new _vehicle_make({
        vehicle_make : req.body.vehicle_make
        
    });
    vehicle_make.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_make/add_vehicle_make')
        }
    })
})

// Vehicle model route
route.get('/api/vehicle/vehicle_models/add_vehicle_models',(req, res)=>{
    _vehicle_models.findAll().then(function(result){
        _vehicle_make.findAll().then(function(vehicle_make){
            db.query(`SELECT _vehicle_models.vehicle_model_id, _vehicle_models.vehicle_model, _vehicle_models.vehicle_make_id, _vehicle_models.vehicle_year, _vehicle_models.registered_by, _vehicle_models.registered_on, _vehicle_models.service_id, _vehicle_models.session_id, _vehicle_make.vehicle_make_id, _vehicle_make.vehicle_make FROM _vehicle_make , _vehicle_models WHERE _vehicle_make.vehicle_make_id = _vehicle_models.vehicle_make_id`).then(function(vehicle_makeout){
                res.render('./vehicle/add_vehicle_model', {
                    result:result,
                    vehicle_make:vehicle_make,
                    vehicle_makeout:vehicle_makeout[0]
                })
            })
        })
    })
    
})

route.post('/api/vehicle/vehicle_models/add_vehicle_models',(req, res)=>{

    var service_id = ''
    let vehicle_models = new _vehicle_models({
        vehicle_model : req.body.vehicle_model,
        vehicle_make_id: req.body.vehicle_make_id,
        vehicle_year: req.body.vehicle_year,
        
    });
    vehicle_models.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_models/add_vehicle_models')
        }
    })
})


// Vehicle types route
route.get('/api/vehicle/vehicle_types/add_vehicle_types',(req, res)=>{
    _vehicle_types.findAll().then(function(result){
        _vehicle_categories.findAll().then(function(vehicle_categosrie){
            db.query(`SELECT
            _vehicle_types.vehicles_types_id,
            _vehicle_types.vehicle_type,
            _vehicle_types.service_id,
            _vehicle_types.session_id,
            _vehicle_types.registered_by,
            _vehicle_types.registered_on,
            _vehicle_types.vehicle_category_id,
            _vehicle_categories.vehicle_category_id,
            _vehicle_categories.vehicle_category
            FROM
            _vehicle_types ,
            _vehicle_categories
            WHERE
            _vehicle_types.vehicle_category_id = _vehicle_categories.vehicle_category_id
            `).then(function(vehicle_makeout){
                res.render('./vehicle/add_vehicle_type', {
                    result:result,
                    vehicle_categosrie:vehicle_categosrie,
                    vehicle_makeout:vehicle_makeout[0]
                })
            })
        })
    })
    
})

route.post('/api/vehicle/vehicle_types/add_vehicle_types',(req, res)=>{
    var service_id = req.user.service_id
    let vehicle_types = new _vehicle_types({
        vehicle_type : req.body.vehicle_type,
        vehicle_category_id: req.body.vehicle_category_id,
        //service_id : service_id
    });
    vehicle_types.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_types/add_vehicle_types')
        }
    })
})


// Vehicle weight route
route.get('/api/vehicle/vehicle_weight/add_vehicle_weight',(req, res)=>{
    _vehicle_weight.findAll().then(function(result){
        res.render('./vehicle/add_vehicle_weight', {
            result:result
        })
    })
    
})

route.post('/api/vehicle/vehicle_weight/add_vehicle_weight',(req, res)=>{
  
    var service_id = ''
    let vehicle_weight = new _vehicle_weight({
        vehicle_weight : req.body.vehicle_weight
        
    });
    vehicle_weight.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle_weight/add_vehicle_weight')
        }
    })
})


// Vehicle  route
route.get('/api/vehicle/vehicle/add_vehicle',(req, res)=>{
    _vehicle_colors.findAll().then(function(vehicle_colors){
        _vehicle_models.findAll().then(function(vehicle_models){
            _vehicle_types.findAll().then(function(vehicle_type){
                _vehicle_make.findAll().then(function(vehicle_make){
                    _vehicle_capacities.findAll().then(function(vehicle_capacity){
                        _vehicle_cost.findAll().then(function(vehicle_cost){
                            db.query(`SELECT
                            vehicles.vehicle_id, vehicles.vin, vehicles.vehicle_make_id, vehicles.vehicle_model_id, vehicles.taxpayer_name,
                            vehicles.vehicle_color_id, vehicles.insured,  vehicles.insurer, vehicles.insurance_policy_number,
                            vehicles.policy_expiry_date,  vehicles.registration_number, vehicles.date_log,
                            vehicles.vehicle_type_id, vehicles.engine_no, vehicles.engine_capacity_id, vehicles.owner,
                            vehicles.registered_by, vehicles.year, vehicles.temp_name, vehicles.weight, vehicles.authorize_to_carry,
                            vehicles.no_of_persons, vehicles.service_id, vehicles.session_id, vehicles.authorized,
                            vehicles.authorized_by, vehicles.authorized_on, vehicles.registered_on, vehicles.vehicle_gross,
                            vehicles.Vehicle_reg_book_no, vehicles.vehicle_extra_option_id,  vehicles.vehicle_cost_id,
                            vehicles.vehicle_capacity_id,  _vehicle_make.vehicle_make_id,  _vehicle_make.vehicle_make,
                            _vehicle_models.vehicle_model_id, _vehicle_models.vehicle_model, _vehicle_colors.vehicle_color_id,
                            _vehicle_colors.color,  _vehicle_types.vehicles_types_id, _vehicle_types.vehicle_type, _vehicle_capacities.vehicle_capacity_id,
                            _vehicle_capacities.capacity, _vehicle_cost.vehicles_cost_id,  _vehicle_cost.vehicle_cost
                            FROM
                            vehicles ,  _vehicle_make ,  _vehicle_models , _vehicle_colors , _vehicle_types , _vehicle_capacities , _vehicle_cost
                            WHERE
                            vehicles.vehicle_make_id = _vehicle_make.vehicle_make_id AND
                            vehicles.vehicle_model_id = _vehicle_models.vehicle_model_id AND
                            vehicles.vehicle_color_id = _vehicle_colors.vehicle_color_id AND
                            vehicles.vehicle_type_id = _vehicle_types.vehicles_types_id AND
                            vehicles.engine_capacity_id = _vehicle_capacities.vehicle_capacity_id`).then(function(result){
                                
                                res.render('./vehicle/vehicle', {
                                    vehicle_colors:vehicle_colors,
                                    vehicle_models:vehicle_models,
                                    vehicle_type:vehicle_type,
                                    vehicle_make:vehicle_make,
                                    vehicle_capacity:vehicle_capacity,
                                    vehicle_cost:vehicle_cost,
                                    result:result[0]
                                })
                            })
                        })
                    })
                })

            })
        })
        
    })
    
})

route.post('/api/vehicle/vehicle/add_vehicle',(req, res)=>{
  
    var service_id = ''
    let Vehicles = new vehicles({
        vin : req.body.vin,
        vehicle_make_id : req.body.vehicle_make_id,
        vehicle_model_id : req.body.vehicle_model_id,
        taxpayer_name : req.body.taxpayer_name,
        vehicle_color_id : req.body.vehicle_color_id,
        vehicle_type_id : req.body.vehicle_type_id,
        engine_no : req.body.engine_no,
        engine_capacity_id : req.body.engine_capacity_id,

        owner : req.body.owner,
        year : req.body.year,
        vehicle_gross : req.body.vehicle_gross,
        vehicle_cost_id : req.body.vehicle_cost_id,
        vehicle_capacity_id : req.body.vehicle_capacity_id,
        
    });
    Vehicles.save().then(function(result){
        if(result){
            res.redirect('/api/vehicle/vehicle/add_vehicle')
        }
    })
})



module.exports = route;