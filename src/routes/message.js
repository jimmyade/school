const express = require("express");
const multer = require("multer");
const mysql = require("mysql");
const fs = require('fs');

const readXlsxFile = require('read-excel-file/node');
const request = require('request');
const {Sequelize, QueryTypes} = require('sequelize');
const Op = Sequelize.Op;

const msg = express.Router();

const {
  revenue_categories,
  revenue_dimensions,
  revenue_frequencies,
  revenues,
  RevenuesInvoices,
  Revenuesettings,
  Revenue_upload,
  Api_Payments,
} = require("../models/revenueModels/allRevenueModels");
const {
  _business_operations,businesses
} = require("../models/buildingModels/allBuildingAndBusinessModels");
const {
  tax_payer_items,
  tax_payers,
  _lgas,
  _cities,
  Signature
} = require("../models/taxPayersModels/texPayersmodels");
const { client_services } = require("../models/client_service_and_bufferModel");
const db = require("../datab/db");
var service = require("../datab/service");
var {Users} = require('../models/userModel/userModels')

// access control uthentication middleware 
const accessControl = require ('../lib/service');
const { get } = require("http");
const { _states } = require("../models/lga_state_country_cities_streetModel");
var isAdmin = accessControl.isAdmin;

msg.route('/api/admin/send_messages')
.get(async(req, res)=>{
    res.render('./mandate/demand_notice_msg')
})
.post(async(req, res)=>{
  var category = req.body.category;
  var service = await client_services.findOne({where:{service_id:req.user.service_id}});
  var smsSender = `${service.spc_beneficiary}`;
  var message = `${req.body.messages}`;
   console.log(req.body.messages)
  if(category == 1){
    Revenue_upload.findAll({where:{payment_status:category}}).then(msg =>{
      msg.forEach(cus => {
        var phone = `${cus.phone_number}`;
        // console.log(cus.assessment_no+" "+req.body.messages)
        request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phone}&body=${message}&dnd=2`, function (error, ress, body) {
          console.error('error:', error); // Print the error if one occurred
          console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
          console.log('body:', body); // Print the HTML for the Google homepage.
        });
      });
      
  res.redirect("back");
    })
  } else if(category == 2){
    Revenue_upload.findAll({where:{payment_status:category}}).then(msg =>{
      msg.forEach(cus => {
        var phone = `${cus.phone_number}`;
        // console.log(cus.assessment_no+" "+req.body.messages)
        request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phone}&body=${message}&dnd=2`, function (error, ress, body) {
          console.error('error:', error); // Print the error if one occurred
          console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
          console.log('body:', body); // Print the HTML for the Google homepage.
        });
      });
      
  res.redirect("back");
    })
  } else if(category == 3){
    Revenue_upload.findAll({where:{payment_status:category}}).then(msg =>{
      msg.forEach(cus => {
        var phone = `${cus.phone_number}`;
        // console.log(cus.assessment_no+" "+req.body.messages)
        request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phone}&body=${message}&dnd=2`, function (error, ress, body) {
          console.error('error:', error); // Print the error if one occurred
          console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
          console.log('body:', body); // Print the HTML for the Google homepage.
        });
      });
      res.redirect("back");
    })
  } else {
    Revenue_upload.findAll().then(msg =>{
      msg.forEach(cus => {
        var phone = `${cus.phone_number}`;
        // console.log(phone)
        // console.log(cus.assessment_no+" "+req.body.messages)
        request(`https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=21bRVcGh0va47twKWpnNbVsxFJGE3QfEDG7OuKhj2NxLfTXn4bqC8L0T7yJ4&from=${smsSender}&to=${phone}&body=${message}&dnd=2`, function (error, ress, body) {
          console.error('error:', error); // Print the error if one occurred
          console.log('statusCode:', ress && ress.statusCode); // Print the response status code if a response was received
          console.log('body:', body); // Print the HTML for the Google homepage.
        });
      });
      res.redirect("back");
    })
  }
})



module.exports = msg;