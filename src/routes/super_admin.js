const express = require('express');
const {Sequelize, QueryTypes} = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db')
const { client_services } = require('../models/client_service_and_bufferModel');
const { businesses } = require('../models/buildingModels/allBuildingAndBusinessModels');
const { tax_payers } = require('../models/taxPayersModels/texPayersmodels');
const { Users } = require("../models/usersMosel");
const { RevenuesInvoices } = require('../models/revenueModels/allRevenueModels');
const superadmin = express.Router();
// access control
const accessControl = require ('../lib/service');
var isSuperAdmin = accessControl.isSuperAdmin;


superadmin.get('/api/super_admin/dashboard', isSuperAdmin, async(req, res)=>{
     const {count, rows: clientservices} = await client_services.findAndCountAll({where:{service_id:{[Op.ne]: null}}})
     var YearlyRev = await db.query(`SELECT COUNT(*) as count from revenue_invoices where year(date_log) = year(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var YearlyMonth = await db.query(`SELECT COUNT(*) as count from revenue_invoices where  month(date_log) = month(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var Yearlysum = await db.query(`SELECT sum(amount) as total,  sum(amount_paid) as paid_total from revenue_invoices where year(date_log) = year(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var Monthsum = await db.query(`SELECT sum(amount) as total , sum(amount_paid) as paid_total from revenue_invoices where month(date_log) = month(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        businesses.count({where:{service_id:{[Op.ne]: null}}}).then(businesC =>{
            tax_payers.count({where:{service_id:{[Op.ne]: null}}}).then(TaxpayerC =>{
                // res.locals.clientC = result.count;
                res.render('./super_admin/dashboard_copy', {
                    count,
                    YearlyRev:YearlyRev[0], YearlyMonth:YearlyMonth[0], Yearlysum:Yearlysum[0], Monthsum:Monthsum[0],
                    businesC,
                    TaxpayerC,
                    clientservices
                })
            })
        });
     
});

superadmin.get('/api/super_admin/view_client/:client_name/:clientID/:service_id', isSuperAdmin, async(req, res)=>{
    try {
        var client_name = req.params.client_name;
        var id = req.params.clientID;
        var serviceid = req.params.service_id
        var { count, rows} = await Users.findAndCountAll({where:{service_id:serviceid}});
        var buscount = await businesses.count({where:{service_id:serviceid}});
        var taxpayercount = await tax_payers.count({where:{service_id:serviceid}});
        var YearlyRev = await db.query(`SELECT COUNT(*) as count from revenue_invoices where service_id = ${serviceid} and year(date_log) = year(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var YearlyMonth = await db.query(`SELECT COUNT(*) as count from revenue_invoices where service_id = ${serviceid} and month(date_log) = month(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var Yearlysum = await db.query(`SELECT sum(amount) as total,  sum(amount_paid) as paid_total from revenue_invoices where service_id = ${serviceid} and year(date_log) = year(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var Monthsum = await db.query(`SELECT sum(amount) as total , sum(amount_paid) as paid_total from revenue_invoices where service_id = ${serviceid} and month(date_log) = month(CURRENT_DATE());`, {type: QueryTypes.SELECT});
        var Taxpayerandbus = await db.query(`SELECT * from businesses INNER JOIN tax_payers on businesses.taxpayer_rin =  tax_payers.taxpayer_rin where tax_payers.service_id = ${serviceid}`, {type: QueryTypes.SELECT});
        client_services.findAll({where:{idclient_services:id}}).then(clientServices =>{
            res.locals.usercount = count;
            res.locals.users = rows;
            res.render('./super_admin/client_services', {
                clientServices,
                //usercount,
                buscount,
                taxpayercount,
                YearlyRev: YearlyRev[0].count,
                YearlyMonth: YearlyMonth[0].count,
                Yearlysum: Yearlysum[0],
                Monthsum: Monthsum[0],
                Taxpayerandbus:Taxpayerandbus
            });
        });
    } catch (err) {
        console.log(err)
    }
});

// superadmin.post('/api/super_admin/activat/deactivate/client_service', (req, res)=>{
//     var clientId = [req.body];
//     var status = [req.body];
   
//         for (var id of clientId) {
//             for (var st of status) {
//                 var client_id =id.id;
//                 var client_st = st.choice;
                
//                 for (var i = 0; i < client_id.length; i++) {
//                     var clientservice_id = client_id[i];
//                     var clientservice_st = client_st[i];
//                     client_services.findAll({where:{idclient_services:clientservice_id}}).then(service =>{
//                         service.forEach(function (client_status) {
//                             client_status.update({ service_status: clientservice_st }, {new:true});
//                         });
//                     })
//                 }
//             }
//         }
// })

module.exports = superadmin;

