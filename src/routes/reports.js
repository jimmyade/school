const express = require("express");
const multer = require("multer");
const mysql = require("mysql");
const fs = require('fs');
const routess = express.Router();
const db = require("../datab/db");
const accessControl = require('../lib/service');
const exam = require("../controller/examController");
const { client_services } = require("../models/client_service_and_bufferModel");
const { studentregistration } = require("../models/studentModel/studentModel");
// var isAdmin = accessControl.isAdmin;

var details = accessControl.details
const report = express.Router()



// report.route("*")
//   .get(async (req, res, next) => {
//     let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
//     res.locals.clientName =  c.client;
//     res.locals.clientCode = c.service_code;
//     next();
//   }).post(async (req, res, next) => {
//     let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
//     res.locals.clientCode = c.service_code;
//     next();
//   });
report.route('/report/sumarry_result')
  .get(details, async (req, res) => {
    exam.examSummarypage(req, res)
  })
  .post(details, async (req, res) => {
    exam.examSummary(req, res)
  })


report.route('/view_class_result')
  .get(details, async (req, res) => {
    exam.classExamReport(req, res)
  })
  .post(details, async (req, res) => {
    exam.examReport(req, res)
  });

  report.get('/print_report_sheet', details, async(req, res)=>{
    exam.resultSheet(req, res)
  })

  report.get('/print_individual_report_sheet', details, async(req, res)=>{
    exam.resultSheet(req, res)
  })


module.exports = report;
