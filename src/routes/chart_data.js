const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db')

const chartd = express.Router();

chartd.get('/get_class_name_and_count', async (req, res) => {
    let result = await db.query(`SELECT COUNT(student_reg_Id) as classcount, className FROM studentregistration WHERE schoolId = ${req.user.organization_id} GROUP BY className;`, { type: QueryTypes.SELECT });
    let result1 = await db.query(`SELECT COUNT(student_reg_Id) as count, className, gender FROM studentregistration WHERE schoolId = ${req.user.organization_id} AND gender ="Male" GROUP BY className;`, { type: QueryTypes.SELECT })
    let result2 = await db.query(`SELECT COUNT(student_reg_Id) as count, className, gender FROM studentregistration WHERE schoolId = ${req.user.organization_id} AND gender ="Female" GROUP BY className;`, { type: QueryTypes.SELECT })
    // .then(function(result){
    res.send({
        result : result,
        result1 : result1,
        result2 : result2,
    })
     console.log(result2)

})

module.exports = chartd;
