"use strict"
const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db')

//models
const adminController = require('../controller/studentRegController');
const paymentContr = require('../controller/paymentController');
const { client_services } = require('../models/client_service_and_bufferModel');
const { parent } = require('../models/parentModel/parentModel');
const parentdash = require('../controller/parentController');


const parentRoute = express.Router();


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    // if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    //     cb(null, true);
    // } else {
    //     cb(null, fulse)
    // }
    if (file.mimetype.includes("image/jpeg") || file.mimetype.includes("image/png") || file.mimetype.includes("excel") || file.mimetype.includes("spreadsheetml") || file.mimetype.includes("application/pdf")) {
        cb(null, true);
    } else {
        cb(null, false)
    }

}
var upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
});


parentRoute.route("*")
    .get(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        let pInfo = await parent.findOne({ where: { schoolId: req.user.organization_id } })
        res.locals.clientCode = c.service_code;
        res.locals.parentInfo = pInfo;
        next();
    }).post(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        let pInfo = await parent.findOne({ where: { schoolId: req.user.organization_id } })
        res.locals.clientCode = c.service_code;
        res.locals.parentInfo = pInfo;
        next();
    });

parentRoute.route('/dashboard')
    .get(async (req, res) => {
        parentdash.parentDashboard(req, res);
    })

parentRoute.route('/view_wards')
    .get(async (req, res) => {
        parentdash.wards(req, res);
    })

parentRoute.route('/kids/school/fees')
    .get(async (req, res) => {
        parentdash.individualFee(req, res);
    })


parentRoute.route('/kids/review_result_')
    .get(async (req, res) => {
        parentdash.classExamReport(req, res);
    })
parentRoute.get('/kids/school/fees_checkpoint', async (req, res) => {
    parentdash.individualPayReview(req, res)
})
parentRoute.get('/school_fee_breakdown', async (req, res) => {
    parentdash.feesBreakDown(req, res)
})

parentRoute.route('/kids/school/fees/check_point')
    .get(async (req, res) => {
        parentdash.feeChecPoint(req, res);
    })
// school related activities 



parentRoute.get('/school_related_activity_center', async (req, res) => {
    res.render('./parent/performance/skills_center')
})

// performance
parentRoute.route('/view/performance/punctuality')
    .get(async (req, res) => {
        parentdash.getPunctuality(req, res);
    })

parentRoute.get('/view/performance/get_punctuality', async (req, res) => {
    parentdash.getClassPunctuality(req, res);
})

parentRoute.get('/view/students_attention_skills', async (req, res) => {
    parentdash.attentionPage(req, res);
});

parentRoute.get('/view/student_attention_skills_per-class', (req, res) => {
    parentdash.getAttention(req, res);
})

// work habit
parentRoute.get('/view/students_workhbit', async (req, res) => {
    parentdash.workhabitPage(req, res);
})
parentRoute.get('/view/wards_workhbit', async (req, res) => {
    parentdash.getWorkhabit(req, res);
})
// pychomotorskills
parentRoute.get('/view/pychomotorskills', async (req, res) => {
    parentdash.pychomotorskillsPage(req, res);
})
parentRoute.get('/view/get_wards_pychomotorskills', async (req, res) => {
    parentdash.getPychomotorskills(req, res);
})


parentRoute.route('/wards_result')
    .get(async (req, res) => {
        parentdash.getWardsResultDetails(req, res)
    })
    .post(async (req, res) => {
        parentdash.wardsResultDetails(req, res)
    })
module.exports = parentRoute;