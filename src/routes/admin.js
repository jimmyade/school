const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const auth = require('../lib/JWT')
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db')
const adminController = require('../controller/studentRegController')
const applicantRegController = require('../controller/applicantRegController')
const settingsController = require('../controller/settingsController');
const performaceController = require('../controller/performanceController')
const { punctuality } = require('../models/performance/performanceModel');
const { classtable, class_groups } = require('../models/classModel/classModel');
const classAndSubjectCont = require('../controller/classController');
const examController = require('../controller/examController');
const calenderAndGradebook = require('../controller/calenderAndGradebookController');
const { client_services } = require('../models/client_service_and_bufferModel');
const adminPanelController = require('../controller/adminController');
const paymentContr = require('../controller/paymentController');
const inventory = require('../controller/expensiveRequestController');
const downlformat = require('../controller/formatDownloadController');
const { studentregistration } = require('../models/studentModel/studentModel');
const { schoolcategory, shlsession } = require('../models/basicTablesModel');
const { details } = require('../lib/service');


const admin = express.Router();
// admin.use(auth.validateUser);

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    // if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    //     cb(null, true);
    // } else {
    //     cb(null, fulse)
    // }
    if (file.mimetype.includes("image/jpeg") || file.mimetype.includes("image/png") || file.mimetype.includes("excel") || file.mimetype.includes("spreadsheetml") || file.mimetype.includes("application/pdf")) {
        cb(null, true);
    } else {
        cb(null, false)
    }

}
var upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
});


admin.route('*')
    .get(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    }).post(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    });


admin.get('/dashboard', async (req, res) => {
    adminPanelController.adminDashboard(req, res)
})
admin.get('/payment/dashboard', async (req, res) => {
    adminPanelController.paymentDash(req, res)
})
admin.get('/report/dashboard', async (req, res) => {
    adminPanelController.adminPanel(req, res)
})
admin.get('/panel/dashboard', async (req, res) => {
    adminPanelController.adminPanel(req, res)
})
admin.get('/inventory/dashboard', async (req, res) => {
    adminPanelController.adminPanel(req, res)
})

admin.get('/payment_list/panel', function (req, res) {
    res.render('./admin/payment/payment_panel')
})

admin.get('/admission/panel', (req, res) => {
    res.render('./admin/admission/panel')
})

admin.route('/admission/registration')
    .get(async (req, res) => {
        adminController.studentRegistration(req, res)
    })
    .post(upload.single('photo'), async (req, res) => {
        adminController.createStudent(req, res)
    })
admin.route('/ulpload_student/records')
    .get(async (req, res) => {
        adminController.get_upload_page(req, res);
    })
    .post(upload.single('studentupload'), async (req, res) => {
        adminController.upload_students(req, res)
    })

admin.route('/view_/registered_students')
    .get(async (req, res) => {
        adminController.getAllRegisterdSt(req, res);
    })
admin.route('/search_/students')
    .get(async (req, res) => {
        adminController.getSearch(req, res)
    })
    .post(async (req, res) => {
        adminController.search(req, res)
    })
admin.get('/get/class_categories', async (req, res) => {
    var classid = await classtable.findOne({ where: { className: req.query.category } })
    console.log(classid.Id)
    class_groups.findAll({ where: { class_level: classid.Id } }).then(result => {
        res.send(result);
    })
    // })
})
// applicant 
admin.route('/aplicant_registration')
    .get(async (req, res) => {
        applicantRegController.getApplicatepage(req, res);
    })
    .post(upload.single('photo'), async (req, res) => {
        applicantRegController.createApplicant(req, res);
    })
admin.get('/aplicant_profile', async (req, res) => {
    applicantRegController.viewStaffDetails(req, res);
})

admin.post('/update_staff_personal_info', async (req, res) => {
    applicantRegController.updStaffInform(req, res)
})
admin.post('/update_staff_acd_info', async (req, res) => {
    applicantRegController.staffAcdInfo(req, res)
})

admin.route('/aplicant_update_details')
    .get(async (req, res) => {
        applicantRegController.getStaffUpdatePage(req, res);
    })
    .post(async (req, res) => {
        applicantRegController.updateStaffinfo(req, res);
    });

admin.get('/aplicant_update_details', async (req, res) => {
    applicantRegController.viewStaffDetails(req, res);
})
// parents account 
admin.route('/parent_account/registration')
    .get(async (req, res) => {
        adminController.parentLogin(req, res);
    })
    .post(upload.single('passp'), async (req, res) => {
        adminController.createParentLogin(req, res);
    })

admin.get('/settings/setup_panel', async (req, res) => {
    res.render('./admin/settings_panel')
})
// settings sessions
admin.route('/settings/accademic_session_setup')
    .get(async (req, res) => {
        settingsController.sesionSetup(req, res);
    })
    .post(async (req, res) => {
        settingsController.createSesionSetup(req, res);
    });

// subject settings
admin.route('/settings/subject_setup')
    .get(async (req, res) => {
        settingsController.getSubjectPage(req, res)
    })
    .post(async (req, res) => {
        settingsController.createSubject(req, res)
    })

// term setup
admin.route('/setting/term_setup')
    .get(async (req, res) => {
        settingsController.getTermPage(req, res);
    })
    .post(async (req, res) => {
        settingsController.AddTermSetup(req, res);
    })
admin.get('/setting/term_setup/delete', async (req, res) => {
    settingsController.deleteTermSetup(req, res)
})
admin.route('/setting/term_setup/edit')
    .get(async (req, res) => {
        settingsController.getUpdateTermSetup(req, res);
    })
    .post(async (req, res) => {
        settingsController.updateTermSetup(req, res);
    })

// Teachers settings 
admin.route('/settings/teacher_setup')
    .get(function async(req, res) {
        settingsController.getTeachersetup(req, res);
    })
    .post(async (req, res) => {
        settingsController.assignSubjectToTeacher(req, res);
    })
// teachers settings update 
admin.route('/settings/teacher_setup/edit')
    .get(async (req, res) => {
        settingsController.getUpdateTeachersS(req, res)
    })
    .post(async (req, res) => {
        settingsController.updateTeacherSettings(req, res)
    })
// a
admin.get('/settings/teacher_setup/delete', async (req, res) => {
    settingsController.deleteTeacherSettings(req, res)
})

// calender setting 
admin.get('/settings/calender_setup_panel', async (req, res) => {
    res.render('./admin/calender/calender_setup_panel')
})

admin.route('/settings/calender/calender_type')
    .get(async (req, res) => {
        calenderAndGradebook.calenderType(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.addCanderType(req, res);
    });
// update 
admin.route('/settings/calender/calender_type/edit')
    .get(async (req, res) => {
        calenderAndGradebook.getUpdateCalenderType(req, res)
    })
    .post(async (req, res) => {
        calenderAndGradebook.UpdateCalenderType(req, res)
    });

admin.get('/settings/calender/calender_type/delete', async (req, res) => {
    calenderAndGradebook.deleteCalenderType(req, res)
});

admin.route('/settings/calender/calender_type_periods')
    .get(async (req, res) => {
        calenderAndGradebook.calenderTypePeriod(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.addCalenderTypePeriod(req, res)
    })

// 
admin.route('/settings/calender/calender_type_periods/edit')
    .get(async (req, res) => {
        calenderAndGradebook.getUpdateCalenderTypePeriod(req, res)
    })
    .post(async (req, res) => {
        calenderAndGradebook.updateCalenderTypePeriod(req, res)
    });
admin.get('/settings/calender/calender_type_periods/delete', async (req, res) => {
    calenderAndGradebook.deleteCalenderTypePeriod(req, res)
})


// calender_year_settings
admin.route('/settings/calender/calender_year_settings')
    .get(async (req, res) => {
        calenderAndGradebook.calender_year_settings(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.addcalender_year_settings(req, res);
    })
// update 
admin.route('/settings/calender/calender_year_settings/edit')
    .get(async (req, res) => {
        calenderAndGradebook.getUpdateCalender_year_settings(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.UpdateCalender_year_settings(req, res);
    })

admin.get('/settings/calender/calender_year_settings/delete', async (req, res) => {
    calenderAndGradebook.deleteCalender_year_settings(req, res);
})
admin.route('/settings/calender/calender_year_period_setting')
    .get(async (req, res) => {
        calenderAndGradebook.calender_year_period_setting(req, res)
    })
    .post(async (req, res) => {
        calenderAndGradebook.addcalender_year_period_settings(req, res);
    })



//skill managemtn
admin.get('/skill_management_settings', async (req, res) => {
    res.render('./admin/performance/skills_center')
})

// performance
admin.route('/view/performance/punctuality')
    .get(async (req, res) => {
        performaceController.getPunctuality(req, res);
    })
admin.get('/view/performance/get_punctuality', async (req, res) => {
    performaceController.getClassPunctuality(req, res);
})
admin.post('/upload/performance/get_punctuality', upload.single('punctuality'), async (req, res) => {
    performaceController.uploadPunctuality(req, res)
})


admin.get('/view/students_attention_skills', async (req, res) => {
    performaceController.attentionPage(req, res);
});

admin.get('/view/student_attention_skills_per-class', (req, res) => {
    performaceController.getAttention(req, res);
})

admin.post('/upload/students_attention_skills', upload.single('attention_skills'), async (req, res) => {
    performaceController.uploadAttention(req, res)
})

// work habit
admin.get('/view/students_workhbit', async (req, res) => {
    performaceController.workhabitPage(req, res);
})
admin.get('/view/students_workhbits', async (req, res) => {
    performaceController.getWorkhabit(req, res);
})
admin.post('/upload/students_workhbit', upload.single('students_workhbit'), async (req, res) => {
    performaceController.uploadWorkhabit(req, res)
})

// pychomotorskills
admin.get('/view/pychomotorskills', async (req, res) => {
    performaceController.pychomotorskillsPage(req, res);
})
admin.get('/view/pychomotorskill', async (req, res) => {
    performaceController.getPychomotorskills(req, res);
})
admin.post('/upload/pychomotorskills', upload.single('pychomotorskills'), async (req, res) => {
    performaceController.uploadPychomotorskills(req, res)
})


// subject Center
admin.get('/subject_center', async (req, res) => {
    res.render('./admin/subject/sunject_course_center')
});

admin.route('/subject_center/courese_subject')
    .get(async (req, res) => {
        classAndSubjectCont.courseSubject(req, res);
    })
    .post(async (req, res) => {
        classAndSubjectCont.addCourseSubject(req, res)
    });


admin.get('/view/subject_center/get_course_subject/:classid', (req, res) => {
    classAndSubjectCont.getCourseSubjectByClass(req, res);
})

// exam panel
admin.get('/examination/exam_panel', async (req, res) => {
    examController.examPanel(req, res)
})

// exam page
admin.route('/exam/exam_management/create_exam')
    .get(async (req, res) => {
        examController.getCreateExampage(req, res);
    })

admin.route('/exam/exam_management/create_exam/class')
    .get(async (req, res) => {
        examController.getExamClass(req, res);
    })
    .post(async (req, res) => {
        examController.addExamClassSubjects(req, res)
    })


admin.get('/certificate_panel', async (req, res) => {
    res.render('./admin/certificate')
})

admin.get('/settings/gradebook_setup_panel', (req, res) => {
    res.render('./admin/gradebooks/gradebook_setup_panel')
})

admin.route('/settings/gradebook/gradebook_columns')
    .get(async (req, res) => {
        calenderAndGradebook.get_gradebook_columns(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.add_gradebook_columns(req, res)
    })

admin.route('/settings/gradebook/gradebook_setings')
    .get(async (req, res) => {
        calenderAndGradebook.get_gradebook_setting(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.add_gradebook_setting(req, res);
    })

admin.route('/settings/gradebook/gradebook_template')
    .get(async (req, res) => {
        calenderAndGradebook.get_gradebook_template(req, res);
    })
    .post(async (req, res) => {
        calenderAndGradebook.add_gradebook_template(req, res)
    })

admin.route('/assessment/upload_assessment')
    .get(async (req, res) => {
        calenderAndGradebook.getassessments(req, res)
    })
    .post(upload.single('assessment'), async (req, res) => {
        calenderAndGradebook.uploadCountiniousAssessment(req, res)
    })
admin.get('/view_/registered_students/view', async (req, res) => {
    adminController.getStudentInfo(req, res);
})

admin.route('/update_student_personal_info')
    .get(async (req, res) => {
        res.redirect('back')
    })
    .post(async (req, res) => {
        adminController.updateStudentPersonalInfo(req, res)
    })

admin.route('/update_academic_info')
    .get(async (req, res) => {
        res.redirect('back')
    })
    .post(async (req, res) => {
        adminController.updateAcademicInfo(req, res)
    })
admin.post('/update_student_passport', upload.single('student_image'), async (req, res) => {
    adminController.uoloadStudentPassport(req, res)
})

admin.get('/view_/registered_students/delete', async (req, res) => {
    adminController.deleteStudentInfo(req, res);
})




// academic item settings 
admin.route('/item/add_new/item')
    .get(async (req, res) => {
        paymentContr.addItems(req, res);
    })
    .post(async (req, res) => {
        paymentContr.postItems(req, res);
    })
admin.post('/setup/pyment_item_setup', async(req, res) => {
    paymentContr.feeItems(req, res)
})    
admin.route('/item/item_setup_session')
    .get(async (req, res) => {
        paymentContr.searchWithSession(req, res)
    });
admin.route('/item/item_setup')
    .get(async (req, res) => {
        paymentContr.academicitems(req, res)
    });


// payment 
admin.get('/setup', async (req, res) => {
    paymentContr.paymentSetup1(req, res)
})
// add 
admin.post('/setup/session_setup', async (req, res) => {
    paymentContr.addSession(req, res)
})
///setup updt
admin.route('/setup_edit')
    .get(async (req, res) => {
        paymentContr.getUpdateSesPage(req, res)
    })
    .post(async (req, res) => {
        paymentContr.UpdteSession(req, res)
    })
admin.get('/setup_delete', async(req, res) => {
    paymentContr.deleteSession(req, res)
})
admin.route('/payment/item_setup/payment_setup')
    .get(async (req, res) => {
        paymentContr.paymentSetup(req, res)
    })
    .post(async (req, res) => {
        paymentContr.addPaymentSetup(req, res);
    })
admin.route('/payment/settings')
    .get(async (req, res) => {
        paymentContr.paymentSettings(req, res);
    })
    .post(async (req, res) => {
        paymentContr.addpaysettings(req, res);
    });
admin.get('/admin/payment/settings/delete', async (req, res) => {
    paymentContr.deleteSetup(req, res);
})

admin.post('/setup/payment_fee_settings', async(req, res) => {
    paymentContr.paymentsetup(req, res)
})
admin.route('/payment/settings/update')
    .get(async (req, res) => {
        paymentContr.getUpdateP(req, res);
    })
    .post(async (req, res) => {
        paymentContr.updatePaysettings(req, res);
    })
// api payment table
admin.get('/view/all_payments', async (req, res) => {
    paymentContr.viewPayments(req, res);
})
admin.get('/view/individual_transactions', async (req, res) => {

})
admin.get('/payment/view_student_/payment/history', async (req, res) => {
    paymentContr.getPayment(req, res);
})
// search mandate using acd session 
admin.get('/search/generated/mandate', async (req, res) => {
    paymentContr.searchAccsession(req, res);
})
admin.get('/view/generated/mandate', async (req, res) => {
    paymentContr.generatedMandate(req, res);
})

admin.route('/general/payment/mandate')
    .get(async (req, res) => {
        paymentContr.getMandatePage(req, res);
    })
    .post(async (req, res) => {
        paymentContr.generateStudentFeesMandate(req, res);
    });
admin.get('/view/generated/mandate/approval', async (req, res) => {
    paymentContr.approveMandate(req, res)
});

admin.get('/view/generated/mandate/delete', async (req, res) => {
    paymentContr.deleteMandate(req, res);
})


// inventory expense
admin.route('/view_add/inventory/expense_head')
    .get(async (req, res) => {
        inventory.getExpensiveHead(req, res);
    })
    .post(async (req, res) => {
        inventory.addExpenseHead(req, res);
    });
admin.route('/view_add/inventory/expensive/expense_limit_level')
    .get(async (req, res) => {
        inventory.getExpense_limit_level(req, res)
    })
    .post(async (req, res) => {
        inventory.addExpense_limit_level(req, res);
    })
admin.route('/view_add/inventory/expensive/expense_limit_setup')
    .get(async (req, res) => {
        inventory.getExpense_limit_setup(req, res);
    })
    .post(async (req, res) => {
        inventory.addExpense_limit_setup(req, res);
    });
admin.route('/view/inventory/inventory_item_categories')
    .get(async (req, res) => {
        inventory.get_inventory_item_categories(req, res);
    })
    .post(async (req, res) => {
        inventory.add_inventory_item_categories(req, res)
    });
admin.route('/view/inventory/raw_material_inventory')
    .get(async (req, res) => {
        inventory.get_inventory_items(req, res)
    })
    .post(async (req, res) => {
        inventory.add_inventory_items(req, res);
    })
admin.route('/view/inventory/raw_material_orders')
    .get(async (req, res) => {
        inventory.get_raw_material_orders(req, res)
    })
    .post(async (req, res) => {
        inventory.add_raw_material_orders(req, res);
    })
admin.route('/view/inventory/raw_material_requests')
    .get(async (req, res) => {
        inventory.get_raw_material_orders(req, res)
    })
    .post(async (req, res) => {
        inventory.add_raw_material_orders(req, res);
    })

// time table route 
admin.get('/timetable_class_set', async (req, res) => {
    settingsController.getTimeTableClass(req, res);
})

admin.route('/create/school_general_time_table')
    .get(async (req, res) => {
        settingsController.getTimeTablePage(req, res)
    })
    .post(async (req, res) => {
        settingsController.addTimeTable(req, res)
    });

admin.get('/view_time_table', async (req, res) => {
    settingsController.viewTimeTable(req, res)
})

admin.route('/period_settings')
    .get(async (req, res) => {
        settingsController.viewPeriodDuration(req, res)
    })
    .post(async (req, res) => {
        settingsController.addPeriodDuration(req, res)
    });

// school notice board

admin.route('/school_notice_board')
    .get(async (req, res) => {
        settingsController.getSchoolNotice(req, res)
    })
    .post(async (req, res) => {
        settingsController.addSchoolNotice(req, res)
    });
// //


admin.get('/download_center', async (req, res) => {
    let classt = await classtable.findAll({ where: { schoolId: req.user.organization_id } })
    res.render('./admin/admission/download_cen', {
        classt
    })
})

admin.get('/download_assessment_format', async (req, res) => {
    downlformat.assessmentFormat(req, res)
})
admin.get('/download_puntuality_format', async (req, res) => {
    downlformat.punctualityFormat(req, res)
})
admin.get('/download_workhabit_format', async (req, res) => {
    downlformat.workhabit(req, res)
})
admin.get('/download_pychomoto_format', async (req, res) => {
    downlformat.pychomotor(req, res)
})
admin.get('/download_attention_format', async (req, res) => {
    downlformat.attendance(req, res)
})


admin.route('/create_and_view_classes')
    .get(async (req, res) => {
        classAndSubjectCont.GetCreateClassPage(req, res);
    })
    .post(async (req, res) => {
        classAndSubjectCont.CreateClass(req, res);
    })
admin.route('/classes_edit')
    .get(async (req, res) => {
        classAndSubjectCont.getEdit(req, res)
    })
    .post(async (req, res) => {
        classAndSubjectCont.updateClass(req, res)
    });
admin.get('/classes_delete', async (req, res) => {
    classAndSubjectCont.deleteClass(req, res)
})


module.exports = admin;

