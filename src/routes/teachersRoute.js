const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db');

const { studentregistration } = require('../models/studentModel/studentModel');
const { attendance } = require('../models/performance/performanceModel');
const { client_services } = require('../models/client_service_and_bufferModel');
const { classtable } = require('../models/classModel/classModel');

var teacherController = require('../controller/teachersController');
const performaceController = require('../controller/performanceController');
const { staffregistration } = require('../models/staffModel/staffmodel');
const { classsettings } = require('../models/basicTablesModel');
// const { client_services } = require('../models/client_service_and_bufferModel');

const teacher = express.Router();
teacher.route('*')
    .get(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        let teachesettings = await staffregistration.findOne({where: {personal_phone_no: req.user.user_phone}});
        let vclass = await classsettings.findOne({where: {staff_reg_id:teachesettings.staff_reg_id}})
        console.log(vclass.className)
       
        res.locals.settings =  vclass.className == ''? " ":vclass.className ;
        res.locals.clientCode = c.service_code;
        next();
    })
    .post(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    });


teacher.route('/dashboard')
    .get(async (req, res) => {
        teacherController.teachersDashboard(req, res);
    })



teacher.get('/student_records', async (req, res) => {
    teacherController.studentRecords(req, res)
})

teacher.route('/attendance_record')
    .get(async (req, res) => {
        teacherController.attendanceRecord(req, res);
    })

teacher.route('/countinious_assessment')
    .get(async (req, res) => {

    })

teacher.route('/view_work_habit')
    .get(async (req, res) => {
        teacherController.workHabit(req, res);
    })
    .post(async (req, res) => {
        teacherController.addWorkHabit(req, res)
    })


teacher.route('/attention_skils')
    .get(async (req, res) => {
        teacherController.attentionsSkils(req, res);
    })
    .post(async (req, res) => {
        teacherController.addAttentionsSkils(req, res)
    })


teacher.route('/pychomotor_skills')
    .get(async (req, res) => {
        teacherController.pychomotorSkills(req, res);
    })
    .post(async (req, res) => {
        teacherController.addPychomotorskills(req, res)
    })

teacher.route('/punctuality')
    .get(async (req, res) => {
        teacherController.punctuality(req, res);
    })
    .post(async (req, res) => {
        teacherController.addPunctuality(req, res)
    })

teacher.route('/social_development')
    .get(async (req, res) => {
        teacherController.socialdevelopment(req, res);
    })
    .post(async (req, res) => {
        teacherController.addSocialdevelopment(req, res)
    })

teacher.route('/continious_assesment')
    .get(async (req, res) => {
        teacherController.continiousAssesment(req, res);
    })


teacher.get('/get_class', async (req, res) => {
    teacherController.getstudentsbyclass(req, res);
})

teacher.route('/add/continious_assesment')
    .get(async (req, res) => {
        teacherController.addContiniousAssesmentPage(req, res);
    })
    .post(async (req, res) => {
        teacherController.addContiniousAssesment(req, res)
    })


teacher.get('/get_student_by_class/:classid', (req, res) => {
    teacherController.getStudents(req, res);
});





//  attendance 
const TODAY_START = new Date().setHours(0, 0, 0, 0);
const NOW = new Date();

teacher.get('/attendance', async (req, res) => {
    let classMember = await classtable.findAll({ where: { schoolId: req.user.organization_id } });
    res.render('./admin/attendance/get_class', {
        classMember
    })
})


teacher.get('/get_student_in_a_class', async (req, res) => {
    let teachesettings = await db.query(`SELECT classsettings.className as classname from classsettings INNER JOIN staffregistration ON classsettings.staff_reg_id = classsettings.staff_reg_id 
    where staffregistration.personal_phone_no = ${req.user.user_phone}`, { type: QueryTypes.SELECT });
    let classMember = await studentregistration.findAll({ where: { className: teachesettings[0].classname } });
    let uploads = [];
   
    db.query(`SELECT * from attendance where classid ='${teachesettings[0].classname}' AND  DAY(registered_date)=DAY(now()) AND schoolid = ${req.user.organization_id}`, { type: QueryTypes.SELECT }).then(attend => {
        if (attend.length < 1) {
            classMember.forEach(function (result) {
                var detail = {
                    schoolid: req.user.organization_id,
                    classid: teachesettings[0].classname,
                    studentid: result.student_reg_Id,
                };
                uploads.push(detail);
                // console.log(detail)
            })
            attendance.bulkCreate(uploads).then(function (result) {
                req.flash('success', `${teachesettings[0].classname} Attendance for  ${NOW.toDateString()} created!`);
                res.redirect(`/attendance/studentattendance?classname=${teachesettings[0].classname}`)
            })
        } else {
            req.flash('success', `${teachesettings[0].classname} Attendance for  Today ${NOW.toDateString()}  Already Generated!`);
            res.redirect(`/attendance/studentattendance?classname=${teachesettings[0].classname}`)
        }
    })
})


teacher.get('/studentattendance', async (req, res) => {
    let classname = req.query.classname;
    console.log(classname)
    // let classN = await classtable.findOne({ where: { Id: className} });
    let classMember = await studentregistration.findAll({ where: { schoolId: req.user.organization_id } });
    // let attend = await attendance.findAll({
    //     where: {
    //         classid: classname, registered_date: {
    //             [Op.gt]: TODAY_START,
    //             [Op.lt]: NOW
    //         }
    //     }
    // });
    attend = await db.query(`SELECT * from attendance where classname ='${classname}' AND  DAY(registered_date)=DAY(now()) AND schoolid = ${req.user.organization_id}`)
    res.render('./admin/attendance/attendance', {
        classMember, attend, moment
    })
});


teacher.get('/take_attendance', async (req, res) => {
    try {
        const data = {
            date: Date.now(),
            attendance: 1,
            takenby: req.user.username
        };

        let attendanceid = req.query.attendanceid;
        const checkinUser = await attendance.findOne({ where: { attendanceID: attendanceid } });

        console.log(checkinUser.date)
        if (checkinUser.date == null) {
            // user.attendance.push(data);
            // await user.save();
            checkinUser.update(data, { new: true })
            req.flash('success', 'You have been signed in for today');
            res.redirect('back')

        } else {
            req.flash("danger", "You have signed in today already");
            res.redirect("back");
        }


    } catch (error) {
        console.log("something went wrong");
        console.log(error);
    }
})




//skill managemtn
teacher.get('/skill_management_settings', async (req, res) => {
    res.render('./admin/performance/skills_center')
})
// performance
teacher.route('/view/performance/punctuality')
    .get(async (req, res) => {
        performaceController.getPunctuality(req, res);
    })

teacher.get('/view/performance/get_punctuality/:classid', async (req, res) => {
    performaceController.getClassPunctuality(req, res);
})

teacher.get('/view/students_attention_skills', async (req, res) => {
    performaceController.attentionPage(req, res);
});

teacher.get('/view/student_attention_skills_per-class/:classid', (req, res) => {
    performaceController.getAttention(req, res);
})

// work habit
teacher.get('/view/students_workhbit', async (req, res) => {
    performaceController.workhabitPage(req, res);
})
teacher.get('/view/students_workhbit/:classid', async (req, res) => {
    performaceController.getWorkhabit(req, res);
})
// pychomotorskills
teacher.get('/view/pychomotorskills', async (req, res) => {
    performaceController.pychomotorskillsPage(req, res);
})
teacher.get('/view/pychomotorskills/:classid', async (req, res) => {
    performaceController.getPychomotorskills(req, res);
})




module.exports = teacher;