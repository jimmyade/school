const express = require('express');
const multer = require('multer');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');
const { Sequelize, QueryTypes } = require('sequelize');
const Op = Sequelize.Op;
const db = require('../datab/db');
const { client_services } = require('../models/client_service_and_bufferModel');
const libraryController = require('../controller/libraryController');
const { lms_members } = require('../models/library/library');


const library = express.Router();

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    // if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    //     cb(null, true);
    // } else {
    //     cb(null, fulse)
    // }
    if (file.mimetype.includes("image/jpeg") || file.mimetype.includes("image/png") || file.mimetype.includes("excel") || file.mimetype.includes("spreadsheetml") || file.mimetype.includes("application/pdf")) {
        cb(null, true);
    } else {
        cb(null, false)
    }
}
var upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 2
    },
    fileFilter: fileFilter
});


library.route('*')
    .get(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    }).post(async (req, res, next) => {
        let c = await client_services.findOne({ attributes: ['service_code'], where: { service_id: req.user.service_id } });
        // res.locals.clientName =  c.client;
        res.locals.clientCode = c.service_code;
        next();
    });

library.route('/library_dashboard')
    .get(async (req, res) => {
        libraryController.dashbaord(req, res);
    })
    .get(async (req, res) => {

    })

// books registration
library.route('/book_registration')
    .get(async (req, res) => {
        libraryController.getAddBookPage(req, res)
    })
    .post(async (req, res) => {
        libraryController.addBook(req, res)
    })

library.route('/book_registration/edit')
    .get(async (req, res) => {
        libraryController.viewBookDetail(req, res)
    })
    .post(async (req, res) => {
        libraryController.updateMslBook(req, res)
    })
library.route('/mls_updte/library_book_ing')
    .get(async (req, res) => {
        res.redirect(`/api/labrary/book_registration/edit?book_code=${req.query.book_code}`)
    })
    .post(upload.single('photo'), async (req, res) => {
        libraryController.updatePhoto(req, res)
    })

// books issued out
library.route('/issued_books')
    .get(async (req, res) => {
        libraryController.getIssuedBook(req, res);
    })
    .post(async (req, res) => {

    })
// library member
library.route('/library_users')
    .get(async (req, res) => {
        libraryController.getLmMember(req, res);
    }).post(async (req, res) => {
        libraryController.LmMember(req, res);
    })

// search 
library.route('/search_for_books')
    .get(async (req, res) => {

    })
    .post(async (req, res) => {

    })
// upload books from excel document
library.route('/upload_book')
    .get(async (req, res) => {

    })
    .post(async (req, res) => {

    })

// 
library.route('/fine')
    .get(async (req, res) => {

    })
    .post(async (req, res) => {

    });

library.route('/supplier')
    .get(async (req, res) => {
        libraryController.getSupplierpage(req, res)
    })
    .post(async (req, res) => {
        libraryController.addNewSupplier(req, res);
    })


module.exports = library;
