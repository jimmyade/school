const express = require('express');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const multer = require('multer');
const passport = require('passport');
var bcrypt =  require('bcryptjs');

const saltRounds = 10;
const nodemailer = require("nodemailer");
require('dotenv').config()


const {client_services} = require('../../models/client_service_and_bufferModel');
const {_states, _residential_address_status, _countries, _lgas, _cities }= require('../../models/basicTablesModel');

const { Users } = require('../../models/usersMosel');

const clientRegistration = require('../../controller/registratonController');
const { createTokens, validateToken, logUserOut } = require('../../lib/JWT');
// const userid = validateToken.

const appUsers = express.Router();
// function for uploading picture via multer
   var storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb){
        cb(null, Date.now() + file.originalname)
    }
   })

  const fileFilter = (req, file, cb)=>{
    if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/png'){
        cb(null, true);
    } else {
        cb(null, fulse)
    }
}
var upload = multer ({
    storage: storage,
    limits:{
        fileSize: 1024 * 1024 * 2
    },
    fileFilter:fileFilter
});

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  //host: "smtp.mailtrap.io",
  //port: 2525,
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secure: true, // use SSL
  auth: {
    // user: "709b0038a45240",
    // pass: "dad9baf5795764"
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS
  }
});




function redirectIfLoggedIn(req, res, next) {
  if (req.user) return res.redirect('/users/pauls_dashboard');
  return next();
}

appUsers.post('/login1',  passport.authenticate('local', {
  failureFlash: true,
  failureRedirect: '/login'
}),(req, res, next)=>{
  res.redirect('/api/success')
});



appUsers.post('/login',  async(req, res) => {
  const {username , password} = req.body;
  const user = await Users.findOne({
    attributes:["id", "group_id", "name", "email", "user_phone", "service_id", "service_code", "organization_id", "service_logo", "password", "username"], 
    where:{username:username}})
    if(!user){
        req.flash('info', "Unknown User!")
        res.redirect('back')
        // res.send("Unknown User!")
    } 
    bcrypt.compare(password, user.password, function(err, isMatch){
        if (err) console.log(err);

        if(isMatch) {
          const accessToken = createTokens(user)
          res.cookie("access-token", accessToken, {
            maxAge: 1000 * 60 * 60 * 24 ,
            httpOnly: true
          })
           res.redirect("/api/success")
        } else{
          // res.send("Password Incorrect")
          req.flash('info', "Password Incorrect")
          res.redirect('back')
        }
    });
})

appUsers.get('/api/success', validateToken,  async(req, res)=>{

 clientRegistration.userLoginControl(req, res);
})
// module.exports = () => {
  // appUsers.post('/login',  passport.authenticate('local', {
  //   successRedirect: '/api/dashboard',
  //   failureRedirect: '/login?error=true'
  // }));
  appUsers.get('/login',  (req, res) =>{
    res.render('./registration/login');
  })

  appUsers.get('/logout', (req, res) => {
    logUserOut(req, res);
  });

  appUsers.get('/',(req, res)=>{
   
      res.render('index')
    
  })

  appUsers.get('/about_us', (req, res)=>{
    res.render('./about_us')
  });

  appUsers.get('/our_services', (req, res)=>{
    res.render('./our_service')
  });


  // get state 
appUsers.get('/api/getstate/:country_id',  (req, res)=>{
    let country_id = parseInt(req.params.country_id);
    _states.findAll({where:{country_id:country_id}}).then(function(result){
        res.send(result);
    })
});
//get lga 
appUsers.get('/api/getlga/:stateID',  (req, res)=>{
    let stateID = parseInt(req.params.stateID);
    _lgas.findAll({where:{state_id:stateID}}).then(function(result){
        res.send(result);
    })
});
// get city 
// appUsers.get('/api/getcity/:lgaID',  (req, res)=>{
//   let lgaID = parseInt(req.params.lgaID);
//   _cities.findAll({where:{lga_id:lgaID}}).then(function(result){
//       res.send(result);
//   })
// });

appUsers.route('/create_account')
.get(async(req, res) =>{
    clientRegistration.getClientRegPage(req, res)
})
.post(upload.single('service_logo'), async (req, res, next) => {
   clientRegistration.clientRegistration(req, res)
});

 // return appUsers;
// };

// appUsers.get('/api/getcity/:lgaID',  (req, res)=>{
//   let lgaID = parseInt(req.params.lgaID);
//   _cities.findAll({where:{lga_id:lgaID}}).then(function(result){
//       res.send(result);
//   })
// });


appUsers.post('/api/super/admin/rgistration', (req, res)=>{
  //const { firstname, surname, middlename, email, user_phone, password, password2 } = req.body;
  let errors = [];
  var firstname = req.body.firstname;
  var surname = req.body.surname;
  var middlename = req.body.middlename;
  var email = req.body.email;
  var username = req.body.email;
  var user_phone = req.body.user_phone;
  var password = req.body.password;
  // if(!firstname || !surname ||  !email || !user_phone || !password || !password2){
  //     errors.push({msg : 'Please enter all fields'})
  // }
  // if (password != password2) {
  //     errors.push({ msg: 'Password do not match' });
  //   }
  
    
  //   if (errors.length > 0) {
  //     res.send(errors)
  //     //console.log(errors)
  //   } else {
      Users.findOne({where:{ username: email }}).then(user => {
        if (user) {
         res.send("Email exist");
          //console.log(errors)
        } else {
          let newUser = new Users({
            group_id: 111, name: `${firstname} ${surname} ${middlename}`, firstname, surname, middlename, email, user_phone, password
          });
          //console.log(newUser)
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser.save().then(saveUser => {
                Users.findOne({where: {email:newUser.email}}).then(realuser=>{
                  var userid = realuser.userid
                  res.send("Super Admin created")
                }).catch(err => console.log(err));
              })
                
            });
          });
        }
      });
    //}
});



module.exports = appUsers
