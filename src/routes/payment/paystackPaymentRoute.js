const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const _ = require('lodash');
const path = require('path');
const paystacts = express.Router();
// const { Payment, Orders, Ordersproduct, Products } = require('../../models/productModel/productmodel');
const { Users } = require('../../models/usersMosel');
const { payment_details } = require('../../models/payment/paymentModel');
// const { Donation } = require('../../models/normalize_model');


const { initializePayment, verifyPayment } = require('../../lib/paystack')(request);


paystacts.post('/paystack/pay', (req, res) => {
    console.log(req.body)
    function invoice_n(len) {
        len = len || 100;
        var nuc = "0123456789";
        var i = 0;
        var n = 0;
        s = "";
        while (i <= len - 1) {
            n = Math.floor(Math.random() * 7);
            s += nuc[n];
            i++;
        }
        return s;
    }
    var invoice_number = invoice_n(7);

    const form = _.pick(req.body, ['amount', 'email', 'full_name']);
    form.metadata = {
        full_name: form.full_name
    }
    form.amount *= 100;
    initializePayment(form, (error, body) => {
        if (error) {
            //handle errors
            console.log(error);
            return;
        }
        response = JSON.parse(body);
        console.log(response);

        if (response.data.authorization_url) {
            var item = [req.body]
            var psymentDetails = [];
            for (var pid of item) {
                var class_id = pid.class_id,
                    class_name = pid.class_name,
                    items = pid.item,
                    iteamAmount = pid.iteamAmount,
                    acdsessions = pid.sessions,
                    options = pid.options,
                    term = pid.term,
                    studentid = pid.studentid,
                    itemid = pid.itemid;
                for (var i = 0; i < class_id.length; i++) {
                    var CI = class_id[i],
                        CN = class_name[i],
                        IN = items[i],
                        IA = iteamAmount[i],
                        AS = acdsessions[i],
                        PO = options[i],
                        AT = term[i],
                        SI = studentid[i],
                        II = itemid[i];

                    let paymentdata = {
                        class_id: CI,
                        studentid: SI,
                        taxpayer_name: req.user.name,
                        item:AT,
                        class_name: CN,
                        item: IN, 
                        paymentItemID: II,
                        invoice_number: invoice_number,
                        amount: IA,
                        sessions: AS,
                        options: PO,
                        entered_by: req.user.username,
                        payersName: req.body.full_name,
                        email: req.body.email,
                        phone: req.body.phone,
                        service_id: req.user.service_id,
                        schoolid: req.user.organization_id ,
                        reference:response.data.reference
                    };
                    psymentDetails.push(paymentdata);
                    
                }


            }
            payment_details.bulkCreate(psymentDetails)
            res.redirect(response.data.authorization_url)
        }

    });
});

paystacts.get('/paystack/callback', (req, res) => {
    const ref = req.query.reference;
    verifyPayment(ref, (error, body) => {
        if (error) {
            //handle errors appropriately
            console.log(error)
            return res.redirect('/error');
        }
        response = JSON.parse(body);

        const data = _.at(response.data, ['reference', 'amount', 'customer.email', 'metadata.full_name']);

        [reference, amount, email, full_name] = data;
        console.log("this is a return data " + data)
        newDonor = { references: reference, amount, email, full_name }
        // console.log(newDonor)

        Donation.findOne({ where: { refernce: ref } }).then(success => {
            success.update({ confirm_payment: 1 }, { new: true })

            req.flash('info', 'Your Payment Was Successful')
            res.render('./home/payment_success_payment_page', {
                success,
            });
        })

    })
});
paystacts.get('payment_notification', (req, res) => {

})
paystacts.get('/receipt/:id/:reference', (req, res) => {
    const id = req.params.id;
    const reference = req.params.reference;
    Ordersproduct.findAll({ where: { paymentRef: reference } }).then(result => {
        Payment.findOne({ where: { references: reference } }).then(details => {
            res.render('./home/payment/payment_invoice', {
                result: result,
                details: details
            })
        })
    })
})

paystacts.get('/error', (req, res) => {
    res.render('', {

    })
})


module.exports = paystacts;