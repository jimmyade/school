const Joi = require('joi');
// const { min } = require('moment');


const newStudentInfo = Joi.object({
    title: Joi.string().required(),
    studentId: Joi.string().required(),
    surname: Joi.string().required(),
    firstname: Joi.string().required(),
    middlename: Joi.string(),
    gender: Joi.string().valid('Male', 'Female'),
    dateofbirth: Joi.date().required(),
    className: Joi.string().required(),
    classCategory: Joi.string(),
    guardian_name: Joi.string().required(),
    guardian_address: Joi.string().required(),
    phone: Joi.string().regex(/^[0-9]{11}$/).required(),
    yearOfEntry: Joi.number().min(2012).max(4040),
    previous_class: Joi.string(),
    previous_school: Joi.string(),
    yearOfGraduation: Joi.number().min(2012).max(4040),
    photo: Joi.string(),
    guardian_email: Joi.string().email().lowercase().required(),
});


const updStudtInfo = Joi.object({
    surname: Joi.string().required(),
    firstname: Joi.string().required(),
    middlename: Joi.string(),
    gender: Joi.string().valid('Male', 'Female').required(),
    dateofbirth: Joi.date().required(),
    guardian_name: Joi.string().required(),
    guardian_address: Joi.string().required(),
    phone: Joi.string().regex(/^[0-9]{11}$/).required(),
    guardian_email: Joi.string().email().lowercase().required(),
    id: Joi.number().required()
});


const updStudentAcademicInfo = Joi.object({
    className: Joi.string().required(),
    yearOfEntry: Joi.number().min(2012).max(4040),
    previous_school: Joi.string().required(),
    yearOfGraduation: Joi.number().min(2012).max(4040),
    id: Joi.number().required()
});


const staffRegistration = Joi.object({
    title : Joi.string().required(),
    surname : Joi.string().required(),
    firstname : Joi.string().required(),
    middlename : Joi.string(),
    gender : Joi.string().valid('Male', 'Female').required(),
    dateofbirth : Joi.date().raw().required(),
    kin_name : Joi.string().required(),
    email: Joi.string().email().lowercase().required(),
    activity: Joi.string().required(),
    kin_address : Joi.string().required(),
    kin_phone : Joi.string().regex(/^[0-9]{11}$/).required(),
    date_Of_Entry : Joi.date().raw().required(),
    personal_phone_no : Joi.string().regex(/^[0-9]{11}$/).required(),
    home_address : Joi.string().required(),
    designation : Joi.string().required(),
    specialization : Joi.string().required(),
    highest_qualification : Joi.string().required(),
    qualification_year : Joi.number().min(2012).max(4040),
})


const infostaff = Joi.object({
    // title : Joi.string().required(),
    
    firstname : Joi.string().required(),
    middlename : Joi.string(),
    surname : Joi.string().required(),
    gender : Joi.string().valid('Male', 'Female').required(),
    dateofbirth : Joi.date().raw().required(),
    kin_name : Joi.string().required(),
    email_address: Joi.string().email().lowercase().required(),
    kin_address : Joi.string().required(),
    kin_phone : Joi.string().regex(/^[0-9]{11}$/).required(),
    date_Of_Entry : Joi.date().raw().required(),
    personal_phone_no : Joi.string().regex(/^[0-9]{11}$/).required(),
    home_address : Joi.string().required(),
    staff_id:  Joi.number()
})

const staffAcademicInfo = Joi.object({
    highest_qualification : Joi.string().required(),
    qualification_year : Joi.number().min(2012).max(4040),
    designation : Joi.string().required(),
    specialization : Joi.string().required(),
    staff_id:  Joi.number()
})

const termSetupV = Joi.object({
    term: Joi.string().valid('FIRST', 'SECOND', 'THIRD').required(),
    sesion: Joi.string().required(),
    term_ends:Joi.date().raw().required(),
    term_begins:Joi.date().raw().required(),
    term_name: Joi.string(),
    no_of_times_opened: Joi.number().required()
})

const addclassVal = Joi.object({
    classCategory: Joi.string().required(),
    className: Joi.array().unique().required()
})

module.exports = {
    updStudtInfo, updStudentAcademicInfo, newStudentInfo, staffRegistration, infostaff, staffAcademicInfo, termSetupV, addclassVal
}