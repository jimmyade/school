const Sequelize = require('sequelize');
const db = require('../../datab/db');


const parent = db.define('parents', {
    parent_id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    title: {
        type: Sequelize.STRING
    },
    firstname: {
        type: Sequelize.STRING
    },
    middlename: {
        type: Sequelize.STRING
    },
    surname: {
        type: Sequelize.STRING
    },
    dob: {
        type: Sequelize.DATEONLY
    },
    email: {
        type: Sequelize.STRING(50)
    },
    phone: {
        type: Sequelize.STRING(15)
    },
    address: {
        type: Sequelize.TEXT
    },
    schoolId: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    date_created: {
        type: Sequelize.DATE
    },
    login_id: {
        type: Sequelize.INTEGER(11)
    },
    photo: {
        type: Sequelize.STRING(11)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

module.exports = {
    parent, 
}