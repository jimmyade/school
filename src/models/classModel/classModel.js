const Sequelize = require('sequelize');
const db = require('../../datab/db');

const school_classes = db.define('school_classes', {
    schoolclass_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCategory: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCode: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    school_id: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    dateregistered: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    serviceid: {
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});


const classtable = db.define('classtable', {
    Id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCategory: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCode: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    dateregistered: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    serviceid: {
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const result = db.define('result', {
    id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    studentId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    surname: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    firstname: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    middlename: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subjects: {
        type: Sequelize.STRING(145),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    total_CA: {
        type: Sequelize.DECIMAL(13),
        decimal: 1
    },
    exam_Score: {
        type: Sequelize.DECIMAL(13),
        decimal: 1
    },
    total_score: {
        type: Sequelize.DECIMAL(13),
        decimal: 1
    },
    subject_grade: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCode: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    serviceid: {
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const scorerecord = db.define('resulttable', {
    result_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subjects: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE,
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    comments: {
        type: Sequelize.STRING(75)
    },
    assignment1: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    continiousassesment1: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    test1: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    assignment2: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    continiousassesment2: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    test2: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    registeredBy: {
        type: Sequelize.DECIMAL(12),
        decimal: 1
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    result_status: {
        type: Sequelize.STRING(75)
    },
    serviceid: {
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const class_groups = db.define('class_groups', {
    id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    created_at: {
        type: Sequelize.DATE
    },
    deleted_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    arm: {
        type: Sequelize.STRING
    },
    class_level: {
        type: Sequelize.INTEGER(11)
    },
    school_id: {
        type: Sequelize.BIGINT(20)
    }
}, {
    timestamps: false,
    freezeTableName: true,
})

module.exports = {
    classtable, result, scorerecord, class_groups, school_classes
}