const Sequelize = require('sequelize');
const db = require('../../datab/db');

const calender_type = db.define('calender_type', {
    id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    CalenderTypeName: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    duration: {
        type: Sequelize.INTEGER(45),
        allowNull: true
    },
    schoolid: {
        type: Sequelize.STRING(45),
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const calender_type_periods = db.define('calender_type_periods', {
    id_term: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    period_name: {
        type: Sequelize.STRING(75)
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    calendertype_id: {
        type: Sequelize.STRING
    },
    session_id: {
        type: Sequelize.STRING(45)
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const calender_year_period_setting = db.define('calender_year_period_setting', {
    id_schoolterm: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    period_id: {
        type: Sequelize.BIGINT(20),
    },
    period_end_date: {
        type: Sequelize.DATE
    },
    period_start_date: {
        type: Sequelize.DATE
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    session_id: {
        type: Sequelize.STRING(45)
    },
    calender_year_id: {
        type: Sequelize.BIGINT(20),
    },
    schoolcategory_id: {
        type: Sequelize.BIGINT(20),
    },
    no_of_days_opened: {
        type: Sequelize.BIGINT(20),
    },
    next_term_begins: {
        type: Sequelize.DATE
    },
    no_of_weeks_opened: {
        type: Sequelize.BIGINT(20),
    },
    last_modified_by: {
        type: Sequelize.STRING(45)
    },
    next_term_begins: {
        type: Sequelize.DATE
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});


const school_calender_type = db.define('school_calender_type', {
    school_id: {
        type: Sequelize.INTEGER(11)
    },
    id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    calender_type_id: {
        type: Sequelize.STRING
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const calender_year_setings = db.define("calender_year_setting", {
    id_calender_year: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    calender_year_name: {
        type: Sequelize.STRING
    },
    start_date: {
        type: Sequelize.DATE
    },
    end_date: {
        type: Sequelize.DATE
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    last_modified_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});


const gradebook_columns = db.define('gradebook_columns', {
    idgradebook_columns: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    gradebook_columns_title: {
        type: Sequelize.STRING(50)
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    last_modified_by: {
        type: Sequelize.STRING(45)
    },
    last_modified_on: {
        type: Sequelize.DATE
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const gradebook_setting = db.define('gradebook_setting', {
    idgrade: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    grade_id: {
        type: Sequelize.INTEGER(11),
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    session_id: {
        type: Sequelize.STRING(100)
    },
    schoolcategory_id: {
        type: Sequelize.INTEGER(11),
    },
    calender_year_period_id: {
        type: Sequelize.INTEGER(11),
    },
    session_id: {
        type: Sequelize.STRING(100)
    },
    grade_title: {
        type: Sequelize.STRING(50)
    },
    grade: {
        type: Sequelize.CHAR(5)
    },
    grade_min:{
        type: Sequelize.DOUBLE(10, 2)
    },
    grade_max:{
        type: Sequelize.DOUBLE(10, 2)
    },
    gradepoint: {
        type: Sequelize.INTEGER(11),
    },
    grade_color: {
        type: Sequelize.STRING(50)
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    
}, {
    timestamps: false,
    freezeTableName: true,
});

const gradebook_template = db.define('gradebook_template', {
    idscoresetup: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    school_year_period_id: {
        type: Sequelize.INTEGER(11),
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    schoolcategory_id: {
        type: Sequelize.INTEGER(11),
    },
    schoolclass_id: {
        type: Sequelize.INTEGER(11),
    },
    score:{
        type: Sequelize.DOUBLE(20, 2),
    },
    gradebook_columnname_id: {
        type: Sequelize.INTEGER(11),
    },
    locked: {
        type: Sequelize.TINYINT(1)
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    last_modified_by: {
        type: Sequelize.STRING(45)
    },
    last_modified_on: {
        type: Sequelize.DATE
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
    template_order_id: {
        type: Sequelize.INTEGER(11),
    },
    templatename: {
        type: Sequelize.STRING(75)
    },
}, {
    timestamps: false,
    freezeTableName: true,
})

module.exports = {
    calender_type, calender_type_periods, calender_year_period_setting, school_calender_type, calender_year_setings,
    gradebook_columns, gradebook_setting, gradebook_template
}