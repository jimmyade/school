const Sequelize = require('sequelize');
const db = require('../../datab/db');

const staffregistration = db.define('staffregistration', {
    staff_reg_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    staff_id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    title: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    surname: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    firstname: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    middlename: {
        type: Sequelize.STRING(45)
    },
    gender: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    dateofbirth: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    activity: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    kin_name: {
        type: Sequelize.STRING(145),
        allowNull: true
    },
    kin_address: {
        type: Sequelize.STRING(145),
        allowNull: true
    },
    kin_phone: {
        type: Sequelize.STRING(45)
    },
    dateRegistered: {
        type: Sequelize.DATE,
        allowNull: true
    },
    registered_by: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    date_Of_Entry: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    personal_phone_no: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    home_address: {
        type: Sequelize.STRING(145),
        allowNull: true
    },
    date_Of_Leaving: {
        type: Sequelize.STRING(45)
    },
    designation: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    photo: {
        type: Sequelize.STRING(100)
    },
    username: {
        type: Sequelize.STRING(75)
    },
    specialization: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    highest_qualification: {
        type: Sequelize.STRING(75)
    },
    qualification_year: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    serviceid:{
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
})


module.exports = {
    staffregistration
}