const Sequelize = require('sequelize');
const db = require('../../datab/db');

const resulttable = db.define('resulttable', {
    result_id:{
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull:true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull:true
    },
    subjects: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    className: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    dateRegistered:{
        type: Sequelize.DATE
    },
    term: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    sesion: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    comments: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    assignment1: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    continiousassesment1: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    test1: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    assignment2: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    continiousassesment2: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    test2: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    exam: {
        type: Sequelize.DECIMAL(12),
        decimal:1
    },
    registeredBy: {
        type: Sequelize.STRING(75),
        allowNull:true
    },
    result_status: {
        type: Sequelize.STRING(75),
        allowNull:true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

module.exports={
    resulttable
}