const Sequelize = require('sequelize');
const db = require('../../datab/db');

const continiousassesment = db.define('continiousassesment', {
    id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    studentId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    surname: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    firstname: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    middlename: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subjects: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classCode: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    assessment_type: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    score: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const assessment = db.define('assessment', {
    idassessment: {
        type: Sequelize.BIGINT(20),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    assessmentid: {
        type: Sequelize.BIGINT(20)
    },
    staff_id: {
        type: Sequelize.INTEGER(11)
    },
    schoolclass_id: {
        type: Sequelize.INTEGER(11)
    },
    student_id: {
        type: Sequelize.STRING(50)
    },
    // studen_reg_no: {
    //     type: Sequelize.STRING(50)
    // },
    subject_id: {
        type: Sequelize.INTEGER(11)
    },
    school_id: {
        type: Sequelize.STRING(45)
    },
    session_id: {
        type: Sequelize.STRING(45)
    },
    school_session_id: {
        type: Sequelize.INTEGER(11)
    },
    term_id: {
        type: Sequelize.INTEGER(11)
    },
    schoolcategory_id: {
        type: Sequelize.INTEGER(11)
    },
    A1: {
        type: Sequelize.DOUBLE(11, 2)
    },
    A2: {
        type: Sequelize.DOUBLE(11, 2)
    },
    A3: {
        type: Sequelize.DOUBLE(11, 2)
    },
    A4: {
        type: Sequelize.DOUBLE(11, 2)
    },
    A5: {
        type: Sequelize.DOUBLE(11, 2)
    },
    A6: {
        type: Sequelize.DOUBLE(11, 2)
    },
    participated: {
        type: Sequelize.TINYINT(1)
    },
    recordable: {
        type: Sequelize.STRING(45)
    },
    obtainable_score: {
        type: Sequelize.DOUBLE(11, 2)
    },
    result_id: {
        type: Sequelize.BIGINT(20)
    },
    subject_score: {
        type: Sequelize.DOUBLE(11, 2)
    },
    subject_average: {
        type: Sequelize.DOUBLE(12, 2)
    },
    subject_grade: {
        type: Sequelize.STRING(100)
    },
    grade_title: {
        type: Sequelize.STRING(255)
    },
    subject_position: {
        type: Sequelize.STRING(25)
    },
    gp: {
        type: Sequelize.INTEGER(25)
    },
    out_of: {
        type: Sequelize.DECIMAL(10)
    },
    subject_remark: {
        type: Sequelize.STRING
    },
    min_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    max_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    firstterm_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    secondterm_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    sum_secondterm: {
        type: Sequelize.DOUBLE(12, 2)
    },
    avg_secondterm: {
        type: Sequelize.DOUBLE(12, 2)
    },
    thirdterm_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    sum_thirdterm: {
        type: Sequelize.DOUBLE(12, 2)
    },
    fourthterm_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    fourthterm_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    overall_score: {
        type: Sequelize.DOUBLE(12, 2)
    },
    average_overall: {
        type: Sequelize.DOUBLE(12, 2)
    },
    overall_subject_grade: {
        type: Sequelize.STRING(100)
    },
    overall_grade_title: {
        type: Sequelize.STRING
    },
    term_subject_position: {
        type: Sequelize.STRING(25)
    },
    registered_by: {
        type: Sequelize.STRING(25)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    last_modified_by: {
        type: Sequelize.STRING(45)
    },
    registered_on: {
        type: Sequelize.DATE
    },
    lock_result: {
        type: Sequelize.DATE
    },
    locked: {
        type: Sequelize.TINYINT(1)
    },
    firstterm_abscent: {
        type: Sequelize.TINYINT(1)
    },
    mark_delete: {
        type: Sequelize.TINYINT(1)
    },
    secondterm_abscent: {
        type: Sequelize.TINYINT(1)
    },
    subject_point: {
        type: Sequelize.INTEGER(11)
    },
    credit_point: {
        type: Sequelize.INTEGER(11)
    },
    okk: {
        type: Sequelize.INTEGER(11)
    },
    class_id: {
        type: Sequelize.INTEGER(11)
    },
    tri_used: {
        type: Sequelize.STRING
    },
}, {
    timestamps: false,
    freezeTableName: true,
})
module.exports ={
    continiousassesment, assessment
}