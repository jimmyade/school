const Sequelize = require('sequelize');
const db = require('../../datab/db');

const studentregistration = db.define('studentregistration', {
    student_reg_Id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    studentId: {
        type: Sequelize.STRING(45)
    },
    schoolId: {
        type: Sequelize.STRING(45)
    },
    title: {
        type: Sequelize.STRING(15)
    },
    surname: {
        type: Sequelize.STRING(45)
    },
    firstname: {
        type: Sequelize.STRING(45)
    },
    middlename: {
        type: Sequelize.STRING(45)
    },
    gender: {
        type: Sequelize.STRING(45)
    },
    dateofbirth: {
        type: Sequelize.DATEONLY
    },
    className: {
        type: Sequelize.STRING(45)
    },
    class_id: {
        type: Sequelize.INTEGER(11)
    },
    classCategory: {
        type: Sequelize.STRING(45)
    },
    activity: {
        type: Sequelize.STRING(45)
    },
    guardian_name: {
        type: Sequelize.STRING(145)
    },
    guardian_address: {
        type: Sequelize.STRING(245)
    },
    phone: {
        type: Sequelize.STRING(45)
    },
    alternative_phone: {
        type: Sequelize.STRING(45)
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    registered_by: {
        type: Sequelize.STRING(45)
    },
    yearOfEntry: {
        type: Sequelize.STRING(45)
    },
    previous_class: {
        type: Sequelize.STRING(45)
    },
    previous_school: {
        type: Sequelize.STRING(145)
    },
    yearOfGraduation: {
        type: Sequelize.STRING(45)
    },
    reason_for_leaving: {
        type: Sequelize.STRING(255)
    },
    photo: {
        type: Sequelize.STRING
    },
    username: {
        type: Sequelize.STRING(45)
    },
    active_status: {
        type: Sequelize.STRING(45)
    },
    serviceid:{
        type: Sequelize.STRING(50),
        allowNull: true
    },
    guardian_email: {
        type: Sequelize.STRING(100)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const studentmandate = db.define('studentmandate', {
    student_mandate_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    invoice_id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    accounts_head_id: {
        type: Sequelize.INTEGER(10),
        allowNull: true
    },
    accounts_head_name: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    sub_account: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    account_description: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    amount_payable: {
        type: Sequelize.DECIMAL(50),
        demimal: 2,
        allowNull: true
    },
    dateRaised: {
        type: Sequelize.DATE,
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    payment_status: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    date_of_payment: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    serviceid:{
        type: Sequelize.STRING(50),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

module.exports = {
    studentregistration, 
    studentmandate
}