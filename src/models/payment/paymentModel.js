const Sequelize = require('sequelize');
const db = require('../../datab/db');

const Api_Payments = db.define('api_payments',{
    idapi_payments:{
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true
    },
    service_id:{
        type: Sequelize.STRING(45)
    },
    PaymentLogId:{
        type: Sequelize.STRING(200)
    },
    CustReference:{
        type: Sequelize.STRING(200)
    },
    AlternateCustReference:{
        type: Sequelize.STRING(200)
    },
    Amount:{
        type: Sequelize.DECIMAL(50),
        decimals: 2
    },
    PaymentMethod:{
        type: Sequelize.STRING(45)
    },
    PaymentReference:{
        type: Sequelize.STRING(100)
    },
    TerminalId:{
        type: Sequelize.STRING(45)
    },
    ChannelName:{
        type: Sequelize.STRING(45)
    },
    Location:{
        type: Sequelize.STRING(45)
    },
    PaymentDate:{
        type: Sequelize.DATE,
        allowNull : true
    },
    InstitutionId:{
        type: Sequelize.STRING(200) 
    },
    InstitutionName:{
        type: Sequelize.STRING(200) 
    },
    BranchName:{
        type: Sequelize.STRING(50) 
    },
    BankName:{
        type: Sequelize.STRING(50) 
    },
    CustomerName:{
        type: Sequelize.STRING(200) 
    },
    OtherCustomerInfo:{
        type: Sequelize.STRING(200) 
    },
    ReceiptNo:{
        type: Sequelize.STRING(45) 
    },
    CollectionsAccount:{
        type: Sequelize.STRING(45) 
    },
    BankCode:{
        type: Sequelize.STRING(45) 
    },
    CustomerAddress:{
        type: Sequelize.STRING(200) 
    },
    CustomerPhoneNumber:{
        type: Sequelize.STRING(45) 
    },
    DepositorName:{
        type: Sequelize.STRING(200) 
    },
    DepositSlipNumber:{
        type: Sequelize.STRING(45) 
    },
    PaymentCurrency:{
        type: Sequelize.STRING(45) 
    },
    IsReversal:{
        type: Sequelize.STRING(45) 
    },
    ItemName:{
        type: Sequelize.STRING(200) 
    },
    ItemCode:{
        type: Sequelize.STRING(45) 
    },
    ItemAmount:{
        type: Sequelize.DECIMAL(50),
        decimals: 2
    },
    locked:{
        type: Sequelize.TINYINT(1)
    },
    revenue_id:{
        type: Sequelize.INTEGER(11)
    },
    invoice_no:{
        type: Sequelize.STRING
    },
    ServiceUrl:{
        type: Sequelize.STRING
    },
    ServiceUsername:{
        type: Sequelize.STRING
    },
    ServicePassword:{
        type: Sequelize.STRING
    },
    FtpUrl:{
        type: Sequelize.STRING
    },
    FtpUsername:{
        type: Sequelize.STRING
    },
    FtpPassword:{
        type: Sequelize.STRING
    },
    IsRepeated:{
        type: Sequelize.STRING
    },
    ProductGroupCode:{
        type: Sequelize.STRING
    },
    SettlementDate:{
        type: Sequelize.STRING
    },
    BranchNam:{
        type: Sequelize.STRING
    },
    ThirdPartyCode:{
        type: Sequelize.STRING
    },
    LeadBankCode:{
        type: Sequelize.STRING
    },
    LeadBankCbnCode:{
        type: Sequelize.STRING
    },
    CategoryCode:{
        type: Sequelize.STRING
    },
    ItemQuantity:{
        type: Sequelize.STRING
    },
    OriginalPaymentLogId:{
        type: Sequelize.STRING
    },
    OriginalPaymentReference:{
        type: Sequelize.STRING
    },
    Teller:{
        type: Sequelize.STRING
    },
    synch_status:{
        type: Sequelize.TINYINT(1),
        allowNull: true
    },
    found_taxpayer:{
        type: Sequelize.STRING
    },
    found_invoice:{
        type: Sequelize.STRING
    },
    settled:{
        type: Sequelize.STRING
    },
    logged_date:{
        type: Sequelize.DATE
    },
    service_charge:{
        type: Sequelize.DECIMAL(50),
        decimals: 2,
        allowNull: true
    },
    amount_net:{
        type: Sequelize.DECIMAL(50),
        decimals: 2,
        allowNull: true
    },
    sources:{
        type: Sequelize.STRING
    },
    taxpayer_rin:{
        type: Sequelize.STRING
    },
    status:{
        type: Sequelize.TINYINT(1),
        allowNull: true
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const payment_settings = db.define('payment_settings', {
    paymentsetup_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true 
    },
    item:{
        type: Sequelize.STRING
    },
    amount:{
        type: Sequelize.DECIMAL(50, 2)
    },
    sesion_id:{
        type: Sequelize.STRING
    },
    options:{
        type: Sequelize.TINYINT(2)
    },
    termid:{
        type: Sequelize.INTEGER(11)
    },
    schoolid:{
        type: Sequelize.STRING(50)
    },
    date_registered:{
        type: Sequelize.DATE
    },
    class_id:{
        type: Sequelize.INTEGER(11)
    },
    classname:{
        type: Sequelize.STRING(50)
    }
}, {
    timestamps: false,
    freezeTableName: true,
});


const items = db.define('items', {
    itemsID: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true 
    },
    items:{
        type: Sequelize.STRING
    },
    acdsession:{
        type: Sequelize.STRING
    },
    schoolid:{
        type: Sequelize.STRING(50)
    },
    options: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true,
})


const payment_mandate = db.define('payment_mandate', {
    mandate_invoice_id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true 
    },
    paymentsetup_id:{
        type: Sequelize.INTEGER(11)
    },
    studentid: {
        type: Sequelize.INTEGER(11)
    },
    class_id:{
        type: Sequelize.STRING(45)
    },
    class_name:{
        type: Sequelize.STRING(45)
    },
    item:{
        type: Sequelize.TEXT
    },
    discription:{
        type: Sequelize.STRING(45)
    },
    amount:{
        type: Sequelize.DECIMAL(50, 2)
    },
    amount_paid:{
        type: Sequelize.DECIMAL(50, 2)
    },
    date_log:{
        type: Sequelize.DATE
    },
    sessions:{
        type: Sequelize.STRING(45)
    },
    options:{
        type: Sequelize.STRING(45)
    },
    term:{
        type: Sequelize.STRING(45)
    },
    entered_by:{
        type: Sequelize.STRING(100)
    },
    invoice_number:{
        type: Sequelize.STRING(100)
    },
    paid:{
        type: Sequelize.TINYINT(1)
    },
    service_id:{
        type: Sequelize.STRING(45)
    },
    registered_by:{
        type: Sequelize.STRING(55)
    },
    registered_on:{
        type: Sequelize.DATE
    },
    source:{
        type: Sequelize.STRING
    },
    schoolid:{
        type: Sequelize.STRING
    },
    session_id:{
        type: Sequelize.STRING(45)
    },
    amount_remaining:{
        type: Sequelize.DECIMAL(50, 2)
    },
    authorized:{
        type: Sequelize.TINYINT(1)
    },
    authorized_by:{
        type: Sequelize.STRING(45)
    },
    rebet:{
        type: Sequelize.TINYINT(1)
    },
    prev_amount:{
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true,
})


const payment_details = db.define('payment_details', {
    mandate_invoice_id: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull : true 
    },
    paymentsetup_id:{
        type: Sequelize.INTEGER(11)
    },
    class_id:{
        type: Sequelize.STRING(45)
    },
    studentid: {
        type: Sequelize.INTEGER(11)
    },
    taxpayer_name:{
        type: Sequelize.STRING(45)
    },
    paymentItemID: {
        type: Sequelize.INTEGER(11)
    },
    class_name:{
        type: Sequelize.STRING(45)
    },
    item:{
        type: Sequelize.TEXT
    },
    discription:{
        type: Sequelize.STRING(45)
    },
    amount:{
        type: Sequelize.DECIMAL(50, 2)
    },
    amount_paid:{
        type: Sequelize.DECIMAL(50, 2)
    },
    date_log:{
        type: Sequelize.DATE
    },
    sessions:{
        type: Sequelize.STRING(45)
    },
    options:{
        type: Sequelize.STRING(45)
    },
    term:{
        type: Sequelize.STRING(45)
    },
    entered_by:{
        type: Sequelize.STRING(100)
    },
    invoice_number:{
        type: Sequelize.STRING(100)
    },
    paid:{
        type: Sequelize.TINYINT(1)
    },
    invoice_number:{
        type: Sequelize.STRING(100)
    },
    payersName:{
        type: Sequelize.STRING(200)
    },
    email:{
        type: Sequelize.STRING(100)
    },
    phone:{
        type: Sequelize.STRING(100)
    },
    registered_by:{
        type: Sequelize.STRING(55)
    },
    registered_on:{
        type: Sequelize.DATE
    },
    source:{
        type: Sequelize.STRING
    },
    schoolid:{
        type: Sequelize.STRING
    },
    session_id:{
        type: Sequelize.STRING(45)
    },
    amount_remaining:{
        type: Sequelize.DECIMAL(50, 2)
    },
    authorized:{
        type: Sequelize.TINYINT(1)
    },
    authorized_by:{
        type: Sequelize.STRING(45)
    },
    rebet:{
        type: Sequelize.TINYINT(1)
    },
    prev_amount:{
        type: Sequelize.STRING(45)
    },
    reference:{
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true,
})

module.exports = {
    Api_Payments, payment_settings, items, payment_mandate, payment_details
}