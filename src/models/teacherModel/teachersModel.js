const Sequelize = require('sequelize');
const db = require('../../datab/db');

const teachersetting = db.define('teachersetting', {
    staff_setting_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    staff_reg_id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subjects: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subject_status: {
        type: Sequelize.STRING(45),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true
})

module.exports = {
    teachersetting
}