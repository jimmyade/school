const Sequelize = require('sequelize');
const db = require('../datab/db');
const {} = require('../models/basicTablesModel')


const _lgas = db.define('_lga', {
    lga_id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    lga: {
        type: Sequelize.STRING(50),
    },
    lga_code: {
        type: Sequelize.STRING
    },
    state_id: {
        type: Sequelize.BIGINT(20),
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const _countries = db.define('_countries', {
    country_id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    country: {
        type: Sequelize.STRING(45),
    },
    country_code: {
        type: Sequelize.STRING(45)
    },
} , {
        timestamps: false,
        freezeTableName: true
    });

const _states = db.define('_states', {
    state_id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    state: {
        type: Sequelize.STRING(45),
    },
    state_code: {
        type: Sequelize.STRING(45)
    },
    country_id: {
        type: Sequelize.INTEGER(20),
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const titletable = db.define('titletable', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    titleName: {
        type: Sequelize.STRING(45),
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    registeredBy: {
        type: Sequelize.STRING(95)
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const termsetup = db.define('termsetup', {
    setup_id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
    },
    sesion: {
        type: Sequelize.STRING(45),
    },
    term_ends: {
        type: Sequelize.DATEONLY,
    },
    term_begins: {
        type: Sequelize.DATEONLY,
    },
    term_name: {
        type: Sequelize.STRING(45),
    },
    no_of_times_opened: {
        type: Sequelize.STRING(45),
    },
    date_registered: {
        type: Sequelize.DATE
    },
    schoolId: {
        type: Sequelize.STRING(50),
    },
    session: {
        type: Sequelize.STRING(50),
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const subjecttable = db.define('subjecttable', {
    Id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    subjectName: {
        type: Sequelize.STRING(145),
    },
    schoolId: {
        type: Sequelize.STRING(45),
    },
    dateRegistered: {
        type: Sequelize.STRING(45),
    },
    registeredBy: {
        type: Sequelize.STRING(145),
    },
}, {
    timestamps: false,
    freezeTableName: true
});

const sessionsetting = db.define('sessionsetting', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(145),
    },
    subjects: {
        type: Sequelize.STRING(145),
    },
    term:{
        type: Sequelize.STRING(15)
    },
    sesion:{
        type: Sequelize.STRING(45)
    },
    dateRegistered: {
        type: Sequelize.DATE,
    },
    schoolId: {
        type: Sequelize.STRING(45),
    },
    registeredBy: {
        type: Sequelize.STRING(145),
    },
    classCode:{
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true
});

const shlsession = db.define('shlsessions', {
    id: {
        type: Sequelize.BIGINT(20),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    acdsession:{
        type: Sequelize.STRING(45)
    },
    schoolId: {
        type: Sequelize.STRING(45),
    },
    registeredOn: {
        type: Sequelize.DATE,
    },
    registeredBy: {
        type: Sequelize.STRING(45),
    }
}, {
    timestamps: false,
    freezeTableName: true
}) 

const user_groups = db.define('user_groups', {
    idgroups: {
        type: Sequelize.INTEGER(10),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    group_id:{
        type: Sequelize.INTEGER(10)
    },
    group_name: {
        type: Sequelize.STRING(45),
    },
    
}, {
    timestamps: false,
    freezeTableName: true
});

const subject_groups = db.define('subject_groups', {
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    created_at:{
        type: Sequelize.DATE
    },
    deleted_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    },
    class_id: {
        type: Sequelize.INTEGER(11),
    },
    class_group_id: {
        type: Sequelize.STRING,
    },
    subject_id: {
        type: Sequelize.BIGINT(20),
    },
    teacher_id: {
        type: Sequelize.BIGINT(20),
    },
    school_id: {
        type: Sequelize.STRING(45),
    },
    
}, {
    timestamps: false,
    freezeTableName: true
});

const schoolcategory = db.define('schoolcategory', {
    schoolcategoryid:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    schoolCategory: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const _designations = db.define('_designations', {
    designation_id:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    designation: {
        type: Sequelize.STRING
    },
    code: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const classsettings = db.define('classsettings', {
    class_settings_id:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    staff_reg_id: {
        type: Sequelize.INTEGER(11)
    },
    className: {
        type: Sequelize.STRING
    },
    session: {
        type: Sequelize.STRING
    },
    registeredby: {
        type: Sequelize.STRING
    },
    date: {
        type: Sequelize.DATE
    },
    // updated_at: {
    //     type: Sequelize.DATE
    // }
}, {
    timestamps: false,
    freezeTableName: true
});

const timetable = db.define('TimeTable', {
    TimeTableId:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    PeriodTimeStart: {
        type: Sequelize.STRING(11)
    },
    PeriodTimeEnd: {
        type: Sequelize.STRING
    },
    PeriodDate: {
        type: Sequelize.DATEONLY
    },
    periodid: {
        type: Sequelize.INTEGER(11)
    },
    StaffId: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    AltStaffId: {
        type: Sequelize.INTEGER(11)
    },
    WeekDay: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    PeriodNum: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    ClassId: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    week: {
        type: Sequelize.INTEGER(11)
    },
    subjectId: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING,
        allowNull: true
    },
    acdsesion:{
        type: Sequelize.STRING(50),
        allowNull: true
    }
    
}, {
    timestamps: false,
    freezeTableName: true
});


const periodsettings = db.define('periodSettings', {
    id:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    periods: {
        type: Sequelize.STRING
    },
    periods: {
        type: Sequelize.INTEGER(11)
    },
    duraton: {
        type: Sequelize.STRING
    },
    acdsessions: {
        type: Sequelize.STRING
    },
    schoolId: {
        type: Sequelize.STRING(50),
        allowNull: true
    },
    
}, {
    timestamps: false,
    freezeTableName: true
});


const noticeboard = db.define('noticeboard', {
    noticeid:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(50)
    },
    notice: {
        type: Sequelize.STRING
    },
    date: {
        type: Sequelize.DATE
    },
    schoolId: {
        type: Sequelize.STRING
    },
    viewby: {
        type: Sequelize.STRING
    },
    dateregistered: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const paystack_credentials = db.define('paystack_credentials', {
    paystackId:{
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    live_key: {
        type: Sequelize.BLOB
    },
    test_key: {
        type: Sequelize.BLOB
    },
    date: {
        type: Sequelize.DATE
    },
    schoolId: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
});
module.exports = {
    _lgas, _countries, _states, titletable, termsetup, subjecttable, sessionsetting, shlsession, user_groups, subject_groups, schoolcategory,
    _designations, classsettings, timetable, periodsettings, noticeboard, paystack_credentials
}





