const Sequelize = require('sequelize');
const db = require('../datab/db');

const lesson_comments  = db.define('lesson_comments', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        // autoIncrement: true,
        allowNull: true
    },
    created_at: {
        type: Sequelize.DATE
    },
    deleted_at: {
        type: Sequelize.DATE
    },
    message: {
        type: Sequelize.STRING
    },
    lesson_id: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
})

const lesson_contents = db.define('lesson_contents', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        // autoIncrement: true,
        allowNull: true
    },
    created_at: {
        type: Sequelize.DATE
    },
    deleted_at: {
        type: Sequelize.DATE
    },
    deleted_at: {
        type: Sequelize.DATE
    },
    message: {
        type: Sequelize.STRING
    },
    lesson_id: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
})