const Sequelize = require('sequelize');
const db = require('../../datab/db');

const pychomotorskills = db.define('pychomotorskills', {
    pychomotorskills_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    drawing_and_painting: {
        type: Sequelize.STRING(45)
    },
    games: {
        type: Sequelize.STRING(45)
    },
    flexibility: {
        type: Sequelize.STRING(45)
    },
    verbal_fluency: {
        type: Sequelize.STRING(45)
    },
    overall_progress: {
        type: Sequelize.STRING(45)
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    pychomotorskills_status: {
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});


const socialdevelopment = db.define('socialdevelopment', {
    socialdevelopment_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    is_courteous: {
        type: Sequelize.STRING(45)
    },
    shows_respect: {
        type: Sequelize.STRING(45)
    },
    self_control: {
        type: Sequelize.STRING(45)
    },
    respond_to_correction: {
        type: Sequelize.STRING(45)
    },
    relates_to_adults: {
        type: Sequelize.STRING(45)
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    socialdevelopment_status: {
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});



const workhabit = db.define('workhabit', {
    workhabit_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(75),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    follows_instructions: {
        type: Sequelize.STRING(45)
    },
    works_independently: {
        type: Sequelize.STRING(45)
    },
    does_not_disturb: {
        type: Sequelize.STRING(45)
    },
    care_for_materials: {
        type: Sequelize.STRING(45)
    },
    completes_task: {
        type: Sequelize.STRING(45)
    },
    registeredBy: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    workhabit_status: {
        type: Sequelize.STRING(45)
    },
}, {
    timestamps: false,
    freezeTableName: true,
});

const attendance = db.define('attendance', {
    attendanceID: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolid: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    classid: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    studentid: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    subject_id: {
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    date: {
        type: Sequelize.DATE
    },
    attendance: {
        type: Sequelize.TINYINT(1)
    },
    takenby: {
        type: Sequelize.INTEGER(11)
    },
    comment: {
        type: Sequelize.TEXT
    },
    registered_date: {
        type: Sequelize.DATE
    }
}, {
    timestamps: false,
    freezeTableName: true,
})

const punctuality = db.define('punctuality', {
    punctuality_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    no_student_attended: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    totals: {
        type: Sequelize.STRING(45)
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    date_registered: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(45),
        allowNull: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
})

const attentionskills = db.define('attentionskills', {
    attentionskills_id: {
        type: Sequelize.INTEGER(10),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    student_reg_Id: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    // date_registered: {
    //     type: Sequelize.STRING(45),
    //     allowNull: true
    // },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    concentration_for_3_minutes: {
        type: Sequelize.STRING(45)
    },
    concentration_for_5_minutes: {
        type: Sequelize.STRING(45)
    },
    concentration_for_10_minutes: {
        type: Sequelize.STRING(45)
    },
    respond_to_correction: {
        type: Sequelize.STRING(45)
    },
    easily_distracted: {
        type: Sequelize.STRING(45)
    },
    overcome_difficulties: {
        type: Sequelize.STRING(45)
    },
    enjoy_listening: {
        type: Sequelize.STRING(45)
    },
    willing_to_paticipate: {
        type: Sequelize.STRING(45)
    },
    registeredBy: {
        type: Sequelize.STRING(95)
    },
    attentionskills_status: {
        type: Sequelize.STRING(45)
    }

}, {
    timestamps: false,
    freezeTableName: true,
})


module.exports = {
    socialdevelopment, pychomotorskills, 
    workhabit, attendance, punctuality, attentionskills
}