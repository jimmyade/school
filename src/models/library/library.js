const Sequelize = require('sequelize');
const db = require('../../datab/db');


const lms_book_details = db.define('lms_book_details', {
    BOOK_CODE: {
        type: Sequelize.STRING(10),
    },
    BOOK_TITLE: {
        type: Sequelize.STRING(50),
    },
    CATEGORY: {
        type: Sequelize.STRING(15),
    },
    AUTHOR: {
        type: Sequelize.STRING(30),
    },
    PUBLICATION: {
        type: Sequelize.STRING(30),
    },
    PUBLISH_DATE: {
        type: Sequelize.DATE,
    },
    BOOK_EDITION: {
        type: Sequelize.INTEGER(2),
    },
    PRICE: {
        type: Sequelize.DECIMAL(8),
        decimals: 2
    },
    RACK_NUM: {
        type: Sequelize.INTEGER(3),
    },
    // PUBLISH_DATE: {
    //     type: Sequelize.DATE,
    // },
    SUPPLIER_ID: {
        type: Sequelize.INTEGER(20),
    },
    schoolid: {
        type: Sequelize.STRING(45)
    },
    id:{
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    photo: {
        type: Sequelize.STRING(255)
    }
}, {
    timestamps: false,
    freezeTableName: true,
})

const lms_book_issue = db.define('lms_book_issue', {
    BOOK_ISSUE_NO: {
        type: Sequelize.INTEGER(11),
        autoIncrement: true,
        primaryKey: true,
        allowNull: true
    },
    MEMBER_ID: {
        type: Sequelize.STRING(10),
    },
    BOOK_CODE: {
        type: Sequelize.STRING(10),
    },
    DATE_ISSUE: {
        type: Sequelize.DATE,
    },
    DATE_RETURN: {
        type: Sequelize.DATE,
    },
    DATE_RETURNED: {
        type: Sequelize.DATE,
    },
    FINE_RANGE: {
        type: Sequelize.STRING(3),
    },
    schoolid: {
        type: Sequelize.STRING(45)
    }

}, {
    timestamps: false,
    freezeTableName: true,
});

const lms_fine_details = db.define('lms_fine_details', {
    FINE_RANGE: {
        type: Sequelize.STRING(3),
        NULL: true
    },
    FINE_AMOUNT: {
        type: Sequelize.DECIMAL(10),
    },
    schoolid: {
        type: Sequelize.STRING(45)
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const lms_members = db.define('lms_members', {
    MEMBER_ID: {
        type: Sequelize.STRING(10),
        NULL: true
    },
    MEMBER_NAME: {
        type: Sequelize.DECIMAL(30),
    },
    CITY: {
        type: Sequelize.DECIMAL(20),
    },
    DATE_REGISTER: {
        type: Sequelize.DATE,
    },
    DATE_EXPIRE: {
        type: Sequelize.DATE,
    },
    MEMBERSHIP_STATUS: {
        type: Sequelize.STRING(15),
    },
    schoolid: {
        type: Sequelize.STRING(45)
    },
    id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

const lms_suppliers_details = db.define('lms_suppliers_details', {
    SUPPLIER_ID: {
        type: Sequelize.STRING(20),
        NULL: true
    },
    SUPPLIER_NAME: {
        type: Sequelize.STRING(30),
    },
    ADDRESS: {
        type: Sequelize.STRING(50),
    },
    CONTACT: {
        type: Sequelize.BIGINT(11),
    },
    EMAIL: {
        type: Sequelize.STRING(15),
    },
    schoolid: {
        type: Sequelize.STRING(45)
    }
}, {
    timestamps: false,
    freezeTableName: true,
});

module.exports = {
    lms_book_details, lms_book_issue, lms_fine_details, lms_members, lms_suppliers_details
}