const Sequelize = require('sequelize');
const db = require('../../datab/db');

const expense_details = db.define('expense_details', {
    expense_details_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    expense_head_id: {
        type: Sequelize.STRING
    },
    amount: {
        type: Sequelize.DECIMAL(50, 2)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const expense_heads = db.define('expense_heads', {
    expense_head_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    expense_head: {
        type: Sequelize.STRING
    },
    expense_code: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.TEXT
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
})

const expense_limit_level = db.define('expense_limit_level', {
    acct_expense_limit_level_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    expense_limit_level: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
})

const expense_limit_setup = db.define('expense_limit_setup', {
    acct_expense_limit_setup_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    expense_head_id: {
        type: Sequelize.INTEGER(11)
    },
    expense_limit_level: {
        type: Sequelize.STRING
    },
    designation_id: {
        type: Sequelize.INTEGER(11)
    },
    employee_no: {
        type: Sequelize.STRING
    },
    limit_amount: {
        type: Sequelize.DECIMAL(50, 2)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.DATE
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
})

const expense_requests = db.define('expense_requests', {
    expense_request_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    expense_title: {
        type: Sequelize.STRING
    },
    expense_head_id: {
        type: Sequelize.INTEGER(11)
    },
    quantity: {
        type: Sequelize.INTEGER(11)
    },
    requested_amount: {
        type: Sequelize.DECIMAL(50, 2)
    },
    employee_no: {
        type: Sequelize.STRING
    },
    designation_id: {
        type: Sequelize.INTEGER(11)
    },
    batch_no: {
        type: Sequelize.STRING
    },
    batch_description: {
        type: Sequelize.STRING
    },
    approver_employee_no: {
        type: Sequelize.STRING
    },
    first_approver: {
        type: Sequelize.TINYINT(1)
    },
    first_reject: {
        type: Sequelize.TINYINT(1)
    },
    first_rejection_reason: {
        type: Sequelize.STRING
    },
    second_approver: {
        type: Sequelize.TINYINT(1)
    },
    second_rejection_reason: {
        type: Sequelize.STRING
    },
    approved: {
        type: Sequelize.TINYINT(1)
    },
    approved_amount: {
        type: Sequelize.DECIMAL(50, 2)
    },
    remitted_amount: {
        type: Sequelize.DECIMAL(50, 2)
    },
    supporting_file_url: {
        type: Sequelize.STRING
    },
    cash_remitted: {
        type: Sequelize.TINYINT(1)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.DATE
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
})

module.exports = {
    expense_details, expense_heads, expense_limit_level, expense_limit_setup, expense_requests
}