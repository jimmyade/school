const Sequelize = require('sequelize');
const db = require('../../datab/db');

const inventory_item_categories = db.define('inventory_item_categories', {
    item_category_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    parent_item_category: {
        type: Sequelize.STRING
    },
    parent_description: {
        type: Sequelize.STRING
    },
    item_category: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const inventory_items = db.define('inventory_items', {
    item_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    item_name: {
        type: Sequelize.STRING
    },
    sku: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    purchase_price: {
        type: Sequelize.DECIMAL(50, 2)
    },
    quantity: {
        type: Sequelize.DECIMAL(50, 2)
    },
    sales_price: {
        type: Sequelize.DECIMAL(50, 2)
    },
    item_category_id: {
        type: Sequelize.INTEGER(11)
    },
    show_on_purchases: {
        type: Sequelize.TINYINT(1)
    },
    purchase_ledger_id: {
        type: Sequelize.INTEGER(11)
    },
    cost_of_goods_ledger_id: {
        type: Sequelize.INTEGER(11)
    },
    default_incoming_tax_rate_id: {
        type: Sequelize.INTEGER(11)
    },
    show_on_invoices: {
        type: Sequelize.TINYINT(1)
    },
    invoice_ledger_id: {
        type: Sequelize.INTEGER(11)
    },
    default_outgoing_tax_rate_id: {
        type: Sequelize.INTEGER(11)
    },
    tracked: {
        type: Sequelize.TINYINT(1)
    },
    re_order_point: {
        type: Sequelize.DECIMAL(50, 2)
    },
    inventory_asset_ledger_id: {
        type: Sequelize.INTEGER(11)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    },
    is_for_procurement: {
        type: Sequelize.TINYINT(1)
    },
    is_for_sales: {
        type: Sequelize.TINYINT(1)
    },
    is_a_raw_material: {
        type: Sequelize.TINYINT(1)
    },
    user_id: {
        type: Sequelize.INTEGER(11)
    },
    supplier_id: {
        type: Sequelize.INTEGER(11)
    },
    approved: {
        type: Sequelize.TINYINT(1)
    },
    appoved_by: {
        type: Sequelize.STRING
    },
    measurement_id: {
        type: Sequelize.INTEGER(11)
    },
}, {
    timestamps: false,
    freezeTableName: true
});

const inventory_purchases = db.define('inventory_purchases', {
    raw_material_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    item_id: {
        type: Sequelize.STRING
    },
    supplier_id: {
        type: Sequelize.INTEGER(11)
    },
    quantity: {
        type: Sequelize.STRING
    },
    unit_price : {
        type: Sequelize.DECIMAL(50, 2)
    },
    sum_total : {
        type: Sequelize.DECIMAL(50, 2)
    },
    discount : {
        type: Sequelize.DECIMAL(50, 2)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});


const invoice_reminders = db.define('invoice_reminders', {
    invoice_reminder_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    days: {
        type: Sequelize.STRING
    },
    reminder_subject: {
        type: Sequelize.STRING
    },
    reminder_body: {
        type: Sequelize.TEXT
    },
    send_me_a_copy : {
        type: Sequelize.TINYINT(1)
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const raw_material_inventory = db.define('raw_material_inventory', {
    raw_material_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    item_id: {
        type: Sequelize.STRING
    },
    min_measurement: {
        type: Sequelize.STRING
    },
    max_measurement: {
        type: Sequelize.STRING
    },
    quantity: {
        type: Sequelize.STRING
    },
    reorder_level: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});

const raw_material_orders = db.define('raw_material_orders', {
    raw_material_order_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    item_id: {
        type: Sequelize.INTEGER(11)
    },
    quantity: {
        type: Sequelize.STRING
    },
    user_id: {
        type: Sequelize.STRING(50)
    },
    approved: {
        type: Sequelize.STRING
    },
    approved_by: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});
const raw_material_requests = db.define('raw_material_requests', {
    raw_material_request_id: {
        type: Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    product_item_id: {
        type: Sequelize.INTEGER(11)
    },
    quantity: {
        type: Sequelize.STRING
    },
    batch_no: {
        type: Sequelize.INTEGER(11)
    },
    user_id: {
        type: Sequelize.INTEGER(11)
    },
    approved: {
        type: Sequelize.STRING
    },
    approved_by: {
        type: Sequelize.STRING
    },
    service_id: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    updated_by: {
        type: Sequelize.STRING
    },
    enabled: {
        type: Sequelize.TINYINT(1)
    },
    deleted: {
        type: Sequelize.TINYINT(1)
    }
}, {
    timestamps: false,
    freezeTableName: true
});
module.exports = {
    inventory_item_categories,inventory_items,inventory_purchases, invoice_reminders, raw_material_inventory, raw_material_orders, raw_material_requests
}