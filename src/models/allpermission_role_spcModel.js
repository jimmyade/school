const Sequelize = require('sequelize');
const db = require('../datab/db');
const {service_code, service_id} = require('../datab/service')

exports.Permission_role = db.define('permission_role',{
    permission_role_id:{
        type: Sequelize.BIGINT(20),
        allowNull: true,
        PrimaryKey: true,
        autoIncrement: true
    },
    permission_role_id:{
        type: Sequelize.INTEGER(20),
        allowNull: true,
    },
    permission_role_id:{
        type: Sequelize.INTEGER(20),
        allowNull: true,
    },
    permission_role_id:{
        type: Sequelize.STRING(100)
    },
    permission_role_id:{
        type: Sequelize.DATE
    },
    permission_role_id:{
        type: Sequelize.DATE
    },
    permission_role_id:{
        type: Sequelize.STRING(100)
    },
},{
    freezeTableName: true,
    timestamps: false
});


exports.Permissions = db.define('permissions',{
    id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
        autoIncrement: true,
        primaryKey: true
    },
    name:{
        type: Sequelize.STRING,
        allowNull:true
    },
    label:{
        type: Sequelize.STRING
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    }
},{
    freezeTableName: true,
    timestamps: false
});

exports.Role_user = db.define('role_user',{
    role_user_id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    user_id:{
        type: Sequelize.BIGINT(20),
        allowNull: true,
    },
    role_id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
    },
    created_by:{
        type: Sequelize.STRING(55)
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    },
    updated_by:{
        type: Sequelize.STRING(120)
    },
    service_id:{
        type: Sequelize.STRING(145),
        allowNull: true,
        defaultValue: service_id
    },
},{
    freezeTableName: true,
    timestamps: false
});

exports.Roles = db.define('roles',{
    id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    name:{
        type: Sequelize.STRING,
        allowNull:true
    },
    label:{
        type: Sequelize.STRING
    },
    client_type_id:{
        type: Sequelize.INTEGER(10)
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_at:{
        type: Sequelize.DATE
    }
},{
    freezeTableName: true,
    timestamps: false
});

exports.Spc_accounts = db.define('spc_accounts', {
    spc_accounts_id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    spc_category_id:{
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    spc_name:{
        type: Sequelize.STRING,
        allowNull:true
    },
    spc_desciptions:{
        type: Sequelize.STRING
    },
    bank_id:{
        type: Sequelize.INTEGER(11),
        allowNull: true
    },
    sortcode:{
        type: Sequelize.STRING
    },
    isw:{
        type: Sequelize.STRING
    },
    cbn:{
        type: Sequelize.STRING
    },
    account_name:{
        type: Sequelize.STRING,
        allowNull: true
    },
    account_number:{
        type: Sequelize.STRING
    },
    account_type:{
        type: Sequelize.STRING
    },
    service_id:{
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: service_id
    },
    created_by:{
        type: Sequelize.STRING
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_by:{
        type: Sequelize.STRING
    },
    updated_at:{
        type: Sequelize.DATE
    },
    share_method:{
        type: Sequelize.STRING
    },
    amount_share:{
        type: Sequelize.DECIMAL(50),
        allowNull: true,
        decimals: 3
    },
    nibbs_amount_share:{
        type: Sequelize.DECIMAL(50),
        decimals: 3
    }
},{
    freezeTableName: true,
    timestamps: false
})

exports.Spc_categories = db.define('spc_categories',{
    spc_category_id:{
        type: Sequelize.INTEGER(10),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    spc_category:{
        type: Sequelize.STRING,
        allowNull: true,
    },
    spc_category_description:{
        type: Sequelize.STRING
    },
    created_by:{
        type: Sequelize.STRING(55)
    },
    created_at:{
        type: Sequelize.DATE
    },
    updated_by:{
        type: Sequelize.STRING(110)
    },
    updated_at:{
        type: Sequelize.DATE
    },
},{
    freezeTableName: true,
    timestamps: false
});