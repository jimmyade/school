const Sequelize = require('sequelize');
const db = require('../../datab/db');

const exam = db.define('exam', {
    id: {
        type: Sequelize.INTEGER(10),
        primaryKey: true,
        autoIncrement: true,
        allowNull: true
    },
    studentId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    className: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    subjects: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    term: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    sesion: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    dateRegistered: {
        type: Sequelize.DATE,
        allowNull: true
    },
    schoolId: {
        type: Sequelize.STRING(45),
        allowNull: true
    },
    registeredBy: {
        type: Sequelize.STRING(145),
        allowNull: true
    },
    classCode: {
        type: Sequelize.STRING(95),
        allowNull: true
    },
    total_number_of_students:{
        type: Sequelize.INTEGER(11)
    }
}, {
    timestamps: false,
    freezeTableName: true
});

module.exports = {
    exam
}