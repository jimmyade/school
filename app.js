const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('dotenv').config()
const methodOverride = require('method-override');
const path = require('path');
const cors = require('cors');
const expressLayout = require('express-ejs-layouts');
// const routes = require('./src/routes/crmRoutes');
const cookieParser = require('cookie-parser');
// const auth = require('./src/lib/auth');
// const auth = require('./src/middleware/deserializeUser')

const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const auth = require('./src/lib/JWT')
//local variable
app.locals.copyRights = () => {
  return new Date().getFullYear();
}
app.locals.appname = "WEDEX";

//MySql Store setup
const options = {
  host: process.env.DB_HOST,
  user: process.env.NAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DATABASE
};
const sessionStore = new MySQLStore(options);


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.use(cooki)

app.use(cors());
app.use(methodOverride('_method'));
app.use(expressLayout);

app.use(cookieParser());

//session setup
app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'kkjkkljkjjl',
  resave: false,
  saveUninitialized: false,
  store: sessionStore,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 365
  }
}));


// app.use(auth.initialize);
// app.use(auth.session);
// app.use(auth.setUser);

app.use(auth.validateToken)
// express messages middlewere 
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

// app.use(function(req, res, next){
//   if (service_id == "undefined")
//       err.status = 300;
//       next(err);
//  });

app.route("*")
  .get(function (req, res, next) {
    res.locals.user = req.user || null ;
    res.locals.link = req.originalUrl;
    res.locals.cart = req.session.cart;
    res.locals.login = req.isAuthenticated();
    // console.log(req.user)
    next();
  })
  .post(function (req, res, next) {
    res.locals.user = req.user || null;
    res.locals.link = req.originalUrl;
    res.locals.login = req.isAuthenticated();

    next();
  })


// app.get('')
//using app
app.use(express.static('./public'));
app.use(express.static('./uploads'));

//include routers pages
const appUsers = require('./src/routes/users/index');
const admin = require('./src/routes/admin');
const teacher = require('./src/routes/teachersRoute');
const parentRoute = require('./src/routes/parents')
const library = require('./src/routes/library')
const attend = require('./src/routes/attendance')
const chartd = require('./src/routes/chart_data')
const cart = require('./src/routes/cart');
const report = require('./src/routes/reports');
const paystacts = require('./src/routes/payment/paystackPaymentRoute');

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');




app.use(appUsers);
app.use(chartd)

app.use('/admin', admin);
app.use('/api/teachers', teacher);
app.use('/api/parents', parentRoute);
app.use('/api/labrary', library);
app.use('/attendance', attend);

app.use(cart)
app.use(paystacts);
app.use(report);

// error handling
app.use(function(req, res, next){
  const err = new Error("Not found");
  err.status = 404;
  next(err);
 });
 
 app.use((err, req, res, next)=>{
   res.status(err.status || 500);
   console.log(err.message)
  //  if (err.status == 300){
  //    req.flash('info', 'User not Authenticated, Please Login!!');
  //    res.render('./registration/login')
  //  } 
    res.render('error',{
      error:{
        status: err.status || 500,
        message: err.message
      }
    })
   
   
 })


const PORT = process.env.PORT || 5000;


app.listen(PORT, () => {
  console.log(`App running at port ${PORT}`);
});
